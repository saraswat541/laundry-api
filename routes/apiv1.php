<?php

use App\Http\Controllers\Api\v1\LoginController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Api\v1','middleware' => 'App\Http\Middleware\GuestApi'], function(){


});

Route::group(['namespace'=>'Api\v1','middleware' => 'App\Http\Middleware\GuestApi'], function(){

	Route::post('login', 'LoginController@login');
    Route::post('forget-password', 'LoginController@ForgetPassword');
    Route::post('signup', 'LoginController@signUp');
    Route::post('change-password/{validate_string}','LoginController@changePassword');
    Route::post('update-profile','LoginController@updateProfile');
    Route::post('add-category','CategoryController@addCategory');
    Route::post('add-service','CategoryController@addService');
    //  Route::post('asign-driver','DriverController@assignDriver');

    Route::post('resend-otp','LoginController@resendOtp');

});
Route::group(['namespace'=>'Api\v1','middleware' => 'App\Http\Middleware\AuthApi'], function(){

    Route::post('add-contact','LoginController@addContact');

});


