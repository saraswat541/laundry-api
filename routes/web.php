<?php
DB::enableQueryLog() ;
include(app_path().'/global_constants.php');
include(app_path().'/settings.php');
require_once(APP_PATH.'/libraries/CustomHelper.php');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
	Route::get('/base/uploder','BaseController@saveCkeditorImages');
	Route::post('/base/uploder','BaseController@saveCkeditorImages');

