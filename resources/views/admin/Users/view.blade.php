@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		View {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">View {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							@if(!empty($model->user_image))
							<tr>
								<th  width="30%" class="text-right txtFntSze">Image</th>
								<td data-th='Category Name' class="txtFntSze"><img src="{{ USER_IMAGE_URL.$model->user_image }}" width="100" height="50" alt=""></td>
							</tr>
							@endif
							<tr>
								<th  width="30%" class="text-right txtFntSze">Daycare Name</th>
								<td data-th='Category Name' class="txtFntSze">{{ $model->daycare_name }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Name</th>
								<td data-th='Name' class="txtFntSze">{{ isset($model->name) ? $model->name :'' }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Email</th>
								<td data-th='Category Name' class="txtFntSze">{{ $model->email }}</td>
							</tr> 
							<tr>
								<th  width="30%" class="text-right txtFntSze">Phone Number</th>
								<td data-th='Category Name' class="txtFntSze">{{ $model->phone_number }}</td>
							</tr>


							
							<tr>
								<th  width="30%" class="text-right txtFntSze">Registered On</th>
								<td data-th='Category Name' class="txtFntSze">{{ date(config::get("Reading.date_format"),strtotime($model->created_at)) }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Status</th>
								<td data-th='Status' class="txtFntSze">
									@if($model->is_active	==1)
									<span class="label label-success" >{{ trans("Activated") }}</span>
									@else
									<span class="label label-warning" >{{ trans("Deactivated") }}</span>
									@endif 
								</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
