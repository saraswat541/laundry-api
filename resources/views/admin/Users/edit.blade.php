@extends('admin.layouts.default')
@section('content') 
<?php
  $automation = [
    ''    => 'Select Gender',
    '1'    => 'Male',
    '2'     => 'Female',
    '3'         => 'Other',
  ];
?>
<section class="content-header">
	<h1>
		Edit {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Edit {{ $sectionNameSingular }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			{{ Form::open(['role' => 'form','url' =>  route("$modelName.edit",$model->id),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
			<div class="row">
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('daycare')?'has-error':''); ?>">
						<div class="mws-form-row">
						{!! HTML::decode( Form::label('daycare', trans("Daycare Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('daycare',$model->daycare_name, ['class' => 'form-control small']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('daycare'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
						<div class="mws-form-row">
						{!! HTML::decode( Form::label('name', trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('name',$model->name, ['class' => 'form-control small']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('name'); ?>
								</div>
							</div>
						</div>
					</div>  
				</div> 
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('email')?'has-error':''); ?>">
						<div class="mws-form-row">
						{!! HTML::decode( Form::label('email', trans("Email").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('email',$model->email, ['class' => 'form-control small']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('email'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('phone_number')?'has-error':''); ?>">
						<div class="mws-form-row">
						{!! HTML::decode( Form::label('phone_number', trans("Phone Number").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('phone_number',$model->phone_number, ['class' => 'form-control small']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('phone_number'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('user_image')) ? 'has-error' : ''; ?>">
						{!! HTML::decode( Form::label('user_image', trans("Image").'<!-- <span class="requireRed"> * </span> -->', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::file('user_image',['class'=>'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('user_image'); ?>
							</div>
							@if(!empty($model->user_image))
							<br>
							<img src="{{ USER_IMAGE_URL.$model->user_image }}" width="100" height="50" alt="">
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				<a href='{{ route("$modelName.edit",$model->id)}}' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
				<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
			{{ Form::close() }} 
		</div>
	</div>  
</section>
@stop
