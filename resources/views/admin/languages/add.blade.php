@extends('admin.layouts.default')

@section('content')

<section class="content-header">
	<h1>
		Add New Word
	</h1>
	<ol class="breadcrumb">
		<li >
			<a href="{{URL::to('adminpnlx/language-settings')}}">{{ trans("Back To Language Settings") }} </a>
		</li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			{{ Form::open(['role' => 'form','url' => 'adminpnlx/language-settings/add-setting','class' => 'mws-form']) }}
			<div class="row">
				<div class="col-md-6">	
					<div class="mws-form-row">
						{{  Form::label('default', 'Default', ['class' => 'mws-form-label']) }} <span class="asterisk">*</span>
						<div class="mws-form-item">
							{{ Form::text('default', '', ['class' => 'form-control small']) }} 
							<div class="error-message help-inline">
								<?php echo $errors->first('default'); ?>
							</div>
						</div>
					</div>
					@if(!empty($languages))
						@foreach($languages as $key => $val)
							<div class="mws-form-row">
								{{  Form::label('email', $val->title, ['class' => 'mws-form-label']) }}
								<div class="mws-form-item">
									{{ Form::text("language[$val->lang_code]",'', ['class' => 'form-control small']) }} 
									<div class="error-message help-inline">
										<?php echo $errors->first('email'); ?>
									</div>
								</div>
							</div>
						@endforeach
					@endif
					<div class="clearfix" ></div>
					<div class="mws-button-row">
						<div class="input" >
							<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
							<a href="{{URL::to('adminpnlx/language-settings/add-setting')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Reset') }}</a>
						</div>
					</div>
				</div>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</section>
@stop
