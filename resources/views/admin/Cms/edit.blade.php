@extends('admin.layouts.default')

@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		{{ trans("Edit Cms") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/cms-manager')}}">Cms Pages</a></li>
		<li class="active">Edit Cms</li>
	</ol>
</section>

<section class="content"> 
    <div class="box"> 
	    <div class="box-body">	
		    <div class="row">
				<div class="col-md-6">
					@if(count($languages) > 1)
						<div  class="default_language_color">
							{{ Config::get('default_language.message') }}
						</div>
						<div class="wizard-nav wizard-nav-horizontal">
							<ul class="nav nav-tabs">
								<?php $i = 1 ; ?>
								@foreach($languages as $value)
									<li class=" {{ ($i ==  $language_code )?'active':'' }}">
										<a data-toggle="tab" href="#{{ $i }}div">
											{{ $value -> title }}
										</a>
									</li>
									<?php $i++; ?>
								@endforeach
							</ul>
						</div>
					@endif
					{{ Form::open(['role' => 'form','url' => 'adminpnlx/cms-manager/edit-cms/'.$adminCmspage->id,'class' => 'mws-form']) }}
					{{ Form::hidden('id', $adminCmspage->id) }}
						<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
							<div class="mws-form-row">
								{!! HTML::decode( Form::label('name',trans("Page Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
								<div class="mws-form-item">
									{{ Form::text('name',$adminCmspage->name, ['class' => 'form-control']) }}
									<div class="error-message help-inline">
										<?php echo $errors->first('name'); ?>
									</div>
								</div>
							</div>
						</div>
						
						@if(count($languages) > 1)
							<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
								<hr class ="hrLine"/>
								<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
							</div>
						@endif
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						@foreach($languages as $value)
							<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
								<div class="mws-form-inline">
									<div class="form-group <?php if($i == 1){ echo ($errors->first('title')?'has-error':'');} ?>">
										@if($i == 1)
											{!! HTML::decode( Form::label($i.'.title',trans("Page Title").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
											{!! HTML::decode( Form::label($i.'.title',trans("Page Title").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::text("data[$i][title]",isset($multiLanguage[$i]['title'])?$multiLanguage[$i]['title']:'', ['class' => 'form-control']) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('title') : ''; ?>
											</div>
										</div>
									</div>
									<div class="form-group <?php if($i == 1){ echo ($errors->first('body')?'has-error':'');} ?>">
										@if($i == 1)
										{!! HTML::decode( Form::label($i.'._body',trans("Description").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
										{!! HTML::decode( Form::label($i.'._body',trans("Description").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::textarea("data[$i][body]",isset($multiLanguage[$i]['body'])?$multiLanguage[$i]['body']:'', ['class' => 'form-control textarea_resize','id' => 'body_'.$i ,"rows"=>3,"cols"=>3]) }}
											<span class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('body') : ''; ?>
											</span>
										</div>
										<script type="text/javascript">
										/* For CKEDITOR */
											
											CKEDITOR.replace( <?php echo 'body_'.$i; ?>,
											{
												height: 150,
												width: 485,
												filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
												filebrowserImageWindowWidth : '640',
												filebrowserImageWindowHeight : '480',
												enterMode : CKEDITOR.ENTER_BR
											});
												
										</script>
									</div>
									<!--<div class="form-group <?php if($i == 1){ echo ($errors->first('meta_title')?'has-error':'');} ?>">
										@if($i == 1)
										{!! HTML::decode( Form::label('meta_title',trans("messages.system_management.meta_title").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
										{!! HTML::decode( Form::label('meta_title',trans("messages.system_management.meta_title").'<span class="requireRed"> </span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::textarea("data[$i][meta_title]",isset($multiLanguage[$i]['meta_title'])?$multiLanguage[$i]['meta_title']:'', ['class' => 'form-control textarea_resize' ,"rows"=>3,"cols"=>3]) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('meta_title') : ''; ?>
											</div>
										</div>
									</div>
									<div class="form-group <?php if($i == 1){ echo ($errors->first('meta_description')?'has-error':'');} ?>">
										@if($i == 1)
										{!! HTML::decode( Form::label('meta_description',trans("messages.system_management.meta_description").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
										{!! HTML::decode( Form::label('meta_description',trans("messages.system_management.meta_description").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::textarea("data[$i][meta_description]",isset($multiLanguage[$i]['meta_description'])?$multiLanguage[$i]['meta_description']:'', ['class' => 'form-control textarea_resize' ,"rows"=>3,"cols"=>3]) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('meta_description') : ''; ?>
											</div>
										</div>
									</div>
									<div class="form-group <?php if($i == 1){ echo ($errors->first('meta_keywords')?'has-error':'');} ?>">
										@if($i == 1)
										{!! HTML::decode( Form::label('meta_keywords',trans("messages.system_management.meta_keyword").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
										{!! HTML::decode( Form::label('meta_keywords',trans("messages.system_management.meta_keyword").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::textarea("data[$i][meta_keywords]",isset($multiLanguage[$i]['meta_keywords'])?$multiLanguage[$i]['meta_keywords']:'', ['class' => 'form-control textarea_resize' ,"rows"=>3,"cols"=>3]) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('meta_keywords') : ''; ?>
											</div>
										</div>
									</div>-->
								</div>
							</div> 
							<?php $i++ ; ?>
						@endforeach
						<div class="mws-button-row">
							<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
							
							<a href="{{URL::to('adminpnlx/cms-manager/edit-cms/'.$adminCmspage->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
							
							<a href="{{URL::to('adminpnlx/cms-manager')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
						</div>
					</div>
					{{ Form::close() }} 
				</div>		
			</div>	
		</div>
	</div>
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
	#body_1 {
		resize: vertical;
	}
</style>
@stop
