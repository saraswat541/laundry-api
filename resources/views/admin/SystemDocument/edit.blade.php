@extends('admin.layouts.default')
@section('content') 
<script src="{{WEBSITE_JS_URL}}admin/plugins/ckeditor/ckeditor.js"></script>
<section class="content-header">
	<h1>
		Edit {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Edit {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			{{ Form::open(['role' => 'form','url' => route("$modelName.edit",$model->id),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
			<div class="row">
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
						<div class="mws-form-row">
						{!! HTML::decode( Form::label('name', trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('name',$model->name,['class' => 'form-control small',"readonly"]) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('name'); ?>
								</div>
							</div>
						</div>
					</div> 
					<div class="form-group <?php echo ($errors->first('image')?'has-error':''); ?>">
						<div class="mws-form-row">
						{!! HTML::decode( Form::label('image', trans("Document").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::file('image',['class' => 'form-control small']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('image'); ?>
								</div>
								@if($model->image != "")
									<?php
										$ext 	=	explode(".",$model->image);
										$ext 	=	end($ext);
									?>
									@if(in_array($ext,array("jpeg","jpg","png","gif","bmp")))
										<br />
										<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $model->image; ?>">
											<img class="" src="<?php echo $model->image; ?>" width="50px" height="50px">
										</a>
									@else
										<a download href="{{$model->image}}">Download</a>
									@endif
								@endif
							</div>
						</div>
					</div>   
				</div>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				<a href='{{ route($modelName.".edit",$model->id)}}' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
				<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
			{{ Form::close() }} 
		</div>
	</div>  	
</section>
@stop
