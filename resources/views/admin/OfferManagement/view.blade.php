@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		View {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">View {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content">
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							<tr>
								<th width="30%" class="text-right txtFntSze">Offer Title</th>
								<td data-th='GradeName' class="txtFntSze">{{ isset($model->offer_title) ? $model->offer_title :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Offer Description</th>
								<td data-th='GradeName' class="txtFntSze">{!! isset($model->offer_description) ? $model->offer_description :'' !!}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Offer Percent</th>
								<td data-th='GradeName' class="txtFntSze">{{ isset($model->offer_percent) ? $model->offer_percent :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Promo Code</th>
								<td data-th='GradeName' class="txtFntSze">{{ isset($model->promo_code) ? $model->promo_code :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">User Name</th>
								<td data-th='GradeName' class="txtFntSze">{{ isset($users->name) ? $users->name :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Offer Start Date</th>
								<td data-th='GradeName' class="txtFntSze">{{ isset($model->offer_startdate) ? $model->offer_startdate :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Offer End Date</th>
								<td data-th='GradeName' class="txtFntSze">{{ isset($model->offer_enddate) ? $model->offer_enddate :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Status</th>
								<td data-th='Status' class="txtFntSze">
									@if($model->is_active	==1)
										<span class="label label-success" >{{ trans("Activated") }}</span>
									@else
										<span class="label label-warning" >{{ trans("Deactivated") }}</span>
									@endif
								</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
