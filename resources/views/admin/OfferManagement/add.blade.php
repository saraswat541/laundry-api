@extends('admin.layouts.default')

@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>

<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Add New {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Add New {{ $sectionNameSingular }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			{{ Form::open(['role' => 'form','route' => "$modelName.add",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
			<div class="row">
				
				<div class="col-md-6">
					@if(count($languages) > 1)
					<div  class="default_language_color">
						{{ Config::get('default_language.message') }}
					</div>
					<div class="wizard-nav wizard-nav-horizontal">
						<ul class="nav nav-tabs">
							<?php $i = 1 ; ?>
							@foreach($languages as $value)
							<li class=" {{ ($i ==  $language_code )?'active':'' }}">
								<a data-toggle="tab" href="#{{ $i }}div">
									{{ $value -> offer_title }}
								</a>
							</li>
							<?php $i++; ?>
							@endforeach
						</ul>
					</div>
					@endif

					@if(count($languages) > 1)
					<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
						<hr class ="hrLine"/>
						<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
					</div>
					@endif
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						@foreach($languages as $value)
						<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
							<div class="mws-form-inline">
								<div class="form-group <?php if($i == 1){ echo ($errors->first('offer_title')?'has-error':'');} ?>">
									@if($i == 1)
									{!! HTML::decode( Form::label($i.'.offer_title',trans("Offer Title").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									@else
									{!! HTML::decode( Form::label($i.'.offer_title',trans("Offer Title").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
									@endif
									<div class="mws-form-item">
										{{ Form::text("data[$i][offer_title]",'', ['class' => 'form-control']) }}
										<div class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('offer_title') : ''; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $i++ ; ?>
						@endforeach						
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('offer_percent')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('offer_percent',trans("Offer Percent").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::number('offer_percent','',array('class' => 'form-control small', 'min' => '0')) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('offer_percent'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('offer_description')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('offer_description',trans("Offer Description").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::textarea('offer_description','', ['class' => 'form-control small']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('offer_description'); ?>
							</div>
						</div>
						<script type="text/javascript">
						/* For CKEDITOR */
							
							CKEDITOR.replace( <?php echo 'offer_description'; ?>,
							{
								height: 150,
								width: 485,
								filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
								filebrowserImageWindowWidth : '640',
								filebrowserImageWindowHeight : '480',
								enterMode : CKEDITOR.ENTER_BR
							});
								
						</script>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('promo_code')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('promo_code',trans("Promo Code").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('promo_code','',array('class' => 'form-control')) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('promo_code'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('user_id')) ? 'has-error' : ''; ?>">
						{{ Form::label('daycare_id',trans("Daycare List"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
						<div class="mws-form-item">
							{{ Form::select('user_id',$users,'',['class' => 'form-control','placeholder'=>'Choose Name']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('user_id'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('offer_startdate')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('offer_startdate',trans("Offer Start Date").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('offer_startdate','',['class' => 'form-control', 'id' => 'start_date']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('offer_startdate'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('offer_enddate')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('offer_enddate',trans("Offer End Date").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('offer_enddate','',['class' => 'form-control', 'id' => 'end_date']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('offer_enddate'); ?>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				<a href="{{ route($modelName.'.add')}}" class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
				<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</section>
{{ HTML::style('css/admin/bootstrap-datetimepicker.css') }}
{{ HTML::script('js/admin/moment.js') }}
{{ HTML::script('js/admin/bootstrap-datetimepicker.js') }}
<script>
jQuery(document).ready(function(){
		$('#start_date').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		
		$('#end_date').datetimepicker({
			format: 'YYYY-MM-DD'
		});
	});
</script>
@stop