@extends('admin.layouts.default')

@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>

<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Edit {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Edit {{ $sectionNameSingular }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
	    <div class="box-body">
		    <div class="row">
				{{ Form::open(['role' => 'form','url' =>  route("$modelName.edit",[$model->id]),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
				<div class="col-md-6">
					
					@if(count($languages) > 1)
						<div  class="default_language_color">
							{{ Config::get('default_language.message') }}
						</div>
						<div class="wizard-nav wizard-nav-horizontal">
							<ul class="nav nav-tabs">
								<?php $i = 1 ; ?>
								@foreach($languages as $value)
									<li class=" {{ ($i ==  $language_code )?'active':'' }}">
										<a data-toggle="tab" href="#{{ $i }}div">
											{{ $value -> title }}
										</a>
									</li>
									<?php $i++; ?>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						@foreach($languages as $value)
							<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
								<div class="mws-form-inline">
									<div class="form-group <?php if($i == 1){ echo ($errors->first('name')?'has-error':'');} ?>">
										@if($i == 1)
											{!! HTML::decode( Form::label($i.'.name',trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
											{!! HTML::decode( Form::label($i.'.name',trans("Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::text("data[$i][name]",isset($multiLanguage[$i]['name'])?$multiLanguage[$i]['name']:'', ['class' => 'form-control']) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('name') : ''; ?>
											</div>
										</div>
									</div>
								</div>
								<div class="mws-form-inline">
									<div class="form-group <?php if($i == 1){ echo ($errors->first('designation')?'has-error':'');} ?>">
										@if($i == 1)
											{!! HTML::decode( Form::label($i.'.designation',trans("Designation").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
											{!! HTML::decode( Form::label($i.'.designation',trans("Designation").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::text("data[$i][designation]",isset($multiLanguage[$i]['designation'])?$multiLanguage[$i]['designation']:'', ['class' => 'form-control']) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('designation') : ''; ?>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group <?php if($i == 1){ echo ($errors->first('description')?'has-error':'');} ?>">
									@if($i == 1)
									{!! HTML::decode( Form::label($i.'._description',trans("Description").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									@else
									{!! HTML::decode( Form::label($i.'._description',trans("Description").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
									@endif
									<div class="mws-form-item">
										{{ Form::textarea("data[$i][description]",isset($multiLanguage[$i]['description'])?$multiLanguage[$i]['description']:'', ['class' => 'form-control textarea_resize','id' => 'description_'.$i ,"rows"=>3,"cols"=>3]) }}
										<span class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('description') : ''; ?>
										</span>
									</div>
									<script type="text/javascript">
									/* For CKEDITOR */
										
										CKEDITOR.replace( <?php echo 'description_'.$i; ?>,
										{
											height: 250,
											filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
											filebrowserImageWindowWidth : '640',
											filebrowserImageWindowHeight : '480',
											enterMode : CKEDITOR.ENTER_BR
										});
											
									</script>
								</div>
							</div> 
						<?php $i++ ; ?>
						@endforeach
						<div class="mws-button-row">
							<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
							<a href='{{ route("$modelName.edit",[$model->id])}}' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
							<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
						</div>
					</div>
					
				</div>

				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
						{!! HTML::decode( Form::label('image', trans("Image").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::file('image',['class'=>'form-control']) }}
							<div class="error-message help-inline image_error required_error">
								<?php echo $errors->first('image'); ?>
							</div>
						</div>
					</div>

					<div class="usermgmt_image">		
						@if($model->image != "")	
						<br />
						<img height="50" width="50" src="{{ $model->image }}" />
						@endif
					</div>

					
				</div>	

				{{ Form::close() }} 
			</div>
		</div>
	</div>
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
</style>
@stop
