@extends('admin.layouts.default')
@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Edit {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Edit {{ $sectionNameSingular }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			{{ Form::open(['role' => 'form','url' =>  route("$modelName.edit",$model->id),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
							{!! HTML::decode( Form::label('name',trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('name',$model->name, ['class' => 'form-control']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('name'); ?>
								</div>
							</div>
						</div>	
					</div>
				
					<div class="col-md-6">
						@if(count($languages) > 1)
						<div  class="default_language_color">
							{{ Config::get('default_language.message') }}
						</div>
						<div class="wizard-nav wizard-nav-horizontal">
							<ul class="nav nav-tabs">
								<?php $i = 1 ; ?>
								@foreach($languages as $value)
								<li class=" {{ ($i ==  $language_code )?'active':'' }}">
									<a data-toggle="tab" href="#{{ $i }}div">
										{{ $value -> institute_name }}
									</a>
								</li>
								<?php $i++; ?>
								@endforeach
							</ul>
						</div>
						@endif
						@if(count($languages) > 1)
						<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
							<hr class ="hrLine"/>
							<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
						</div>
						@endif
						<div class="mws-panel-body no-padding tab-content">
							<?php $i = 1 ; ?>
							@foreach($languages as $value)
							<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
								<div class="mws-form-inline">
									<div class="form-group <?php if($i == 1){ echo ($errors->first('institute_name')?'has-error':'');} ?>">
										@if($i == 1)
										{!! HTML::decode( Form::label($i.'.institute_name',trans("Institute Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
										{!! HTML::decode( Form::label($i.'.institute_name',trans("Institute Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::text("data[$i][institute_name]",$model->daycare_name, ['class' => 'form-control']) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('institute_name') : ''; ?>
											</div>
										</div>
									</div>						
								</div>
							</div> 
							<?php $i++ ; ?>
							@endforeach	
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('plan')) ? 'has-error' : ''; ?>">
							{{ Form::label('plan',trans("Plans"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
							<div class="mws-form-item">
								{{ Form::select('plan',$plans,$model->plan_id,['class' => 'form-control','placeholder'=>'Choose Plan']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('plan'); ?>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('institute_email')?'has-error':''); ?>">
							{!! HTML::decode( Form::label('institute_email',trans("Institute Email").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('institute_email',$model->email, ['class' => 'form-control']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('institute_email'); ?>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('products')) ? 'has-error' : ''; ?>">
							{{ Form::label('products',trans("Products"), ['class' => 'mws-form-label']) }}<span class="requireRed"> </span>
							<div class="mws-form-item">
								{{ Form::select('products[]',$products,$get_selected_products,['class' => 'form-control','id'=>'products','multiple' => 'multiple']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('products'); ?>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('institute_number')?'has-error':''); ?>">
							{!! HTML::decode( Form::label('institute_number',trans("Institute Contact No.").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('institute_number',$model->phone_number, ['class' => 'form-control']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('institute_number'); ?>
								</div>
							</div>
						</div> 
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('add_image')?'has-error':''); ?>">
							<div class="mws-form-row">
								{!! HTML::decode( Form::label('add_image', trans("Image").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								<div class="mws-form-item">
									{{ Form::file('add_image[]',array('multiple'=>true,'class'=>'form-control')) }}
									<div class="error-message help-inline">
										<?php echo $errors->first('add_image'); ?>
									</div>
									@if($images)
									<br />
										@foreach($images as $image)
										<img height="50" width="50" src="{{ Daycare_IMAGE_URL.$image->add_image }}" />
										@endforeach
									@endif
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('institute_address')?'has-error':''); ?>">
							{!! HTML::decode( Form::label('institute_address',trans("Institute Address").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('institute_address',$model->address, ['class' => 'form-control']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('institute_address'); ?>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="mws-button-row">
					<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
					<a href='{{ route("$modelName.edit",$model->id)}}' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
					<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
				</div>
			{{ Form::close() }} 
		</div>
	</div>
</section>
{{ HTML::style('css/admin/select2/select2.min.css') }}
{{ HTML::script('js/admin/select2/select2.min.js') }}
<script>
$(document).ready(function() {
    $('#products').select2();
});
</script>
@stop