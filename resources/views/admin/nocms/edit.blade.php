@extends('admin.layouts.default')
@section('content')
<!--set width of  Select box on date picker -->
<section class="content-header">
	<h1>
		{{ trans("Edit Seo Page") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/no-cms-manager')}}">{{ trans("Seo Page") }}</a></li>
		<li class="active">{{ trans("Edit Seo Page") }} </li>
	</ol>
</section>
<section class="content"> 
	<div class="box box-primary">
		<div class="box-body">
	<div class="row ">
		<div class="col-md-6">
			@if(count($languages) > 1)
					<div  class="default_language_color">
						{{ Config::get('default_language.message') }}
					</div>
					<div class="wizard-nav wizard-nav-horizontal">
						<ul class="nav nav-tabs">
							<?php $i = 1 ; ?>
							@foreach($languages as $value)
								<li class=" {{ ($i ==  $language_code )?'active':'' }}">
									<a data-toggle="tab" href="#{{ $i }}div">
										{{ $value -> title }}
									</a>
								</li>
								<?php $i++; ?>
							@endforeach
						</ul>
					</div>
				@endif
			<br />
			{{ Form::open(['role' => 'form','url' => 'adminpnlx/no-cms-manager/edit-doc/'.$doc->id,'class' => 'mws-form','enctype'=> 'multipart/form-data']) }}
				<div class="form-group <?php echo ($errors->first('page_id')) ? 'has-error' : ''; ?>">
					{!! HTML::decode( Form::label('page_id',trans("Page ID").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
					<div class="mws-form-item">
						{{ Form::text('page_id',$doc->page_id,['class' => 'form-control',"readonly"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('page_id'); ?>
						</div>
						Example:- about-us mean after website url
					</div>
				</div>
				<div class="form-group <?php echo ($errors->first('page_name')) ? 'has-error' : ''; ?>">
					{!! HTML::decode( Form::label('page_name',trans("Page Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
					<div class="mws-form-item">
						{!! Form::text('page_name',$doc->page_name,['class' => 'form-control']) !!}
						<div class="error-message help-inline">
							<?php echo $errors->first('page_name'); ?>
						</div>
					</div>
				</div>
				<div class="mws-panel-body no-padding tab-content"> 
				<?php $i = 1 ; ?>
				@foreach($languages as $value)
					<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
						<div class="mws-form-inline">
							<div class="form-group <?php if($i == 1){ echo ($errors->first('title')?'has-error':'');} ?>">
								@if($i == 1)
									{!! HTML::decode( Form::label($i.'.title',trans("Title").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
								@else
									{!! HTML::decode( Form::label($i.'.title',trans("Title").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::text("data[$i][title]",$doc->title, ['class' => 'form-control']) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('title') : ''; ?>
									</div>
								</div>
							</div>
							<div class="form-group <?php if($i == 1){ echo ($errors->first('meta_description')?'has-error':'');} ?>">
								@if($i == 1)
								{!! HTML::decode( Form::label('meta_description',trans("Meta Description").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@else
								{!! HTML::decode( Form::label('meta_description',trans("Meta Description").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::textarea("data[$i][meta_description]",$doc->meta_description, ['class' => 'form-control textarea_resize' ,"rows"=>false,"cols"=>false]) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('meta_description') : ''; ?>
									</div>
								</div>
							</div>
							<div class="form-group <?php if($i == 1){ echo ($errors->first('meta_keywords')?'has-error':'');} ?>">
								@if($i == 1)
								{!! HTML::decode( Form::label('meta_keywords',trans("Meta Keywords").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@else
								{!! HTML::decode( Form::label('meta_keywords',trans("Meta Keywords").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::textarea("data[$i][meta_keywords]",$doc->meta_keywords, ['class' => 'form-control textarea_resize' ,"rows"=>false,"cols"=>false]) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('meta_keywords') : ''; ?>
									</div>
								</div>
							</div>
							
							
							<div class="form-group <?php if($i == 1){ echo ($errors->first('twitter_card')?'has-error':'');} ?>">
								@if($i == 1)
								{!! HTML::decode( Form::label('twitter_card',trans("Twitter Card").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@else
								{!! HTML::decode( Form::label('twitter_card',trans("Twitter Card").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::textarea("data[$i][twitter_card]",$doc->twitter_card, ['class' => 'form-control textarea_resize' ,"rows"=>false,"cols"=>false]) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('twitter_card') : ''; ?>
									</div>
								</div>
							</div>

							<div class="form-group <?php if($i == 1){ echo ($errors->first('twitter_site')?'has-error':'');} ?>">
								@if($i == 1)
								{!! HTML::decode( Form::label('twitter_site',trans("Twitter Site").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@else
								{!! HTML::decode( Form::label('twitter_site',trans("Twitter Site").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::textarea("data[$i][twitter_site]",$doc->twitter_site, ['class' => 'form-control textarea_resize' ,"rows"=>false,"cols"=>false]) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('twitter_site') : ''; ?>
									</div>
								</div>
							</div>
							
							<div class="form-group <?php if($i == 1){ echo ($errors->first('og_url')?'has-error':'');} ?>">
								@if($i == 1)
								{!! HTML::decode( Form::label('og_url',trans("Og Url").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@else
								{!! HTML::decode( Form::label('og_url',trans("Og Url").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::textarea("data[$i][og_url]",$doc->og_url, ['class' => 'form-control textarea_resize' ,"rows"=>false,"cols"=>false]) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('og_url') : ''; ?>
									</div>
								</div>
							</div>
							
							<div class="form-group <?php if($i == 1){ echo ($errors->first('og_type')?'has-error':'');} ?>">
								@if($i == 1)
								{!! HTML::decode( Form::label('og_type',trans("Og Type").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@else
								{!! HTML::decode( Form::label('og_type',trans("Og Type").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::textarea("data[$i][og_type]",$doc->og_type, ['class' => 'form-control textarea_resize' ,"rows"=>false,"cols"=>false]) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('og_type') : ''; ?>
									</div>
								</div>
							</div>
							<div class="form-group <?php if($i == 1){ echo ($errors->first('og_title')?'has-error':'');} ?>">
								@if($i == 1)
								{!! HTML::decode( Form::label('og_title',trans("Og Title").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@else
								{!! HTML::decode( Form::label('og_title',trans("Og Title").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::textarea("data[$i][og_title]",$doc->og_title, ['class' => 'form-control textarea_resize' ,"rows"=>false,"cols"=>false]) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('og_title') : ''; ?>
									</div>
								</div>
							</div>
							
							<div class="form-group <?php if($i == 1){ echo ($errors->first('og_description')?'has-error':'');} ?>">
								@if($i == 1)
								{!! HTML::decode( Form::label('og_description',trans("Og Description").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								@else
								{!! HTML::decode( Form::label('og_description',trans("Og Description").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
								@endif
								<div class="mws-form-item">
									{!! Form::textarea("data[$i][og_description]",$doc->og_description, ['class' => 'form-control textarea_resize' ,"rows"=>false,"cols"=>false]) !!}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('og_description') : ''; ?>
									</div>
								</div>
							</div>
							
							
						</div>
					</div> 
				<?php $i++ ; ?>
				@endforeach
				<div class="form-group <?php echo ($errors->first('og_image')) ? 'has-error' : ''; ?>">
					{!! HTML::decode( Form::label('og_image',trans("Og Image").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
					<div class="mws-form-item">
						{{ Form::file('og_image') }}
						<div class="error-message help-inline">
							<?php echo $errors->first('og_image'); ?>
						</div>
						<?php 
					//	$image	=	Input::old('og_image');
						$image	=	$doc->og_image;
						?>
						@if(!empty($image) && file_exists(SEO_PAGE_IAMGE_ROOT_PATH.$image))
							{{ HTML::image( SEO_PAGE_IAMGE_URL.$image, $image , array( 'width' => 100, 'height' => 100 )) }}
						@endif
					</div>
				</div>
				<div class="mws-button-row">
					<div class="input" >
						<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
						<a href="{{URL::to('adminpnlx/no-cms-manager/edit-doc/'.$doc->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
						<a href="{{URL::to('adminpnlx/no-cms-manager')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
					</div>
				</div>
			</div>
			{{ Form::close() }}
		</div>		
	</div>
	</div>
	</div>
</section>
@stop
