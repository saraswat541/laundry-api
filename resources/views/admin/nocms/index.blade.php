@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		{{ trans("Seo Pages") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Seo Pages") }}</li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			{{ Form::open(['method' => 'get','role' => 'form','url' => 'adminpnlx/no-cms-manager','class' => 'row mws-form']) }}
			{{ Form::hidden('display') }}
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					{{ Form::text('title',((isset($searchVariable['title'])) ? $searchVariable['title'] : ''), ['class' => 'form-control','placeholder'=>"Title"]) }}
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('adminpnlx/no-cms-manager')}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> {{ trans("Clear Search") }}</a>
			</div>
			{{ Form::close() }}
			<div class="col-md-5 col-sm-5 ">
				<div class="form-group pull-right">  
					
				</div>
			</div>
		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title">Seo Pages's List</h3>
				<div class="listing-btns">
					<a href="{{URL::to('adminpnlx/no-cms-manager/add-doc')}}" class="btn btn-success btn-small align">{{ trans("Add Seo Page") }} </a>
				</div>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="15%">
							{{
								link_to_route(
								'NoCms.index',
								trans('Title'),
								array(
								'sortBy' => 'title',
								'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						
						<th width="15%">{{ trans("Page Id") }}</th>
						<th width="15%">{{ trans("Page Name") }}</th>
						<th width="20%">{{ trans("Meta Description") }}</th>
						<th width="25%">{{ trans("Meta Keywords") }}</th>
						<th width="10%">{{ trans("Action") }}</th>
					</tr>
				</thead>
				<tbody >
					@if(!$result->isEmpty())
						@foreach($result as $record)
						<tr class="items-inner">
							<td data-th='{{ trans("title") }}'>{{ $record->title }}</td>
							<td data-th='{{ trans("page_id") }}'>{{ $record->page_id }}</td>
							<td data-th='{{ trans("page_name") }}'>{{ $record->page_name }}</td>
							<td data-th='{{ trans("meta_description") }}'>{{ $record->meta_description }}</td>
							<td data-th='{{ trans("meta_keywords") }}'>{{ $record->meta_keywords }}</td>
							<td data-th='{{ trans("messages.system_management.action") }}'>
								<a href="{{URL::to('adminpnlx/no-cms-manager/edit-doc/'.$record->id)}}" title='{{ trans("Edit") }}' class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								<?php /* <a href="{{URL::to('adminpnlx/no-cms-manager/delete-doc/'.$record->id)}}" title='{{ trans("messages.system_management.delete") }}' class="btn btn-danger delete_any_item">
									<i class="fa fa-trash-o"></i>
								</a> */ ?>
							</td>
						</tr>
						@endforeach
						@else
						<tr>
							<td class="alignCenterClass" colspan="6" >{{ trans("messages.user_management.no_record_found_message") }}</td>
						</tr>
						@endif 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div>
</section> 
@stop
