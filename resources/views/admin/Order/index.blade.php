@extends('admin.layouts.default')
@section('content')
<script>
	$(function(){
		/**
		 * For match height of div 
		 */
		 $('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		 var tooltips = $( "[title]" ).tooltip({
		 	position: {
		 		my: "right bottom+50",
		 		at: "right+5 top-5"
		 	}
		 });
		});
</script>
<section class="content-header">
	<h1>
		{{ $sectionName }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li class="active"> {{ $sectionName }}</li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			{{ Form::open(['method' => 'get','role' => 'form','route' => "$modelName.index",'class' => 'row mws-form']) }}
			{{ Form::hidden('display') }}
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::select('is_active',array(''=>trans('All'),1=>trans('Active'),0=>trans('Inactive')),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('order_number',((isset($searchVariable['order_number'])) ? $searchVariable['order_number'] : ''), ['class' => ' form-control','placeholder'=>'Order Number']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('first_name',((isset($searchVariable['first_name'])) ? $searchVariable['first_name'] : ''), ['class' => ' form-control','placeholder'=>'First Name']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('last_name',((isset($searchVariable['last_name'])) ? $searchVariable['last_name'] : ''), ['class' => ' form-control','placeholder'=>'Last Name']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('email',((isset($searchVariable['email'])) ? $searchVariable['email'] : ''), ['class' => ' form-control','placeholder'=>'Email']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('address',((isset($searchVariable['address'])) ? $searchVariable['address'] : ''), ['class' => ' form-control','placeholder'=>'Address']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('city',((isset($searchVariable['city'])) ? $searchVariable['city'] : ''), ['class' => ' form-control','placeholder'=>'City']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('state',((isset($searchVariable['state'])) ? $searchVariable['state'] : ''), ['class' => ' form-control','placeholder'=>'State']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('country',((isset($searchVariable['country'])) ? $searchVariable['country'] : ''), ['class' => ' form-control','placeholder'=>'Country']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('postal_code',((isset($searchVariable['postal_code'])) ? $searchVariable['postal_code'] : ''), ['class' => ' form-control','placeholder'=>'Postal Code']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('phone_number',((isset($searchVariable['phone_number'])) ? $searchVariable['phone_number'] : ''), ['class' => ' form-control','placeholder'=>'Phone Number']) }}
				</div>
			</div>
			
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href='{{ route("$modelName.index")}}' class="btn btn-primary"> <i class="fa fa-refresh "></i> {{ trans('Clear Search') }}</a>
			</div>
			{{ Form::close() }}
		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title">{{ $sectionName }}</h3>
				<div class="listing-btns">
					<span data-href='{{route("$modelName.export-csv")}}' id="export" class="btn btn-success btn-small pull-right" onclick="exportTasks(event.target);"> {{ trans("Export ") }}{{ $sectionNameSingular }} </span>
				</div>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="15%">
							{{
								link_to_route(
								"$modelName.index",
								trans("Order Number"),
								array(
								'sortBy' => 'order_number ',
								'order' => ($sortBy == 'order_number ' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'order_number ' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'order_number ' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">
							{{
								link_to_route(
								"$modelName.index",
								trans("Full Name"),
								array(
								'sortBy' => 'first_name',
								'order' => ($sortBy == 'first_name' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'first_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'first_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">
							{{
								link_to_route(
								"$modelName.index",
								trans("E-mail"),
								array(
								'sortBy' => 'email	',
								'order' => ($sortBy == 'email' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'email' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'email' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">
							{{
								link_to_route(
								"$modelName.index",
								trans("Item Count"),
								array(
								'sortBy' => 'item_count	',
								'order' => ($sortBy == 'item_count' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'item_count' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'item_count' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">
							{{
								link_to_route(
								"$modelName.index",
								trans("Total amount"),
								array(
								'sortBy' => 'grand_total	',
								'order' => ($sortBy == 'grand_total' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'grand_total' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'grand_total' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="11%" class="action-th">
							{{
								link_to_route(
								"$modelName.index",
								trans("Status"),
								array(
								'sortBy' => 'status',
								'order' => ($sortBy == 'status' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'status' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'status' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">{{ trans("Action") }}</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					@if(!$results->isEmpty())
					@foreach($results as $result)
					<tr class="items-inner">
						<td data-th='order_number'>{{ $result->order_number  }}</td>
						<td data-th='first_name'>{{ $result->first_name }} {{ $result->last_name }}</td>
						<td data-th='email'>{{ $result->email	 }}</td>
						<td data-th='item_count'>{{ $result->item_count	}}</td>
						<td data-th='grand_total'>{{ $result->grand_total }}</td>
						<td  data-th=''>
							@if($result->status	== 'pending')
							<span class="label label-warning" >{{ trans("Pending") }}</span>
							@endif
							@if($result->status	== 'processing')
							<span class="label label-warning" >{{ trans("Processing") }}</span>
							@endif
							@if($result->status	== 'completed')
							<span class="label label-success" >{{ trans("Completed") }}</span>
							@endif
							@if($result->status	== 'decline')
							<span class="label label-danger" >{{ trans("Decline") }}</span>
							@endif
						</td>
						<td data-th='' class="action-td">
							@if($result->status == 'pending')
							<a  title="Click To Activate" href='{{route("$modelName.status",array($result->id,0))}}' class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
							</a>
							@else
							<a title="Click To Pending" href='{{route("$modelName.status",array($result->id,1))}}' class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
							</a> 
							@endif 
							
							<a href='{{route("$modelName.delete","$result->id")}}' data-delete="delete" class="delete_any_item btn btn-danger" title="Delete">
								<span class="fa fa-trash-o"></span>
							</a> 
							<a href='{{route("$modelName.view","$result->id")}}' class="btn btn-primary" title="View Order Details"> <span class="fa fa-eye"></span></a> 
						</td>
					</tr>
					@endforeach  
					@else
					<tr>
						<td colspan="12" class="alignCenterClass"> {{ trans("Record not found.") }}</td>
					</tr>
					@endif 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $results])</div>
		</div>
	</div>
</section>
<script>
function exportTasks(_this) {
      let _url = $(_this).data('href');
      window.location.href = _url;
   }
</script>
@stop