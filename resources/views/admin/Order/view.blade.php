@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		View {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">View {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content">
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							
							<tr>
								<th width="30%" class="text-right txtFntSze">Order Number</th>
								<td data-th='order_number ' class="txtFntSze">{{ isset($model->order_number ) ? $model->order_number  :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">First Name</th>
								<td data-th='first_name' class="txtFntSze">{{ isset($model->first_name) ? $model->first_name :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Last Name</th>
								<td data-th='last_name' class="txtFntSze">{{ isset($model->last_name) ? $model->last_name :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">E-mail</th>
								<td data-th='email' class="txtFntSze">{{ isset($model->email) ? $model->email :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Address</th>
								<td data-th='address' class="txtFntSze">{{ isset($model->address) ? $model->address :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">City</th>
								<td data-th='city' class="txtFntSze">{{ isset($model->city) ? $model->city :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">State</th>
								<td data-th='state' class="txtFntSze">{{ isset($model->state) ? $model->state :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Country</th>
								<td data-th='country' class="txtFntSze">{{ isset($model->country) ? $model->country :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Postal Code</th>
								<td data-th='postal_code' class="txtFntSze">{{ isset($model->postal_code) ? $model->postal_code :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Phone Number</th>
								<td data-th='phone_number' class="txtFntSze">{{ isset($model->phone_number) ? $model->phone_number :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Notes</th>
								<td data-th='notes' class="txtFntSze">{{ isset($model->notes) ? $model->notes :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Item Count</th>
								<td data-th='item_count' class="txtFntSze">{{ isset($model->item_count) ? $model->item_count :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Status</th>
								<td data-th='Status' class="txtFntSze">
									@if($model->status	== 'pending')
									<span class="label label-warning" >{{ trans("Pending") }}</span>
									@endif
									@if($model->status	== 'processing')
									<span class="label label-warning" >{{ trans("Processing") }}</span>
									@endif
									@if($model->status	== 'completed')
									<span class="label label-success" >{{ trans("Completed") }}</span>
									@endif
									@if($model->status	== 'decline')
									<span class="label label-danger" >{{ trans("Decline") }}</span>
									@endif
								</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
