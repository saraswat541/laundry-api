@extends('admin.layouts.default')

@section('content')

<?php
	$userInfo	=	Auth::user();
	$full_name	=	(isset($userInfo->full_name)) ? $userInfo->full_name : '';
	$username	=	(isset($userInfo->username)) ? $userInfo->username : '';
	$email		=	(isset($userInfo->email)) ? $userInfo->email : '';
?>

<section class="content-header">
	<h1 class="text-center">Change Password
		<span class="pull-right">
			<a href="{{URL::to('adminpnlx/myaccount')}}" class="btn btn-danger"><i class=\"icon-refresh\"></i>Back</a>
		</span>
	</h1>
	
	<div class="clearfix"></div>
</section>
<section class="content">
    <div class="box">
	    <div class="box-body">
	{{ Form::open(['role' => 'form','url' => 'adminpnlx/changed-password','class' => 'mws-form','files'=>'true']) }}
		<div class="row pad">
			<div class="col-md-6">
				<div class="form-group <?php echo (!empty($errors->first('old_password'))?"has-error":''); ?>">
					{!! HTML::decode( Form::label('email', trans("Old Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
					<div class="mws-form-item">
						{{ Form::password('old_password',['class' => 'form-control']) }}
						<!-- Toll tip div end here -->
						<div class="error-message help-inline">
							<?php echo $errors->first('old_password'); ?>
						</div>
					</div>
				</div>
				<div class="form-group <?php echo (!empty($errors->first('new_password'))?"has-error":''); ?>">
					{!! HTML::decode( Form::label('email', trans("New Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
					
					<div class="mws-form-item">
						{{ Form::password('new_password', ['class' => 'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('new_password'); ?>
						</div>
					</div>
				</div>
				<div class="form-group <?php echo (!empty($errors->first('confirm_password'))?"has-error":''); ?>">
					{!! HTML::decode( Form::label('email', trans("Confirm Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
					
					<div class="mws-form-item">
						{{ Form::password('confirm_password', ['class' => 'form-control']) }}
						
						<div class="error-message help-inline">
							<?php echo $errors->first('confirm_password'); ?>
						</div>
					</div>
				</div>
				<div class="mws-button-row">
					<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
					<a href="{{URL::to('adminpnlx/change-password')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
				</div>	
			</div>
		</div>   	
	{{ Form::close() }}
	    </div>
	</div>
</section>
@stop
