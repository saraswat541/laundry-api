@extends('admin.layouts.default')
@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>

<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Update {{ $sectionPatternName }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active"> <a href="{{ route($modelName.'.sub',$id)}}">{{ $sectionSubName }}</a></li>
		
		<li class="active">Update {{ $sectionPatternName }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="row">
				{{ Form::open(['role' => 'form','route' => "$modelName.updatePatternCategory",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}

				<table class="table table-striped table-responsive table-bordered">
					<thead>
						<tr style="background-color:#3c3f44; color:white;">
							<th class="mainth" colspan="3" style="color:white;">Tailor Measurement
							</tr>
						</thead>
					</table>
					<div class="images_holder">
						@if($measurement->isNotEmpty())
						<?php $i = 0;?>
						@foreach($measurement as $measurements)
						<div class="row images_inner images_inner_{{$i}}" data-id="{{$i}}">
							@if($i > 0)
							<hr></hr>
							@endif

							<div class="col-md-6">
								<div class="image_price form-group <?php echo ($errors->first('image_data['.$i.'][name]')?'has-error':''); ?>">
									<div class="mws-form-row">
										{!! HTML::decode( Form::label('image_data['.$i.'][name]',trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										<div class="mws-form-item">

											{{ Form::text("image_data[$i][name]",$measurements->name, ['class' => 'form-control']) }}

											<div class="error-message help-inline">
												<?php echo $errors->first('image_data['.$i.'][name]'); ?>
											</div>

										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="image_price form-group <?php echo ($errors->first('image_data['.$i.'][image]')?'has-error':''); ?>">
									<div class="mws-form-row">
										{!! HTML::decode( Form::label('image_data['.$i.'][image]',trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										<div class="mws-form-item">
											{{ Form::file('image_data['.$i.'][image]',null,$measurements->meaImg,['class' => 'form-control','accept'=>"image/*"]) }}
											<div class="error-message help-inline">
												<?php echo $errors->first('image_data['.$i.'][image]'); ?>
											</div>
											@if($measurements->meaImg != "")
											<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo CATEGORY_MEASUREMENT_IMAGE_URL.$measurements->meaImg; ?>">
												<img height="50" width="50" src="{{ CATEGORY_MEASUREMENT_IMAGE_URL. $measurements->meaImg }}" /></a>
												@endif
											</div>
										</div>

										@if($i > 0)
										<div class="form-group ">
											<button type="button" class="btn btn-danger add-row" onclick="remove_image({{$i}});">Remove</button>
										</div>
										@endif
									</div>
									
									<input type="hidden" name="image_data[{{$i}}][entry_id]" id="entry_id_{{$i}}" value="{{$measurements->id}}">
								</div>

								<?php $i++;?>
							</div>
							@endforeach
							@else
							<div class="row images_inner images_inner_0" data-id="0">

								<div class="col-md-6">
									<div class="image_price form-group ">
										<div class="mws-form-row">
											{!! HTML::decode( Form::label('image_data[0][name]',trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
											<div class="mws-form-item">


												{{ Form::text("image_data[0][name]",'', ['class' => 'form-control']) }}

												<div class="error-message help-inline">

												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="image_price form-group ">
										<div class="mws-form-row">
											{!! HTML::decode( Form::label('image_data[0][image]',trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
											<div class="mws-form-item">
												{{ Form::file('image_data[0][image]', ['class' => 'form-control','accept'=>"image/*"]) }}
												<div class="error-message help-inline">

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endif
						</div>
						<div class="row pad">
							<div class="col-md-12 col-sm-12">	
								<div class="col-md-6">	
									<button type="button" name="add" id="add" class="btn btn-success add-row" onclick="add_more_images();">Add More</button>
								</div>
							</div>
						</div>

						<div class="mws-button-row">
							<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
							<a href='{{ route("$modelName.pattern",$sub_id)}}' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
							<a href="{{ route($modelName.'.sub',$id) }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
						</div>
						{{ Form::hidden('category_id', $id) }}
						{{ Form::hidden('sub_category_id', $sub_id) }}

					</div>

				</div>
				{{ Form::close() }} 
			</div>
		</div>
	</div>
</section>
<script>
	
	function add_more_images(){
		$("#loader_img").show();
		var get_last_id  = $('.images_holder > div.images_inner').last().attr('data-id');
		// alert(get_last_id);
		var counter  	 		=  	parseInt(get_last_id) + 1;
		//alert(counter);
		$.ajax({
			headers				:	{"X-CSRF-TOKEN":"{{ csrf_token() }}"},
			url					:	"{{route('Categories.addMoreImage')}}",
			type				:	"POST",
			data				:	{'counter':counter},
			success				:	function(response){
				$("#loader_img").hide();
				$('.images_holder').append(response);
			}
		});
	}
	
	function remove_image(id){
		bootbox.confirm("Are you sure want to remove this ?",
			function(result){
				if(result){
					var entry_id = $("#entry_id_"+id).val();
					if(typeof entry_id === "undefined"){
						$('.images_inner_'+id).remove();
					}else{
						$.ajax({
							headers				:	{"X-CSRF-TOKEN":"{{ csrf_token() }}"},
							url					:	"{{route('Categories.removeImage')}}",
							type				:	"POST",
							data				:	{'entry_id':entry_id},
							success				:	function(response){
								if(response == 1){
									$('.images_inner_'+id).remove();
								}
								$("#loader_img").hide();
							}
						});
					}
				}
			}
			);
	}
</script>
@stop
