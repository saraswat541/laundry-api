
<div class="row images_inner images_inner_{{$counter}}" data-id="{{$counter}}">
	<hr></hr>
	
	<div class="col-md-6">
		<div class="image_price form-group <?php echo ($errors->first('image_data['.$counter.'][style_id]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('image_data['.$counter.'][style_id]', trans("Style").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					
					{{ Form::select('image_data['.$counter.'][style_id]',$styleList,'',['class' => 'form-control chosen-select','placeholder'=>'Select Style']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('image_data['.$counter.'][style_id]'); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="form-group ">
			<button type="button" class="btn btn-danger add-row" onclick="remove_image({{$counter}});">Remove</button>
		</div>
	</div>
</div>
