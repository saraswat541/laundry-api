@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		View {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active"> <a href="{{ route($modelName.'.sub',$model->parent_category_id)}}">{{ $sectionSubName }}</a></li>

		<li class="active">View {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							
							<tr>
								<th width="30%" class="text-right txtFntSze">Category Name</th>
								<td data-th='Name' class="txtFntSze">{{ isset($model->name) ? $model->name :'' }}</td>
							</tr>
							
							<tr>
								<th width="30%" class="text-right txtFntSze">Category Order</th>
								<td data-th='CategoryOrder' class="txtFntSze">{{ isset($model->category_order_by) ? $model->category_order_by :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Category Type</th>
								<td data-th='type' class="txtFntSze">
									@if($model->type == 0)
									{{ trans("Ready Mate Product") }}
									@else
									{{ trans("Custom Product") }}
									@endif
								</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Image</th>
								<td data-th='Image' class="txtFntSze">
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $model->image; ?>"><img src="{{$model->image}}" style="width: 50px"></a></td>
								</tr>
								<tr>
									<th width="30%" class="text-right txtFntSze">Status</th>
									<td data-th='Status' class="txtFntSze">
										@if($model->is_active	==1)
										<span class="label label-success" >{{ trans("Activated") }}</span>
										@else
										<span class="label label-warning" >{{ trans("Deactivated") }}</span>
										@endif 
									</td>
								</tr> 
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	@stop
