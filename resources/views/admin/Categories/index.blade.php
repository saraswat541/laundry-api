@extends('admin.layouts.default')
@section('content')
<script>
	$(function(){
		/**
		 * For match height of div 
		 */
		 $('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		 var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		 });
		});
</script>
<section class="content-header">
	<h1>
		{{ $sectionName }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li class="active"> {{ $sectionName }}</li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			{{ Form::open(['method' => 'get','role' => 'form','route' => "$modelName.index",'class' => 'row mws-form']) }}
			{{ Form::hidden('display') }}
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::select('is_active',array(''=>trans('All'),1=>trans('Active'),0=>trans('Inactive')),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => ' form-control','placeholder'=>'Category Name']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('category_order_by',((isset($searchVariable['category_order_by'])) ? $searchVariable['category_order_by'] : ''), ['class' => ' form-control','placeholder'=>'Category Order']) }}
				</div>
			</div>
			
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href='{{ route("$modelName.index")}}'  class="btn btn-primary"> <i class="fa fa-refresh "></i> {{ trans('Clear Search') }}</a>
			</div>
			{{ Form::close() }} 
		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title">{{ $sectionName }}'s List</h3>
				<div class="listing-btns">
					<a href='{{route("$modelName.add")}}'  class="btn btn-success btn-small"> {{ trans("Add New ") }}{{ $sectionNameSingular }} </a>
					<span data-href='{{route("$modelName.export-csv")}}' id="export" class="btn btn-success btn-small pull-right" onclick="exportTasks(event.target);"> {{ trans("Export ") }}{{ $sectionNameSingular }} </span>
				</div>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="15%">
							{{
								link_to_route(
								"$modelName.index",
								trans("Category Name"),
								array(
								'sortBy' => 'name',
								'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th> 
						<th width="15%">
							{{
								link_to_route(
								"$modelName.index",
								trans("Category Order"),
								array(
								'sortBy' => 'category_order_by',
								'order' => ($sortBy == 'category_order_by' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'category_order_by' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'category_order_by' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						
						<th width="11%">
							Image
						</th>		
						<th width="11%" class="action-th"> 
							{{
								link_to_route(
								"$modelName.index",
								trans("Status"),
								array(
								'sortBy' => 'is_active',
								'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">{{ trans("Action") }}</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					@if(!$results->isEmpty())
					@foreach($results as $result)
					<tr class="items-inner">
						<td data-th='name'>{{ $result->name }}</td>
						<td data-th='category_order_by'>{{ $result->category_order_by }}</td>
						<td data-th='image'>
							@if($result->image != "")	
							<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $result->image; ?>"><img height="50" width="50" src="{{ $result->image }}" /></a>
							@endif
						</td>
						<td  data-th=''>
							@if($result->is_active	== 1)
							<span class="label label-success" >{{ trans("Activated") }}</span>
							@else
							<span class="label label-warning" >{{ trans("Deactivated") }}</span>
							@endif
						</td>								
						<td data-th='' class="action-td">
							@if($result->is_active == 1)
							<a  title="Click To Deactivate" href='{{route("$modelName.status",array($result->id,0))}}' class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
							</a>
							@else
							<a title="Click To Activate" href='{{route("$modelName.status",array($result->id,1))}}' class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
							</a> 
							@endif 
							<a href='{{route("$modelName.edit","$result->id")}}' class="btn btn-primary" title="Edit"> <span class="fa fa-pencil"></span></a>
							<a href='{{route("$modelName.delete","$result->id")}}' data-delete="delete" class="delete_any_item btn btn-danger" title="Delete">
								<span class="fa fa-trash-o"></span>
							</a> 
							<a href='{{route("$modelName.view","$result->id")}}' class="btn btn-primary" title="View Category Details"> <span class="fa fa-eye"></span></a> 
							
						</td>
					</tr>
					@endforeach  
					@else
					<tr>
						<td colspan="12" class="alignCenterClass"> {{ trans("Record not found.") }}</td>
					</tr>
					@endif 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $results])</div>
		</div>
	</div>
</section>
<script>
	$(document).on('click', '.copy_any_item', function(e){ 
		e.stopImmediatePropagation();
		url = $(this).attr('href');
		bootbox.confirm("Are you sure you want to copy this course ?",
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
		e.preventDefault();
	});
	
	function exportTasks(_this) {
      let _url = $(_this).data('href');
      window.location.href = _url;
   }
</script>
@stop