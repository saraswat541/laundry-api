<link href="{{WEBSITE_CSS_URL}}admin/button.css" rel="stylesheet">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<a data-dismiss="modal" class="close" href="javascript:void(0)">
			<span style="float:right" class="no-ajax" aria-hidden="true">x</span>
			<span class="sr-only no-ajax"></span></a>	
			<h4 class="modal-title" id="myModalLabel">
				<?php echo trans("Email Detail"); ?>
			</h4>
		</div>
		<div class="modal-body">
			<div class="mws-panel-body no-padding dataTables_wrapper">
				<table class="table table-bordered table-responsive"  >
					<tbody>
						<?php 
						if(!empty($result)){  
							?>
								<tr>
									<th><?php echo trans("Email To"); ?></th>
									<td data-th='<?php echo trans("Email To"); ?>'> <?php echo $result->email_to;  ?></td>
								</tr>
								<tr>
									<th><?php echo trans("Email From"); ?></th>
									<td data-th='<?php echo trans("Email From"); ?>'><?php  echo $result->email_from; ?></td>
								</tr>
								<tr>
									<th><?php echo trans("Subject"); ?></th>
									<td data-th='<?php echo trans("Subject"); ?>'><?php echo  $result->subject; ?></td>
								</tr>
								<tr>
									<th valign='top'>Messages</th>
									<td ><?php  echo  $result->message; ?></td>
								</tr>
							<?php
						
						} ?>
					</tbody>		
				</table>
			</div>
			<div class="clearfix">&nbsp;</div>
		</div>
	</div>
</div>
