@extends('admin.layouts.default')

@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Edit {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Edit {{ $sectionNameSingular }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
	    <div class="box-body">
		    <div class="row">
				{{ Form::open(['role' => 'form','url' =>  route("$modelName.edit",$model->id),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
				<div class="col-md-6">
					@if(count($languages) > 1)
						<div  class="default_language_color">
							{{ Config::get('default_language.message') }}
						</div>
						<div class="wizard-nav wizard-nav-horizontal">
							<ul class="nav nav-tabs">
								<?php $i = 1 ; ?>
								@foreach($languages as $value)
									<li class=" {{ ($i ==  $language_code )?'active':'' }}">
										<a data-toggle="tab" href="#{{ $i }}div">
											{{ $value -> title }}
										</a>
									</li>
									<?php $i++; ?>
								@endforeach
							</ul>
						</div>
					@endif
					@if(count($languages) > 1)
						<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
							<hr class ="hrLine"/>
							<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
						</div>
					@endif
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						@foreach($languages as $value)
							<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
								<div class="mws-form-inline">
									<div class="form-group <?php if($i == 1){ echo ($errors->first('name')?'has-error':'');} ?>">
										@if($i == 1)
											{!! HTML::decode( Form::label($i.'.name',trans("Size Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
											{!! HTML::decode( Form::label($i.'.name',trans("Size Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::text("data[$i][name]",isset($multiLanguage[$i]['name'])?$multiLanguage[$i]['name']:'', ['class' => 'form-control']) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('name') : ''; ?>
											</div>
										</div>
									</div>							
																		
																		
																	
								</div>
							</div> 
						<?php $i++ ; ?>
						@endforeach
						<div class="mws-button-row">
							<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
							<a href='{{ route("$modelName.edit",$model->id)}}' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
							<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
						</div>
					</div>
				</div>
				{{ Form::close() }} 
			</div>
		</div>
	</div>
</section>
@stop
