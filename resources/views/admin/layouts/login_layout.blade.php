<!DOCTYPE html>
<html>
	<head>
		<title><?php echo Config::get("Site.title"); ?></title>
		<link href="{{WEBSITE_CSS_URL}}admin/bootstrap.min.css" rel="stylesheet">
		<link href="{{WEBSITE_CSS_URL}}admin/ionicons.min.css" rel="stylesheet">	
		<link href="{{WEBSITE_CSS_URL}}admin/font-awesome.min.css" rel="stylesheet">
		<link href="{{WEBSITE_CSS_URL}}admin/notification/jquery.toastmessage.css" rel="stylesheet">
		<link href="{{WEBSITE_CSS_URL}}admin/AdminLTE.css" rel="stylesheet">
		<script src="{{WEBSITE_JS_URL}}admin/jquery.min.js"></script>
		<script src="{{WEBSITE_JS_URL}}admin/jquery.slimscroll.min.js"></script>
		<link rel="shortcut icon" href="{{WEBSITE_IMG_URL}}fav.png">
	</head>
	<body class="hold-transition login-page  pace-done">
		@if(Session::has('error'))
			<div class="toast-container toast-position-top-right">
				<div class="toast-item-wrapper">
					<div class="toast-item toast-type-error" style="">
						<div class="toast-item-image toast-item-image-error"></div>
						<div class="toast-item-close"></div>
						<p>{{ Session::get('error') }}</p>
					</div>
				</div>
			</div>
			
		@endif
		
		@if(Session::has('success'))
			<div class="toast-container toast-position-top-right">
				<div class="toast-item-wrapper">
					<div class="toast-item toast-type-success" style="">
						<div class="toast-item-image toast-item-image-success"></div>
						<div class="toast-item-close"></div>
						<p>{{ Session::get('success') }}</p>
					</div>
				</div>
			</div>
		@endif

		@if(Session::has('flash_notice'))
			<div class="toast-container toast-position-top-right">
				<div class="toast-item-wrapper">
					<div class="toast-item toast-type-success" style="">
						<div class="toast-item-image toast-item-image-success"></div>
						<div class="toast-item-close"></div>
						<p>{{ Session::get('flash_notice') }}</p>
					</div>
				</div>
			</div>
		@endif
		@yield('content')
		
		<script src="{{WEBSITE_JS_URL}}admin/bootbox.js"></script>
		<script src="{{WEBSITE_JS_URL}}admin/core/mws.js"></script>
		<script src="{{WEBSITE_JS_URL}}admin/core/themer.js"></script>
		<script src="{{WEBSITE_JS_URL}}admin/bootstrap.js"></script>
		<script src="{{WEBSITE_JS_URL}}admin/app.js"></script>
		<style type="text/css">
			.error-message{
				color:#f56954 !important;
			}
		</style>
		<script>
			$(document).ready(function() {
				$('.toast-item-close').click(function() {
					$(this).parents('.toast-container').fadeOut();
				});
			});
		</script>
	</body>
</html>