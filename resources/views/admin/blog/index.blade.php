
@extends('admin.layouts.default')
@section('content')
{{ HTML::style('css/admin/bootstrap-datetimepicker.css') }}
{{ HTML::script('js/admin/moment.js') }}
{{ HTML::script('js/admin/bootstrap-datetimepicker.js') }} 
<?php $blogcategory = ['1' => 'DIABETES MANAGEMENT', '2' => 'WEIGHT MANAGEMENT']; ?>
<script>
	jQuery(document).ready(function(){
		$('#start_from').datetimepicker({
			format: 'YYYY-MM-DD'
		}); 
		$('#start_to').datetimepicker({
			format: 'YYYY-MM-DD'
		}); 
		
	});
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});
</script>
<section class="content-header">
	<h1>
	  {{ trans("Blog Management") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Blog Management") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			{{ Form::open(['method' => 'get','role' => 'form','url' => "adminpnlx/blog",'class' => 'row mws-form']) }}
			{{ Form::hidden('display') }}
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('title',((isset($searchVariable['title'])) ? $searchVariable['title'] : ''), ['class' => 'form-control','placeholder'=>'Title']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('posted_by',((isset($searchVariable['posted_by'])) ? $searchVariable['posted_by'] : ''), ['class' => 'form-control','placeholder'=>'Posted By']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('date_from',((isset($searchVariable['date_from'])) ? $searchVariable['date_from'] : ''), ['class' => 'form-control','id'=>'start_from','placeholder'=>trans('Date From')]) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('date_to',((isset($searchVariable['date_to'])) ? $searchVariable['date_to'] : ''), ['class' => 'form-control','id'=>'start_to','placeholder'=>trans('Date To')]) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
				    {{ Form::select('category_id',$blogcategory,isset($searchVariable['category_id'])?$searchVariable['category_id']:'',['class' => 'form-control','placeholder'=>'Choose Blog Category']) }}
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('adminpnlx/blog')}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
			</div>
			
			{{ Form::close() }} 
		</div>
		
	</div> 
	<div class="box">
		<div class="box-body ">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title">{{ $sectionName }}'s List</h3>
				<div class="listing-btns">
					<a href="{{URL::to('adminpnlx/blog/add-blog')}}" class="btn btn-success btn-small align">{{ trans("Add New Blog") }} </a>
				</div> 
			</div>
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="10%">
							{{
								link_to_route(
								"blog.listBlog",
								trans("Diabetes Type"),
								array(
									'sortBy' => 'blog_name',
									'order' => ($sortBy == 'blog_name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'blog_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'blog_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<!--<th width="10%">
							{{
								trans("Banner Video Embedded Url")
							}}
						</th>-->
						
						<th width="10%">
							{{
								link_to_route(
								"blog.listBlog",
								trans("Posted By"),
								array(
									'sortBy' => 'posted_by',
									'order' => ($sortBy == 'posted_by' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'posted_by' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'posted_by' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">
							{{
								link_to_route(
								"blog.listBlog",
								trans("Title"),
								array(
									'sortBy' => 'title',
									'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<?php 
						/*
						<th width="20%">
							{{
								trans("Article")
							}}
						</th>
						<th width="5%">
							{{
								trans("Popular")
							}}
						</th>*/ ?>
						<th>
							{{
								link_to_route(
									"blog.listBlog",
									trans("Registered On"),
									array(
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="10%">
							{{
								trans("Status")
							}}
						</th>
						<th>
							Action
						</th>
					</tr>
				</thead>
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)
						<tr>
							<td>
								@if(!empty($record->category_id == 1))
								{{ 'DIABETES MANAGEMENT' }}
								@elseif(!empty($record->category_id == 2))
								{{ 'WEIGHT MANAGEMENT' }}
								@endif
								<?php
								/*
								{{ $record->blog_name }}
								@if($record->blog_type == 'embedded')
									
									
										$path_url 		=	str_replace("embed/","watch?v=",$record->banner_video);
										$path			=	str_replace("watch?v=","embed/",$record->banner_video);
									
									<iframe style="width:100px;height:100px" src="{{$path}}" frameborder="0"></iframe>
								@else
									@if($record->banner_image != '')
										<div class="usermgmt_image">
											<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo config('global.BLOG_IMAGE_URL').$record->banner_image; ?>">
											<img src="{{ url('public/blog/'.$record->banner_image) }}">
											</a>
										</div>
									@else
										<img src="<?php echo WEBSITE_IMG_URL ?>admin/no_image.jpg" style="height:75px;width:75px;">
									@endif
								@endif*/
								?>
							</td>
							<?php /*<td>
								@if(!empty($record->banner_video))
									{{ $record->banner_video }}
								@endif 
							</td> */ ?>
							<?php /* <td>
								@if($record->banner_video != '' && File::exists(BLOG_IMAGE_ROOT_PATH.$record->banner_video))
									<div class="usermgmt_image">
										<a>
											<video width="100" controls>
											  <source src="<?php echo BLOG_IMAGE_URL.'/'.$record->banner_video ?>" type="video/mp4">
											  <source src="<?php echo BLOG_IMAGE_URL.'/'.$record->banner_video ?>" type="video/ogg">
											</video>
										</a>
									</div>
								@else
									<img src="<?php echo WEBSITE_IMG_URL ?>no_video.jpg" style="height:75px;width:75px;">
								@endif
							</td> */?>
							
							<td>
								{{ $record->posted_by }}
							</td>
							<td>
								{{ $record->title }}
							</td>
							<?php /*
							<td>
								{!! strip_tags(Str::limit($record->description, 250)) !!}
							</td>
							
							/*<td>
								{{ $record->is_popular }}
							</td>*/ ?>
							<td data-th='REGISTERED ON'>{{ date(config::get("Reading.date_format"),strtotime($record->created_at)) }}</td>
							<td>
								@if($record->is_active	==	0)
									<label class="label label-warning">Deactivated</label>
								@else
									<label class="label label-success">Activated</label>
								@endif
							</td>
							<td>
								<a href="{{URL::to('adminpnlx/blog/view-blog/'.$record->id)}}" title="{{ trans('View') }}" class="btn btn-info">
									<i class="fa fa-eye"></i>
								</a>
								<a title="{{ trans('Edit') }}" href="{{URL::to('adminpnlx/blog/edit-blog/'.$record->id)}}" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								<a title="{{ trans('Delete') }}" href="{{ URL::to('adminpnlx/blog/delete-blog/'.$record->id) }}"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a>
								<!--<a title="{{ trans('Comments') }}" href="{{ URL::to('adminpnlx/blog/comment-blog/'.$record->id) }}"  class=" btn btn-success">
									<i class="fa fa-comment"></i>
								</a>-->
								
								@if($record->is_active == 1)
									<a  title="Click To Deactivate" href="{{URL::to('adminpnlx/blog/update-status/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item" style="margin-top:3px;"><span class="fa fa-ban"></span>
									</a>
								@else
									<a title="Click To Activate" href="{{URL::to('adminpnlx/blog/update-status/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item" style="margin-top:3px;"><span class="fa fa-check"></span>
									</a> 
								@endif 
								
								<?php /* @if($record->is_popular == 1)
									<a  title="Click To Unpopular" href="{{URL::to('adminpnlx/blog/change-status/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item" style="margin-top:3px;"><span class="fa fa-ban"></span>
									</a>
								@else
									<a title="Click To Popular" href="{{URL::to('adminpnlx/blog/change-status/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item" style="margin-top:3px;"><span class="fa fa-check"></span>
									</a> 
								@endif  */?>
								
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="7" >{{ trans("No record found") }}</td>
						</tr>
					@endif 
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script src="{{WEBSITE_JS_URL}}admin/lightbox.js"></script>
@stop