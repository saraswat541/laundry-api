@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		 {{ trans("View Blog Details") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/blog')}}">{{ trans("Blog Management") }}</a></li>
		<li class="active"> {{ trans("View Blog Details") }}  </li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">BLOG INFORMATION</h3>				
			</div>
			<div class="">
				<table class="table table-striped" style="margin-top:10px;">
					
					<tbody>
						<?php 
							$image		=	isset($blogDetails->banner_image) ? $blogDetails->banner_image : '';
						?>
						@if($image != '')
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Banner Image:</th>
							<td data-th='Banner Image'>
								
								
									<a class="fancybox-buttons" data-fancybox-group="button" href="{{ BLOG_IMAGE_URL.$blogDetails->banner_image }}">
										<div class="usermgmt_image">
											<img src="{{ BLOG_IMAGE_URL.$blogDetails->banner_image }}" width="150" height="100" />
										</div>
									</a>
							</td>
						</tr>
						@endif
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Banner Video Embedded Url :</th>
							<td data-th='Title' style="font-size:14px;">{{ isset($blogDetails->banner_video) ? $blogDetails->banner_video :'' }}</td>
						</tr>
						
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Blog Category Name:</th>
							<?php /*<td data-th='Blog Category Name' style="font-size:14px;">{{ isset($blogDetails->category_name) ? $blogDetails->category_name :'' }}</td>*/ ?>
							<td data-th='Blog Category Name' style="font-size:14px;">
							@if(!empty($blogDetails->category_id == 1))
								{{ 'DIABETES MANAGEMENT' }}
								@elseif(!empty($blogDetails->category_id == 2))
								{{ 'WEIGHT MANAGEMENT' }}
								@endif
							</td>
						</tr>
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Title:</th>
							<td data-th='Title' style="font-size:14px;">{{ isset($blogDetails->title) ? $blogDetails->title :'' }}</td>
						</tr>
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Posted By:</th>
							<td data-th='Posted By' style="font-size:14px;">{{ isset($blogDetails->posted_by) ? $blogDetails->posted_by :'' }}</td>
						</tr>
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Article:</th>
							<td data-th='Description' style="font-size:14px;">{!! isset($blogDetails->description) ? $blogDetails->description :'' !!}</td>
						</tr>
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Meta Key:</th>
							<td data-th='Description' style="font-size:14px;">{!! isset($blogDetails->metakey) ? $blogDetails->metakey :'' !!}</td>
						</tr>
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Meta Description:</th>
							<td data-th='Description' style="font-size:14px;">{!! isset($blogDetails->metadescription) ? $blogDetails->metadescription :'' !!}</td>
						</tr>
						<tr>
							<?php /*<th  width="30%" class="text-right" style="font-size:14px;">Is Active:</th>
							<td data-th='Is Active' style="font-size:14px;">
								@if($blogDetails->is_active	==	0)
									{{trans("No")}}
								@else
									{{trans("Yes")}}
								@endif
							</td>
						</tr>
						*/ ?>
						<?php /*
						<tr>
							<th  width="30%" class="text-right" style="font-size:14px;">Is Popular:</th>
							<td data-th='Is Popular' style="font-size:14px;">
								{{ isset($blogDetails->is_popular) ? $blogDetails->is_popular :'' }}
							</td>
						</tr> */ ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
@stop