@extends('admin.layouts.default')
@section('content')
<!-- CKeditor start here-->
{{ HTML::script('js/admin/ckeditor/ckeditor.js') }}
{{ HTML::style('css/jquery.tagit.css') }}
{{ HTML::script('js/jquery-ui-1.10.4.custom.min.js') }}
{{ HTML::script('js/tag-it.js') }}
<!-- CKeditor ends-->
<?php $blogcategory = ['1' => 'DIABETES MANAGEMENT', '2' => 'WEIGHT MANAGEMENT']; ?>
<section class="content-header">
	<h1>
		 {{ trans("Edit Blog") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/blog')}}">{{ trans("Blog Management") }}</a></li>
		<li class="active">{{ trans("Edit Blog") }} </li>
	</ol>
</section>
<section class="content">
  <div class="box">
   	 <div class="box-body"> 
			{{ Form::open(['role' => 'form','url' => 'adminpnlx/blog/edit-blog/'.$userDetails->id,'class' => 'mws-form','files'=>'true']) }}
			<div class="row ">
				<?php /* 
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('category_id')) ? 'has-error' : ''; ?>">
						{{ Form::label('category_id',trans("Diabetes Type"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
						<div class="mws-form-item">
							{{ Form::select('category_id',$blogCategoryList,isset($userDetails->category_id)?$userDetails->category_id:'',['class' => 'form-control','placeholder'=>'Choose Diabetes Type']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('category_id'); ?>
							</div>
						</div>
					</div>
				</div> */ ?>
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('category_id')) ? 'has-error' : ''; ?>">
						{{ Form::label('category_id',trans("Blog Category"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
						<div class="mws-form-item">
							{{ Form::select('category_id',$blogcategory,isset($userDetails->category_id)?$userDetails->category_id:'',['class' => 'form-control','placeholder'=>'Choose Blog Category']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('category_id'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('title')) ? 'has-error' : ''; ?>">
						{{ Form::label('title',trans("Title"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
						<div class="mws-form-item">
							{{ Form::text('title',isset($userDetails->title)?$userDetails->title:'',['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('title'); ?>
							</div>
						</div>
					</div>
				</div>
				
					<?php /* <div class="form-group <?php echo ($errors->first('designation')) ? 'has-error' : ''; ?>">
						{{ HTML::decode( Form::label('designation', trans("Designation").'<span class="requireRed"> * </span>:', ['class' => 'mws-form-label'])) }}
						<div class="mws-form-item">
							{{ Form::text('designation',isset($userDetails->designation)?$userDetails->designation:'',['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('designation'); ?>
							</div>
						</div>
					</div> */ ?>
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('posted_by')) ? 'has-error' : ''; ?>">
						{{ Form::label('posted_by', trans("Posted By"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
						<div class="mws-form-item">
							{{ Form::text('posted_by',isset($userDetails->posted_by)?$userDetails->posted_by:'',['class'=>'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('posted_by'); ?>
							</div>
						</div>
					</div>
				</div>
					<?php /*
					<div class="form-group <?php echo ($errors->first('is_popular')) ? 'has-error' : ''; ?>">
						{{ Form::label('metadescription', trans("Popular"), ['class' =>  'mws-form-label']) }}<span class="requireRed"> * </span>
						<div class="mws-form-item">
							{{ Form::text("is_popular",isset($userDetails->is_popular)?$userDetails->is_popular:'', 
							['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('is_popular'); ?>
							</div>
						</div>
					</div> */ ?>
					<div class="col-md-6">	
						
							<div class="form-group <?php echo ($errors->first('blog_type')) ? 'has-error' : ''; ?>">
								<div class="mws-form-row">
									{{ Form::label('blog_type', trans("Blog Type"), ['class' => 'mws-form-label']) }}<!-- <span class="requireRed"> * </span> -->
									<div class="mws-form-item">
										<span class="styled-selectors">
											{{ Form::radio('blog_type', 'image',($userDetails->blog_type=='image'?true:''),['class'=>'blog_type']) }}
											<label for="confrm1">Banner Image</label>
										</span>
										<span class="styled-selectors">
											{{ Form::radio('blog_type', 'embedded',($userDetails->blog_type=='embedded'?true:''),['class'=>'blog_type']) }}
											<label for="confrm1">Video Embedded Url</label>
										</span>
										<div class="error-message help-inline">
											<?php echo $errors->first('blog_type'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group embedded_div <?php echo ($errors->first('embedded_url')) ? 'has-error' : ''; ?>">
									<div class="mws-form-row">
										{{ Form::label('embedded_url', trans("Video Embedded Url"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
										<div class="mws-form-item">
											{{ Form::text('embedded_url',isset($userDetails->banner_video)?$userDetails->banner_video:'',['class'=>'form-control']) }}
											<span>Example: https://www.youtube.com/watch?v=RT3fmiODWxI</span>
											<div class="error-message help-inline">
												<?php echo $errors->first('embedded_url'); ?>
											</div>
										</div>
									</div>
								</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group embedded_image<?php echo ($errors->first('banner_image')) ? 'has-error' : ''; ?>">
							{{ Form::label('banner_image', trans("Banner Image"),  ['class' => 'mws-form-label floatleft']) }}
							<span class='tooltipHelp' title="" data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo "The attachment must be a file of type:".IMAGE_EXTENSION; ?>" style="cursor:pointer;">
								<i class="fa fa-question-circle fa-2x"> </i>
							</span>
							<div class="mws-form-item">
								{{ Form::file('banner_image') }}
								<br />
								<?php 
									//$oldImage	=	$request->old('banner_image');
								   $oldImage = '';
									$image		=	isset($oldImage) ? $oldImage : $userDetails->banner_image;
								?>
								@if($userDetails->banner_image != '')
									<a class="fancybox-buttons" data-fancybox-group="button" href="{{ BLOG_IMAGE_URL.$userDetails->banner_image }}">
										<div class="usermgmt_image">
											<img  src="{{ BLOG_IMAGE_URL.$userDetails->banner_image }}" width="100" height="50">
										</div>
									</a>
								@else
									<a class="fancybox-buttons" data-fancybox-group="button" width="150px" height="150px" href="<?php echo WEBSITE_IMG_URL ?>admin/no_image.jpg">
										<div class="usermgmt_image">
											<img class="img-circle" width="150px" height="150px" src="<?php echo WEBSITE_IMG_URL ?>admin/no_image.jpg">
										</div>
									</a>
								@endif
							</div>
							<div class="error-message help-inline">
									<?php echo $errors->first('banner_image'); ?>
							</div>
						</div>
						<div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
								{{ Form::label('description', trans("Article"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
								<div class="mws-form-item">
									{{ Form::textarea("description",isset($userDetails->description)?$userDetails->description:'',
									['class' => 'form-control textarea_resize','id'=>'Description',"rows"=>3,"cols"=>3]) }}
									<script type="text/javascript">
									/* For CKEDITOR */
										CKEDITOR.replace( <?php echo 'Description'; ?>,
										{
											height: 250,
											width: 550,
											filebrowserUploadUrl : 'http://localhost/myndaro/ckeditorimageupload.php',
											filebrowserImageWindowWidth : '640',
											filebrowserImageWindowHeight : '480',
											enterMode : CKEDITOR.ENTER_BR
										});
										CKEDITOR.config.allowedContent = true;				
									</script>
									<div class="error-message help-inline">
										<?php echo $errors->first('description'); ?>
									</div>
								</div>
						</div>
						<div class="form-group <?php echo ($errors->first('metakey')) ? 'has-error' : ''; ?>">
								{{ Form::label('metakey', trans("Meta Key"), ['class' => 'mws-form-label']) }}<!-- <span class="requireRed"> * </span> -->
								<div class="mws-form-item">
									{{ Form::textarea("metakey",isset($userDetails->metakey)?$userDetails->metakey:'',
									['class' => 'form-control textarea_resize','id'=>'metakey',"rows"=>3,"cols"=>3]) }}
									
									<div class="error-message help-inline">
										<?php echo $errors->first('metakey'); ?>
									</div>
								</div>
						</div>
						<div class="form-group <?php echo ($errors->first('metadescription')) ? 'has-error' : ''; ?>">
								{{ Form::label('metadescription', trans("Meta Description"), ['class' => 'mws-form-label']) }}<!-- <span class="requireRed"> * </span> -->
								<div class="mws-form-item">
									{{ Form::textarea("metadescription",isset($userDetails->metadescription)?$userDetails->metadescription:'',
									['class' => 'form-control textarea_resize','id'=>'metadescription',"rows"=>3,"cols"=>3]) }}
									
									<div class="error-message help-inline">
										<?php echo $errors->first('metadescription'); ?>
									</div>
								</div>
						</div>
					</div>
				</div>
			<div class="mws-button-row">
				<div class="input" >
					<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
					<a href="{{URL::to('adminpnlx/blog/edit-blog/'.$userDetails->id)}}" class="btn btn-primary"> {{ trans("Reset") }}</a>
					<a href="{{URL::to('adminpnlx/blog')}}" class="btn btn-info"> {{ trans("Cancel") }}</a>
				</div>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</section>
<script>
	$(function(){
		var type = $(".blog_type:checked").val();
		if(type == 'image'){
			$(".embedded_image").show();
			$(".embedded_div").hide();
		}
		if(type == 'embedded'){
			$(".embedded_div").show();
			$(".embedded_image").hide();
		}	
	});
	$(".blog_type").click(function(){
		var type = $(".blog_type:checked").val();
		if(type == 'embedded'){
			$(".embedded_div").show();
			$(".embedded_image").hide();
		}
		if(type == 'image'){
			$(".embedded_div").hide();
			$(".embedded_image").show();
		}
	});
</script>
@stop
