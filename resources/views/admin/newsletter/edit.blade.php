@extends('admin.layouts.default')

@section('content')

<!-- chosen select box css and js start here-->
{{ HTML::style('css/chosen.css') }}
{{ HTML::script('js/chosen.jquery.js') }}
<!-- chosen select box css and js end here-->

<!-- ckeditor js start here-->
{{ HTML::script('js/admin/ckeditor/ckeditor.js') }}
<!-- ckeditor js end here-->

<!-- datetime picker js and css start here-->
{{ HTML::script('js/admin/jui/js/jquery-ui-1.9.2.min.js') }}
{{ HTML::script('js/admin/jui/js/timepicker/jquery-ui-timepicker.min.js') }}
{{ HTML::script('js/admin/prettyCheckable.js') }}
{{ HTML::style('css/admin/jui/css/jquery.ui.all.css') }}
{{ HTML::style('css/admin/prettyCheckable.css') }}

<!-- date time picker js and css and here-->
<!-- chosen select box css and js start here-->
{{ HTML::style('css/admin/chosen.min.css') }}
{{ HTML::script('js/admin/chosen.jquery.min.js') }}
<!-- chosen select box css and js end here-->

<script type="text/javascript">

/* For datetimepicker */
	$(function(){
		 
		$(".chzn-select").chosen();
		$('#scheduled_time').datetimepicker({ 
			timeFormat: "hh:mm",
			dateFormat: 'yy-mm-dd',
			//ampm: true,
			minDate: new Date(<?php echo date('Y,m-1,d,H,i');  ?>),
			//changeMonth: true,
			//changeYear : true,
		});	
		
	});
	
</script>	
<section class="content-header">
	<h1>
		Edit Scheduled Newsletter
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/news-letter')}}">Newsletter</a></li>
		<li class="active">Eidt Newsletter</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
		{{ Form::open(['role' => 'form','url' => 'adminpnlx/news-letter/edit-template/'.$result->id,'class' => 'mws-form']) }}
		<div class="row">
			<div class="col-md-6">
				<div class="mws-form-inline">
					<div class="form-group">
						{{  Form::label('scheduled_time', trans("Scheduled Time"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							{{ Form::text('scheduled_time', $result->scheduled_time, ['class' => 'form-control ','readonly']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('scheduled_time'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="mws-form-inline">
					<div class="form-group">
						{{  Form::label('subject', trans("Subject"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							{{ Form::text('subject', $result->subject, ['class' => 'form-control ']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('subject'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="mws-form-inline">
					<div class="form-group">
						{{  Form::label('newsletter_subscriber_id',trans("Select Subscriber"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							{{ Form::select('newsletter_subscriber_id[]',$subscriberArray ,$allReadySubscriberArray, ['class' => 'chzn-select' , 'style' => 'width:55%','data-placeholder'=>'Select Subscribers','multiple'=>'multiple']) }}
						
							<div class="error-message help-inline">
								<?php echo $errors->first('newsletter_subscriber_id'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="mws-form-inline">
					<div class="form-group">
						{{  Form::label('constant', trans("Constants"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							<div class="col-md-6">
							<?php $constantArray = Config::get('newsletter_template_constant'); ?>
							{{ Form::select('constant', $constantArray,'', ['id' => 'constants','empty' => 'Select one','class' => 'form-control small']) }}
							</div>
							<div class="col-md-6">
							<span style = "padding-left:20px;padding-top:0px; valign:top">
								<a onclick = "return InsertHTML()" href="javascript:void(0)" class="btn  btn-success no-ajax"><i class="icon-white "></i>{{ trans("Insert Variable") }} </a>
							</span>
							</div>
							<div class="error-message help-inline">
								<?php echo $errors->first('constants'); ?>
							</div>
							
						</div>
					</div>
				</div>
				<div class="mws-form-inline">
					<div class="form-group	">
						{{  Form::label('body', trans("Body"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							{{ Form::textarea("body",$result->body, ['class' => 'small','id' => 'body']) }}
							<span class="error-message help-inline">
								<?php echo $errors->first('body'); ?>
							</span>
						</div>
						<script type="text/javascript">
							// <![CDATA[
							CKEDITOR.replace( 'body',
							{
								height: 350,
								width: 600,
								filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
								filebrowserImageWindowWidth : '640',
								filebrowserImageWindowHeight : '480',
								enterMode : CKEDITOR.ENTER_BR
							});
							//]]>		
						</script>
					</div>
				</div>
				<div class="mws-button-row">
					<div class="input" >
						<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
						
						<a href="{{URL::to('adminpnlx/news-letter/edit-template/'.$result->id)}}" class="btn btn-info"><i class=\"icon-refresh\"></i>{{ trans('Reset') }}</a>
					</div>
				</div>
			</div>    	
		</div>    	
		{{ Form::close() }}
	</div>
	</div>
</section>
<script type='text/javascript'>
	/* this function insert defined onstant on button click */
	function InsertHTML() {
		var strUser = document.getElementById("constants").value;
		if(strUser != ''){
			var newStr = '{'+strUser+'}';
			var oEditor = CKEDITOR.instances["body"] ;
			oEditor.insertHtml(newStr) ;	
		}
    }
</script>

@stop
