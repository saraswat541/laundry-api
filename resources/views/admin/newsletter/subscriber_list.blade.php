@extends('admin.layouts.default')
@section('content')
<script type="text/javascript">
var action_url = '<?php echo WEBSITE_URL; ?>adminpnlx/news-letter/delete-multiple-subscriber';
 /* for equal height of the div */	
</script>
{{ HTML::script('js/admin/multiple_delete.js') }}
<section class="content-header">
	<h1>
		Newsletter Subscribers
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/news-letter/newsletter-templates')}}">Newsletter Templates</a></li>
		<li class="active">Newsletter Subscribers</li>
	</ol>
</section>

<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			{{ Form::open(['method' => 'get','role' => 'form','url' => 'adminpnlx/news-letter/subscriber-list','class' => 'row mws-form']) }}
			{{ Form::hidden('display') }}
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					{{ Form::text('email',((isset($searchVariable['email'])) ? $searchVariable['email'] : ''), ['class' => 'form-control','placeholder'=>'Email']) }}
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('adminpnlx/news-letter/subscriber-list')}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> {{ trans('Reset') }}</a>
			</div>
			{{ Form::close() }}
		</div>
	</div>
	
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title">Newsletter Subscribers's List</h3>
				<div class="listing-btns">
					<a href="{{URL::to('adminpnlx/news-letter/add-subscriber')}}"  class="btn btn-success btn-small align">{{ trans("Add Subscriber") }} </a>
					<a href="{{ route('Subscriber.export') }}" class="btn btn-success btn-small align">{{ 'Export CSV' }} </a>
				</div>
			</div>
			<table class="table table-hover table-striped">
				<thead>
				<tr>
					<th>
						{{
							link_to_route(
							'Subscriber.subscriberList',
							trans("Email"),
							array(
								'sortBy' => 'email',
								'order' => ($sortBy == 'email' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
							),
							array('class' => (($sortBy == 'email' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'email' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<!-- <th >
						{{
							link_to_route(
							'Subscriber.subscriberList',
							trans("Name"),
							array(
								'sortBy' => 'name',
								'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
							),
							array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')))
							)
						}}
					</th>  -->
					<th >
						{{
							link_to_route(
							'Subscriber.subscriberList',
							trans("Created"),
							array(
								'sortBy' => 'created_at',
								'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
							),
							array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<th>{{ trans("Action") }}</th>
				</tr>
				</thead>
				<tbody  id="powerwidgets">
				@if(!$result->isEmpty())
				@foreach($result as $record)
				<tr class="items-inner">
					<td data-th='{{ trans("Email") }}'>{{ $record->email }}</td>
					<!-- <td>{{ $record->name }}</td>  -->
					<td data-th='{{ trans("Created") }}'>{{ date(Config::get("Reading.date_format"),strtotime($record->created_at)) }}</td>
					<td data-th='{{ trans("Action") }}'>
						<a title="{{ trans('Delete') }}" href="{{URL::to('adminpnlx/news-letter/subscriber-delete/'.$record->id)}}"  class="delete_any_item btn btn-danger btn-small delete_user no-ajax"> <span class="fa fa-trash-o"></span> </a>
					</td>
				</tr>
				 @endforeach
				 @else
				<tr>
					<td class="alignCenterClass" colspan="4" >{{ trans("No record found message") }}</td>
				</tr>
				@endif
			</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div>
</section>
@stop
