@extends('admin.layouts.default')
@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Edit {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Edit {{ $sectionNameSingular }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			{{ Form::open(['role' => 'form','url' =>  route("$modelName.edit",$model->id),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
				<div class="row">
				
					<div class="col-md-6">
						@if(count($languages) > 1)
						<div  class="default_language_color">
							{{ Config::get('default_language.message') }}
						</div>
						<div class="wizard-nav wizard-nav-horizontal">
							<ul class="nav nav-tabs">
								<?php $i = 1 ; ?>
								@foreach($languages as $value)
								<li class=" {{ ($i ==  $language_code )?'active':'' }}">
									<a data-toggle="tab" href="#{{ $i }}div">
										{{ $value->section_name }}
									</a>
								</li>
								<?php $i++; ?>
								@endforeach
							</ul>
						</div>
						@endif
						@if(count($languages) > 1)
						<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
							<hr class ="hrLine"/>
							<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
						</div>
						@endif
						<div class="mws-panel-body no-padding tab-content">
							<?php $i = 1 ; ?>
							@foreach($languages as $value)
							<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
								<div class="mws-form-inline">
									<div class="form-group <?php if($i == 1){ echo ($errors->first('section_name')?'has-error':'');} ?>">
										@if($i == 1)
										{!! HTML::decode( Form::label($i.'.section_name',trans("Section Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										@else
										{!! HTML::decode( Form::label($i.'.section_name',trans("Section Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
										@endif
										<div class="mws-form-item">
											{{ Form::text("data[$i][section_name]",$model->section_name, ['class' => 'form-control']) }}
											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('section_name') : ''; ?>
											</div>
										</div>
									</div>						
								</div>
							</div> 
							<?php $i++ ; ?>
							@endforeach	
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('title')?'has-error':''); ?>">
							{!! HTML::decode( Form::label('title',trans("Title").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('title',$model->title, ['class' => 'form-control']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('title'); ?>
								</div>
							</div>
						</div>	
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
							{{ Form::label('description',trans("Description"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
							<div class="mws-form-item">
								{{ Form::textarea("description",$model->description, ['class' => 'form-control textarea_resize','id' => 'description' ,"rows"=>3,"cols"=>3]) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('description'); ?>
								</div>
								<script type="text/javascript">
										/* For CKEDITOR */
										CKEDITOR.replace( <?php echo 'description'; ?>,
										{
											height: 250,
											filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
											filebrowserImageWindowWidth : '640',
											filebrowserImageWindowHeight : '480',
											enterMode : CKEDITOR.ENTER_BR
										});
								</script>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('color_code')?'has-error':''); ?>">
							{!! HTML::decode( Form::label('color_code',trans("Color Code").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('color_code',$model->color_code, ['class' => 'form-control']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('color_code'); ?>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group <?php echo ($errors->first('image')?'has-error':''); ?>">
							<div class="mws-form-row">
								{!! HTML::decode( Form::label('image', trans("Image").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
								<div class="mws-form-item">
									{{ Form::file('image', ['class' => '']) }}
									<div class="error-message help-inline">
										<?php echo $errors->first('image'); ?>
									</div>
									@if($model->image != "")
									<br />
									<img height="50" width="50" src="{{ CONTENT_IMAGE_URL.$model->image }}" />
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="mws-button-row">
					<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
					<a href='{{ route("$modelName.edit",$model->id)}}' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
					<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
				</div>
			{{ Form::close() }} 
		</div>
	</div>
</section>
@stop