@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		View {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">View {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							<tr>
								<th width="30%" class="text-right txtFntSze">Section Name</th>
								<td data-th='Section Name' class="txtFntSze">{{ isset($model->section_name) ? $model->section_name :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Title</th>
								<td data-th='Title' class="txtFntSze">{{ isset($model->title) ? $model->title :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Description</th>
								<td data-th='Description' class="txtFntSze">{!! isset($model->description) ? $model->description :'' !!}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Image</th>
								<td data-th='Image' class="txtFntSze">
									@if($model->image != '')
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo CONTENT_IMAGE_URL.$model->image; ?>"><img height="50" width="50" src="{{ CONTENT_IMAGE_URL.$model->image }}" style="width: 50px"></a>
									@endif
								</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Color Code</th>
								<td data-th='Color Code' class="txtFntSze">{{ isset($model->color_code) ? $model->color_code :'' }}</td>
							</tr>
							
							<tr>
								<th width="30%" class="text-right txtFntSze">Status</th>
								<td data-th='Status' class="txtFntSze">
									@if($model->is_active	==1)
										<span class="label label-success" >{{ trans("Activated") }}</span>
									@else
										<span class="label label-warning" >{{ trans("Deactivated") }}</span>
									@endif
								</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@stop