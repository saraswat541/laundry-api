@extends('admin.layouts.default')
@section('content')
<script type="text/javascript"> 
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});	
</script>
<section class="content-header">
	<h1>
	  {{ trans("Manage FAQ") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Manage FAQ") }}</li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
		{{ Form::open(['method' => 'get','role' => 'form','url' => 'adminpnlx/faqs-manager','class' => 'row mws-form']) }}
		{{ Form::hidden('display') }}
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					{{ Form::text('question',((isset($searchVariable['question'])) ? $searchVariable['question'] : ''), ['class' => 'form-control','placeholder'=>'Question']) }}
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					{{ Form::text('answer',((isset($searchVariable['answer'])) ? $searchVariable['answer'] : ''), ['class' => 'form-control','placeholder'=>'Answer']) }}
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('adminpnlx/faqs-manager')}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> {{ trans("Clear Search") }}</a>
			</div>
		{{ Form::close() }}
		<div class="col-md-3 col-sm-3 ">
			<div class="form-group pull-right">  
				
			</div>
		</div>
	</div> 
	</div> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title">Faq's List</h3>
				<div class="listing-btns">
					<a href="{{URL::to('adminpnlx/faqs-manager/add-faqs')}}" class="btn btn-success btn-small align">{{ trans("Add FAQ") }} </a>
				</div>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="25%">
							{{
								link_to_route(
								'Faq.listFaq',
								trans("Question"),
								array(
									'sortBy' => 'question',
									'order' => ($sortBy == 'question' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'question' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'question' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="25%">
							{{
								link_to_route(
								'Faq.listFaq',
								trans("Answer"),
								array(
									'sortBy' => 'answer',
									'order' => ($sortBy == 'answer' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'answer' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'answer' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<?php /* <th width="15%">{{ 'Category Name' }}</th> */ ?>
						<th width="10%" >
							{{
								link_to_route(
								'Faq.listFaq',
								trans("Status"),
								array(
									'sortBy' => 'is_active',
									'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}</th>
						<th >{{ trans("Action") }}</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					@if(!$result->isEmpty())
					@foreach($result as $record)
					
				<?php //if(!empty($record->category)){ pr($record['category']->name);}else{ echo "sdgdfh";}die;?>
						
						<tr>
							<td data-th="{{ trans('question') }}">{{ $record->question }}</td>
							<td data-th="{{ trans('answer') }}">{{ strip_tags(Str::limit($record->answer, 220)) }}</td>
							<?php /* <td data-th='Category Name '>{{ isset($record['category']->name)?$record['category']->name:" " }}</td> */ ?>
							<td data-th='Category Name '>
								@if($record->is_active)
									<label class="label label-success">Activated</label>
								@else
									<label class="label label-warning">Deactivated</label>
								@endif
							</td>
							<td data-th="{{ trans('action') }}">
								@if($record->is_active == 1)
									<a  title="Click To Deactivate" href="{{URL::to('adminpnlx/faqs-manager/update-status/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
									</a>
								@else
									<a title="Click To Activate" href="{{URL::to('adminpnlx/faqs-manager/update-status/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
									</a> 
								@endif 
								
								<a title='{{ trans("Edit") }}' href="{{URL::to('adminpnlx/faqs-manager/edit-faqs/'.$record->id)}}" class="btn btn-primary"><span class="fa fa-pencil"></span></a>
								
								<?php /* <a title='{{ trans("View") }}' href="{{URL::to('adminpnlx/faqs-manager/view-faqs/'.$record->id)}}" class="btn btn-info"><span class="fa fa-eye"></span></a> */ ?>
								
								<a title='{{ trans("Delete") }}' href="{{URL::to('adminpnlx/faqs-manager/delete-faqs/'.$record->id)}}" class="delete_any_item btn btn-danger"><span class="fa fa-trash-o"></span></a>
							</td>
						</tr>
					@endforeach 
					@else
						<tr>
							<td colspan="5" class="alignCenterClass" >
								{{ trans("No record found") }}
							</td>
						</tr>
					@endif 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
@stop
