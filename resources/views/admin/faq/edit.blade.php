@extends('admin.layouts.default')

@section('content')

<!-- CKeditor js and custom li js  strat here-->
{{ HTML::style('css/admin/custom_li_bootstrap.css') }}	
{{ HTML::script('js/bootstrap.js') }}
{{ HTML::script('js/admin/ckeditor/ckeditor.js') }}
<!-- CKeditor js and custom li js  end here--->
<section class="content-header">
	<h1>
		{{ trans("Edit Question") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/faqs-manager')}}">{{ trans("Manage FAQ") }}</a></li>
		<li class="active">{{ trans("Edit Question") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box box-primary">
		<div class="box-body">
	<div class="row ">
		<div class="col-md-6">	
			@if(count($languages) > 1)
				<div  class="default_language_color">
					{{ Config::get('default_language.message') }}
				</div>
				<div class="wizard-nav wizard-nav-horizontal">
					<ul class="nav nav-tabs">
						@foreach($languages as $value)
						<?php $i = $value -> id ; ?>
							<li class=" {{ ($i ==  $language_code )?'active':'' }}">
								<a data-toggle="tab" href="#{{ $i }}div">
									{{ $value -> title }}
								</a>
							</li>
							
						@endforeach
					</ul>
				</div>
			@endif
			{{ Form::open(['role' => 'form','url' => 'adminpnlx/faqs-manager/edit-faqs/'.$AdminFaq->id,'class' => 'mws-form']) }}
			{{ Form::hidden('id', $AdminFaq->id) }}
			<?php /* <div class="form-group <?php echo ($errors->first('category_id')) ? 'has-error' : ''; ?>">
				<div class="mws-form-row">
					{{ Form::label('topic',trans("Topic"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
					<div class="mws-form-item">
						{{ Form::select(
								'category_id',
								array(''=>'Select topic')+$listDownloadCategory,
								$AdminFaq->category_id,
								['class' => 'form-control']
							) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('category_id'); ?>
						</div>
					</div>
				</div>
			</div> 
			@if(count($languages) > 1)
				<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
					<hr class ="hrLine"/>
					<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
				</div>
			@endif	*/ ?>		
			<div class="mws-panel-body no-padding tab-content"> 
				@foreach($languages as $value)
				<?php $i = $value -> id ; ?>
				<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
						<div class="mws-form-inline">
							<div class="form-group <?php if($value -> id == 1) { echo ($errors->first('answer')) ? 'has-error' : ''; } ?>">
								@if($value -> id == 1)
									{{ Form::label($i.'.question',trans("Question"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
								@else
									{{ Form::label($i.'.question',trans("Question"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
								@endif
								<div class="mws-form-item">
									{{ Form::textarea("data[$i]['question']",isset($multiLanguage[$i]['question'])?$multiLanguage[$i]['question']:'', ['class' => 'form-control','id' => 'question_'.$i]) }}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('question') : ''; ?>
									</div>
								</div>
							</div>
							<div class="form-group <?php  if($value -> id == 1) {  echo ($errors->first('answer')) ? 'has-error' : '';} ?>">
								@if($value -> id == 1)
									{{ Form::label($i.'._answer',trans("Answer"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
								@else
									{{ Form::label($i.'._answer',trans("Answer"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
								@endif
								<div class="mws-form-item">
									{{ Form::textarea("data[$i]['answer']",isset($multiLanguage[$i]['answer'])?$multiLanguage[$i]['answer']:'', ['class' => 'form-control','id' => 'answer_'.$i]) }}
									<div class="error-message help-inline">
										<?php echo ($i ==  $language_code ) ? $errors->first('answer') : ''; ?>
									</div>
								</div>
								<script type="text/javascript">
								/*  CKEDITOR for question */
									
									CKEDITOR.replace( <?php echo 'question_'.$i; ?>,
									{
										height: 200,
										width: 507,
										filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
										filebrowserImageWindowWidth : '640',
										filebrowserImageWindowHeight : '480',
										enterMode : CKEDITOR.ENTER_BR
									});
									
									/*  CKEDITOR for answer */
									
									CKEDITOR.replace( <?php echo 'answer_'.$i; ?>,
									{
										height: 200,
										width: 507,
										filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
										filebrowserImageWindowWidth : '640',
										filebrowserImageWindowHeight : '480',
										enterMode : CKEDITOR.ENTER_BR
									});
										
								</script>
							</div>
						</div>
					</div>
				@endforeach
				<div class="mws-button-row">
					<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
					
					<a href="{{URL::to('adminpnlx/faqs-manager/edit-faqs/'.$AdminFaq->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
					
					<a href="{{URL::to('adminpnlx/faqs-manager')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
				</div>
			</div>
			{{ Form::close() }} 
		</div>
	</div>
	</div>
	</div>
</section>
@stop