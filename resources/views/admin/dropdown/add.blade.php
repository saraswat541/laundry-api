@extends('admin.layouts.default')

@section('content')
<section class="content-header">
	<h1>
		{{ 'Add New '.Str::studly($type) }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/dropdown-manager/'.$type)}}">{{{$type}}}</a></li>
		<li class="active">{{ 'Add New '.Str::studly($type) }}</li>
	</ol>
</section>

<section class="content">
	<div class="box">
		<div class="box-body"> 
			<div class="row">
				<div class="col-md-6">	
					@if(count($languages) > 1)
						<div  class="default_language_color">
							{{ Config::get('default_language.message') }}
						</div>
						<div class="wizard-nav wizard-nav-horizontal">
							<ul class="nav nav-tabs">
								@foreach($languages as $value)
									<?php $i = $value->id ; ?>
									<li class=" {{ ($i ==  $language_code )?'active':'' }}">
										<a data-toggle="tab" href="#{{ $i }}div">
											{{ $value -> title }}
										</a>
									</li>
								@endforeach
							</ul>
						</div>
					@endif
					
					{{ Form::open(['role' => 'form','url' => 'adminpnlx/dropdown-manager/add-dropdown/'.$type,'class' => 'mws-form','files' => true]) }}	
						<?php /* @if(count($languages) > 1)
							<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
								<b>{{ trans("messages.system_management.language_field") }}</b>
							</div>
						@endif */ ?>
						<div class="mws-panel-body no-padding tab-content"> 
							@foreach($languages as $value)
							<?php 
								$i = $value->id ;
							?>
							
								<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
									<div class="mws-form-inline">
										<div class="form-group <?php if($i == 1) { echo ($errors->first('name') ) ? 'has-error' : '';} ?>">
											@if($i == 1)
												{!! HTML::decode( Form::label('name',trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
											@else
												{!! HTML::decode( Form::label('name',trans("Name").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
											@endif
											<div class="mws-form-item">
												{{ Form::text("data[$i][name]",'', ['class' => 'form-control']) }}
												<div class="error-message help-inline">
													<?php echo ($i ==  $language_code ) ? $errors->first('name') : ''; ?>
												</div>
											</div>
										</div>
									</div>
								</div> 
							@endforeach
							<div class="mws-button-row">
								<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
								
								<a href="{{URL::to('adminpnlx/dropdown-manager/add-dropdown/'.$type)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
								
								<a href="{{URL::to('adminpnlx/dropdown-manager/'.$type)}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
							</div>
						</div>
					{{ Form::close() }} 
				</div>
			</div>
		</div>
	</div>
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
</style>
@stop
