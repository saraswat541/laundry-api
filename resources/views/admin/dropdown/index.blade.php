@extends('admin.layouts.default')
@section('content')
<script src="{{ WEBSITE_JS_URL }}adminpnlx/vendors/match-height/jquery.equalheights.js"></script>
<script type="text/javascript"> 
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});	
</script>
<section class="content-header">
	<h1>
		{{ Str::studly($type) }} Management
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ Str::studly($type) }} Management</li>
	</ol>
</section>
	
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
		{{ Form::open(['role' => 'form','url' => 'adminpnlx/dropdown-manager/'.$type,'class' => 'row mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					{{ Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => 'form-control','placeholder'=>Str::studly($type)]) }}
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href="{{URL::to('adminpnlx/dropdown-manager/'.$type)}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
			</div>
		{{ Form::close() }}
		</div>
	</div>
	<div class="box">
		<div class="box-body ">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title">{{ Str::studly($type) }}'s List</h3>
				<div class="listing-btns">
					<a href='{{route("$modelName.add",$type)}}'  class="btn btn-success btn-small pull-right"> {{ trans("Add New ") }}{{ Str::studly($type) }} </a>
				</div>
			</div>
			<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th width="25%">
						{{
							link_to_route(
								'DropDown.listDropDown',
								'Name',
								array(
									$type,
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
							   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<th width="30%">
						{{
							link_to_route(
								'DropDown.listDropDown',
								'Created ',
								array(
									$type,
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
							   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<th>
						{{
							link_to_route(
								'DropDown.listDropDown',
								'Status',
								array(
									$type,
									'sortBy' => 'is_active',
									'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
							   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<th>{{ 'Action' }}</th>
				</tr>
			</thead>
			<tbody id="powerwidgets">
				@if(!$result->isEmpty())
				@foreach($result as $record)
				<tr class="items-inner">
					<td data-th='Name'>{{ $record->name }}</td>
					<td data-th='Created At'>{{ date(Config::get("Reading.date_format") , strtotime($record->created_at)) }}</td>
					<td>
						@if($record->is_active	==1)
							<span class="label label-success" >{{ trans("Activated") }}</span>
						@else
							<span class="label label-warning" >{{ trans("Deactivated") }}</span>
						@endif
					</td>
					<td data-th='Action'>
						@if($record->is_active == 1)
							<a  title="Click To Deactivate" href="{{URL::to('adminpnlx/dropdown-manager/update-dropdown/'.$record->id.'/'.'0'.'/'.$type)}}" class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
							</a>
						@else
							<a title="Click To Activate" href="{{URL::to('adminpnlx/dropdown-manager/update-dropdown/'.$record->id.'/'.'1'.'/'.$type)}}" class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
							</a> 
						@endif 
						<a title="Edit" href="{{URL::to('adminpnlx/dropdown-manager/edit-dropdown/'.$record->id.'/'.$type)}}" class="btn btn-primary">
							<i class="fa fa-pencil"></i>
						</a>
						<a title="Delete" href="{{URL::to('adminpnlx/dropdown-manager/delete-dropdown/'.$record->id.'/'.$type)}}"  class="delete_any_item btn btn-danger">
							<i class="fa fa-trash-o"></i>
						</a> 
					</td>
				</tr>
				 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="4" >{{ trans("No record found") }}</td>
						</tr>
					@endif 
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
@stop
