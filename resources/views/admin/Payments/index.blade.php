@extends('admin.layouts.default')
@section('content')
<script>
	$(function(){
		/**
		 * For match height of div 
		 */
		 $('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		 var tooltips = $( "[title]" ).tooltip({
		 	position: {
		 		my: "right bottom+50",
		 		at: "right+5 top-5"
		 	}
		 });
		});
	</script>
<section class="content-header">
	<h1>
		{{ $sectionName }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li class="active"> {{ $sectionName }}</li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			{{ Form::open(['method' => 'get','role' => 'form','route' => "$modelName.index",'class' => 'row mws-form']) }}
			{{ Form::hidden('display') }}
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::select('is_active',array(''=>trans('All'),1=>trans('Active'),0=>trans('Inactive')),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('institute_name',((isset($searchVariable['institute_name'])) ? $searchVariable['institute_name'] : ''), ['class' => ' form-control','placeholder'=>'Institute Name']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('institute_email',((isset($searchVariable['institute_email'])) ? $searchVariable['institute_email'] : ''), ['class' => ' form-control','placeholder'=>'Email']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('institute_address',((isset($searchVariable['institute_address'])) ? $searchVariable['institute_address'] : ''), ['class' => ' form-control','placeholder'=>'Address']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					{{ Form::text('institute_number',((isset($searchVariable['institute_number'])) ? $searchVariable['institute_number'] : ''), ['class' => ' form-control','placeholder'=>'Contact No.']) }}
				</div>
			</div>
			
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href='{{ route("$modelName.index")}}' class="btn btn-primary"> <i class="fa fa-refresh "></i> {{ trans('Clear Search') }}</a>
			</div>
			{{ Form::close() }}
		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title">{{ $sectionName }}</h3>
				<div class="listing-btns">
					<a href='{{route("$modelName.add")}}'  class="btn btn-success btn-small pull-right"> {{ trans("Add New ") }}{{ $sectionNameSingular }} </a>
				</div>
			</div>
			
			{{ Form::open(['role' => 'form','route' => "$modelName.add",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
			<div class="row">
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('name',trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('name','', ['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('name'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					@if(count($languages) > 1)
					<div  class="default_language_color">
						{{ Config::get('default_language.message') }}
					</div>
					<div class="wizard-nav wizard-nav-horizontal">
						<ul class="nav nav-tabs">
							<?php $i = 1 ; ?>
							@foreach($languages as $value)
							<li class=" {{ ($i ==  $language_code )?'active':'' }}">
								<a data-toggle="tab" href="#{{ $i }}div">
									{{ $value -> institute_name }}
								</a>
							</li>
							<?php $i++; ?>
							@endforeach
						</ul>
					</div>
					@endif

					@if(count($languages) > 1)
					<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
						<hr class ="hrLine"/>
						<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
					</div>
					@endif
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						@foreach($languages as $value)
						<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
							<div class="mws-form-inline">
								<div class="form-group <?php if($i == 1){ echo ($errors->first('institute_name')?'has-error':'');} ?>">
									@if($i == 1)
									{!! HTML::decode( Form::label($i.'.institute_name',trans("Institute Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									@else
									{!! HTML::decode( Form::label($i.'.institute_name',trans("Institute Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
									@endif
									<div class="mws-form-item">
										{{ Form::text("data[$i][institute_name]",'', ['class' => 'form-control']) }}
										<div class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('institute_name') : ''; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $i++ ; ?>
						@endforeach						
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('plan')) ? 'has-error' : ''; ?>">
						{{ Form::label('plan',trans("Plans"), ['class' => 'mws-form-label']) }}<span class="requireRed"> * </span>
						<div class="mws-form-item">
							{{ Form::select('plan',$plans,'',['class' => 'form-control','placeholder'=>'Choose Plan']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('plan'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('institute_email')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('institute_email',trans("Institute Email").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('institute_email','',array('autocomplete'=>'off','class' => 'form-control')) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('institute_email'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('password')) ? 'has-error' : ''; ?>">
						{!! HTML::decode( Form::label('password', trans("Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::password('password',['class'=>'userPassword form-control', 'readonly'=>'readonly', 'onfocus'=>"this.removeAttribute('readonly')", 'autocomplete'=>'false']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('password'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('confirm_password')) ? 'has-error' : ''; ?>">
						{!! HTML::decode( Form::label('confirm_password', trans("Confirm Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::password('confirm_password',['class'=>'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('confirm_password'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('institute_number')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('institute_number',trans("Institute Contact No.").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('institute_number','', ['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('institute_number'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('institute_address')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('institute_address',trans("Institute Address").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('institute_address','', ['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('institute_address'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('add_image')) ? 'has-error' : ''; ?>">
						{!! HTML::decode( Form::label('add_image', trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::file('add_image[]',array('multiple'=>true,'class'=>'form-control')) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('add_image'); ?>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				<a href="{{ route($modelName.'.add')}}" class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
				<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
			{{ Form::close() }}
			
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $results])</div>
		</div>
	</div>
</section>
@stop