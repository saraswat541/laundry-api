
<div class="row variations_inner variations_inner_{{$counter}}" data-id="{{$counter}}">
	<hr></hr>
	<div class="col-md-6">
		<div class="course_price form-group <?php echo ($errors->first('variation_data['.$counter.'][color]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('variation_data['.$counter.'][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					{{ Form::select('variation_data['.$counter.'][color]',$color,'',['class' => 'form-control chosen-select valid','id'=>'color_'.$counter,'placeholder'=>'Select Color']) }}
					<div class="error-message help-inline color_error_{{ $counter }}">
						<?php echo $errors->first('variation_data['.$counter.'][color]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="course_price form-group <?php echo ($errors->first('variation_data['.$counter.'][quantity]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('variation_data['.$counter.'][quantity]',trans("Quantity").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					{{ Form::text('variation_data['.$counter.'][quantity]','', ['class' => 'form-control valid','id'=>'quantity_'.$counter,'onkeypress'=>'return isNumberKey(event,this)']) }}
					<div class="error-message help-inline quantity_error_{{ $counter }}">
						<?php echo $errors->first('variation_data['.$counter.'][quantity]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="course_price form-group <?php echo ($errors->first('variation_data['.$counter.'][discounted_price]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('variation_data['.$counter.'][discounted_price]',trans("Discounted Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					{{ Form::text('variation_data['.$counter.'][discounted_price]','', ['class' => 'form-control valid','id'=>'discountedprice_'.$counter,'onkeypress'=>'return isNumberKey(event,this)']) }}
					<div class="error-message help-inline discountedprice_error_{{ $counter }}">
						<?php echo $errors->first('variation_data['.$counter.'][discounted_price]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="course_price form-group <?php echo ($errors->first('variation_data['.$counter.'][size]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('variation_data['.$counter.'][size]',trans("Size").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					{{ Form::select('variation_data['.$counter.'][size]',$size,'',['class' => 'form-control chosen-select valid','id'=>'size_'.$counter,'placeholder'=>'Select Size']) }}
					<div class="error-message help-inline size_error_{{ $counter }}">
						<?php echo $errors->first('variation_data['.$counter.'][size]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="course_price form-group <?php echo ($errors->first('variation_data['.$counter.'][price]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('variation_data['.$counter.'][price]',trans("Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					{{ Form::text('variation_data['.$counter.'][price]','', ['class' => 'form-control valid','id'=>'price_'.$counter,'onkeypress'=>'return isNumberKey(event,this)']) }}
					<div class="error-message help-inline price_error_{{ $counter }}">
						<?php echo $errors->first('variation_data['.$counter.'][price]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group <?php echo ($errors->first('variation_data['.$counter.'][is_default]')?'has-error':''); ?>">
				
					
					{!! HTML::decode( Form::label('variation_data['.$counter.'][is_default]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
					{{ Form::checkbox('variation_data['.$counter.'][is_default]','','', ['class' => 'is_default']) }}
			
		</div>
		<div class="form-group ">
			<button type="button" class="btn btn-danger add-row" onclick="remove_variation({{$counter}});">Remove</button>
		</div>
	</div>
</div>
			