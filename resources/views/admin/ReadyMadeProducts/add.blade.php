@extends('admin.layouts.default')

@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>
{{ HTML::script('js/admin/chosen/chosen.jquery.min.js') }}
{{ HTML::style('css/admin/chosen.min.css') }}

{{ HTML::script('js/admin/bootstrap-multiselect.js') }}
{{ HTML::style('css/admin/bootstrap-multiselect.css') }}

<script>
	jQuery(document).ready(function(){
		$(".chosen-select").chosen();
	});
</script>
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Add New {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Add New {{ $sectionNameSingular }}</li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			{{ Form::open(['role' => 'form','route' => "$modelName.add",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
			<div class="row">

				<div class="col-md-6">
					@if(count($languages) > 1)
					<div  class="default_language_color">
						{{ Config::get('default_language.message') }}
					</div>
					<div class="wizard-nav wizard-nav-horizontal">
						<ul class="nav nav-tabs">
							<?php $i = 1 ; ?>
							@foreach($languages as $value)
							<li class=" {{ ($i ==  $language_code )?'active':'' }}">
								<a data-toggle="tab" href="#{{ $i }}div">
									{{ $value -> title }}
								</a>
							</li>
							<?php $i++; ?>
							@endforeach
						</ul>
					</div>
					@endif

					@if(count($languages) > 1)
					<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
						<hr class ="hrLine"/>
						<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
					</div>
					@endif
					<div class="mws-panel-body no-padding tab-content">
						<?php $i = 1 ; ?>
						@foreach($languages as $value)
						<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
							<div class="mws-form-inline">
								<div class="form-group <?php if($i == 1){ echo ($errors->first('name')?'has-error':'');} ?>">
									@if($i == 1)
									{!! HTML::decode( Form::label($i.'.name',trans("Product Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									@else
									{!! HTML::decode( Form::label($i.'.name',trans("Product Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
									@endif
									<div class="mws-form-item">
										{{ Form::text("data[$i][name]",'', ['class' => 'form-control valid', 'id' => 'product_0']) }}
										<div class="error-message help-inline product_error_0">
											<?php echo ($i ==  $language_code ) ? $errors->first('name') : ''; ?>
										</div>
									</div>
								</div>

								<div class="form-group <?php if($i == 1){ echo ($errors->first('description')?'has-error':'');} ?>">
									@if($i == 1)
									{!! HTML::decode( Form::label($i.'._description',trans("Description").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
									@else
									{!! HTML::decode( Form::label($i.'._description',trans("Description").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])) !!}
									@endif
									<div class="mws-form-item">
										{{ Form::textarea("data[$i][description]",'', ['class' => 'form-control textarea_resize','id' => 'description_'.$i ,"rows"=>3,"cols"=>3]) }}
										<span class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('description') : ''; ?>
										</span>
									</div>
									<script type="text/javascript">
										/* For CKEDITOR */

										CKEDITOR.replace( <?php echo 'description_'.$i; ?>,
										{
											height: 250,
											filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
											filebrowserImageWindowWidth : '640',
											filebrowserImageWindowHeight : '480',
											enterMode : CKEDITOR.ENTER_BR
										});

									</script>
								</div>
							</div>
						</div>
						<?php $i++ ; ?>
						@endforeach
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group category-container <?php echo ($errors->first('category_id')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('category_id',trans("Category").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::select(
								'category_id',
								[null => 'Select Category'] + $categories,
								"",
								['class' => 'form-control valid','name'=>'category_id', 'id'=>'category_0']
								)
							}}
							<div class="error-message help-inline category_error_0">
								<?php echo $errors->first('category_id'); ?>
							</div>
						</div>
					</div>


					<div class="form-group <?php echo ($errors->first('brand_id')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('brand_id',trans("Brand").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::select(
								'brand_id',
								[null => 'Select Brand'] + $brand,
								"",
								['class' => 'form-control valid', 'id' => 'brand_0']
								)
							}}
							<div class="error-message help-inline brand_error_0">
								<?php echo $errors->first('brand_id'); ?>
							</div>
						</div>
					</div>
				</div>

			</div>
			<table class="table table-striped table-responsive table-bordered">
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th class="mainth" colspan="3" style="color:white;">Variants
						</tr>
					</thead>
				</table>
				<div class="variations_holder">
					<div class="row variations_inner variations_inner_0" data-id="0">
						<div class="col-md-6">
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][color]')?'has-error':''); ?>">
								<div class="mws-form-row">
									{!! HTML::decode( Form::label('variation_data[0][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									<div class="mws-form-item">
										{{ Form::select('variation_data[0][color]',$color,'',['class' => 'form-control chosen-select valid','id'=>'color_0','placeholder'=>'Select Color']) }}
										<div class="error-message help-inline color_error_0">
											<?php echo $errors->first('variation_data[0][color]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][quantity]')?'has-error':''); ?>">
								<div class="mws-form-row">
									{!! HTML::decode( Form::label('variation_data[0][quantity]',trans("Quantity").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									<div class="mws-form-item">
										{{ Form::text('variation_data[0][quantity]','', ['class' => 'form-control valid','id'=>'quantity_0','onkeypress'=>'return isNumberKey(event,this)']) }}
										<div class="error-message help-inline quantity_error_0">
											<?php echo $errors->first('variation_data[0][quantity]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][discounted_price]')?'has-error':''); ?>">
								<div class="mws-form-row">
									{!! HTML::decode( Form::label('variation_data[0][discounted_price]',trans("Discounted Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									<div class="mws-form-item">
										{{ Form::text('variation_data[0][discounted_price]','', ['class' => 'form-control valid','id'=>'discount_0','onkeypress'=>'return isNumberKey(event,this)']) }}
										<div class="error-message help-inline discount_error_0 ">
											<?php echo $errors->first('variation_data[0][discounted_price]'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][size]')?'has-error':''); ?>">
								<div class="mws-form-row">
									{!! HTML::decode( Form::label('variation_data[0][size]',trans("Size").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									<div class="mws-form-item">
										{{ Form::select('variation_data[0][size]',$size,'',['class' => 'form-control chosen-select valid','id'=>'size_0','placeholder'=>'Select Size']) }}
										<div class="error-message help-inline size_error_0">
											<?php echo $errors->first('variation_data[0][size]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][price]')?'has-error':''); ?>">
								<div class="mws-form-row">
									{!! HTML::decode( Form::label('variation_data[0][price]',trans("Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									<div class="mws-form-item">
										{{ Form::text('variation_data[0][price]','', ['class' => 'form-control valid','id'=>'price_0','onkeypress'=>'return isNumberKey(event,this)']) }}
										<div class="error-message help-inline price_error_0">
											<?php echo $errors->first('variation_data[0][price]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group <?php echo ($errors->first('variation_data[0][is_default]')?'has-error':''); ?>">
								{!! HTML::decode( Form::label('variation_data[0][is_default]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
								{{ Form::checkbox('variation_data[0][is_default]','1',true, ['class' => 'is_default', 'id'=>'default_0']) }}
								<div class="error-message help-inline default_error_0">
									<?php echo $errors->first('image_data[0][default]'); ?>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="row pad">
					<div class="col-md-12 col-sm-12">
						<div class="col-md-6">
							<button type="button" name="add" id="add" class="btn btn-success add-row" onclick="add_more_variation();">Add More</button>
						</div>
					</div>
				</div>

				<table class="table table-striped table-responsive table-bordered">
					<thead>
						<tr style="background-color:#3c3f44; color:white;">
							<th class="mainth" colspan="3" style="color:white;">Variant Images
							</tr>
						</thead>
					</table>
					<div class="images_holder">
						<div class="row images_inner images_inner_0" data-id="0">
							<div class="col-md-6">
								<div class="image_price form-group <?php echo ($errors->first('image_data[0][color]')?'has-error':''); ?>">
									<div class="mws-form-row">
										{!! HTML::decode( Form::label('image_data[0][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										<div class="mws-form-item">
											{{ Form::select('image_data[0][color]',$color,'',['class' => 'form-control chosen-select valid','id' => 'select color_0','placeholder'=>'Select Color']) }}
											<div class="error-message help-inline select color_error_0">
												<?php echo $errors->first('image_data[0][color]'); ?>
											</div>
										</div>
									</div>
								</div>

								<div class="image_price form-group <?php echo ($errors->first('image_data[0][other_image]')?'has-error':''); ?>">
									<div class="mws-form-row">
										{!! HTML::decode( Form::label('other_image', trans("Other Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										<div class="mws-form-item">
											{{ Form::file('image_data[0][other_image][]', ['class' => 'form-control valid', 'id'=>'other image_0','accept'=>"image/*", 'multiple'=>"multiple"]) }}
											<div class="error-message help-inline other image_error_0">
												<?php echo $errors->first('image_data[0][other_image]'); ?>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-6">

								<div class="image_price form-group <?php echo ($errors->first('image_data[0][image]')?'has-error':''); ?>">
									<div class="mws-form-row">
										{!! HTML::decode( Form::label('image', trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
										<div class="mws-form-item">
											{{ Form::file('image_data[0][image]', ['class' => 'form-control valid','id'=>'image_0','accept'=>"image/*"]) }}
											<div class="error-message help-inline image_error_0">
												<?php echo $errors->first('image_data[0][image]'); ?>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group <?php echo ($errors->first('image_data[0][is_defaults]')?'has-error':''); ?>">
									{!! HTML::decode( Form::label('image_data[0][is_defaults]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									{{ Form::checkbox('image_data[0][is_defaults]','1',true, ['class' => 'is_defaults', 'id'=>'default_1']) }}
									<div class="error-message help-inline default_error_1">
										<?php echo $errors->first('image_data[0][defaults]'); ?>
									</div>
								</div>
							</div>

						</div>

					</div>
					<div class="row pad">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-6">
								<button type="button" name="add" id="add" class="btn btn-success add-row" onclick="add_more_images();">Add More</button>
							</div>
						</div>
					</div>

					<div class="mws-button-row">
                        <input type="submit" value="{{ trans('Save') }}" id="add_product" class="btn btn-danger">
						<a href="{{ route($modelName.'.add')}}" class="btn btn-primary reset_form"><i class="\icon-refresh\"></i> {{ trans('Clear') }}</a>
						<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class="\icon-refresh\"></i> {{ trans('Cancel')  }}</a>
					</div>
					{{ Form::close() }}
				</div>
			</div>
		</section>
		<style>
		.textarea_resize {
			resize: vertical;
		}
		.error{
			color: red;
		}

	</style>
	<script>
		function add_more_variation(){
			$("#loader_img").show();
			var get_last_id  =  $('.variations_holder > div.variations_inner').last().attr('data-id');
			var counter  	 =  parseInt(get_last_id) + 1;
		$.ajax({
			headers				:	{"X-CSRF-TOKEN":"{{ csrf_token() }}"},
			url					:	"{{route('ReadyMadeProducts.addMoreVariation')}}",
			type				:	"POST",
			data				:	{'counter':counter},
			success				:	function(response){
				$("#loader_img").hide();
				$('.variations_holder').append(response);
			}
		});
	}
	function remove_variation(id){
		bootbox.confirm("Are you sure want to remove this ?",
			function(result){
				if(result){
					$('.variations_inner_'+id).remove();
				}
			}
			);
	}

	$(document).on('change', '.is_default', function(){
		if($(this).prop("checked") == true){
			$('.is_default').not(this).prop('checked', false);
		}
	});


	function add_more_images(){
		$("#loader_img").show();
		var get_last_id  = $('.images_holder > div.images_inner').last().attr('data-id');
		var counter  	 		=  	parseInt(get_last_id) + 1;
		$.ajax({
			headers				:	{"X-CSRF-TOKEN":"{{ csrf_token() }}"},
			url					:	"{{route('ReadyMadeProducts.addMoreImage')}}",
			type				:	"POST",
			data				:	{'counter':counter},
			success				:	function(response){
				$("#loader_img").hide();
				$('.images_holder').append(response);
			}
		});
	}
	function remove_image(id){
		bootbox.confirm("Are you sure want to remove this ?",
			function(result){
				if(result){
					$('.images_inner_'+id).remove();
				}
			}
		);
	}

	$(document).on('change', '.is_defaults', function(){
		if($(this).prop("checked") == true){
			$('.is_defaults').not(this).prop('checked', false);
		}
	});

	$('#add_product').on('click',function(e){
		$(".help-inline").removeClass("error");
		$(".help-inline").html("");
		$("#loader_img").show();

		var error 	 = 	0;
		let validExt = ['jpg','jpeg','png','gif','bmp'];
		$(".valid").each(function(){

			var value			=	$(this).val();
			var id				=	$(this).attr("id");
			var idarr			=	id.split("_");
			if(value == ''){
				error = 1;
				$("."+idarr[0]+"_"+idarr[1]).focus();
				$("."+idarr[0]+"_error_"+idarr[1]).addClass("error");
				$("."+idarr[0]+"_error_"+idarr[1]).html("The "+idarr[0]+" field is required.");
				$("#loader_img").hide();
			}

			if($('#quantity_'+idarr[1]).val() == 0 && $('#quantity_'+idarr[1]).val() != ''){
				error = 1;
				$(".quantity_error_"+idarr[1]).addClass("error");
				$(".quantity_error_"+idarr[1]).html("Quantity should be greater than 0.");
				$("#loader_img").hide();
			}
			if($('#price_'+idarr[1]).val() == 0 && $('#price_'+idarr[1]).val() != ''){
				error = 1;
				$(".price_error_"+idarr[1]).addClass("error");
				$(".price_error_"+idarr[1]).html("Price should be greater than 0.");
				$("#loader_img").hide();
			}

            if($('#discount_'+idarr[1]).val() == 0 && $('#discount_'+idarr[1]).val() != '') {
				error = 1;
				$(".discount_error_"+idarr[1]).addClass("error");
				$(".discount_error_"+idarr[1]).html("Discounted Price should be greater than 0.");
				$("#loader_img").hide();
			}
            if(parseInt($('#discount_'+idarr[1]).val()) > parseInt($('#price_'+idarr[1]).val())){
                error = 1;
                $(".discount_error_"+idarr[1]).addClass("error");
				$(".discount_error_"+idarr[1]).html("Discounted Price should not be greater than actual price.");
				$("#loader_img").hide();
            }

			var exten = $("#image_"+idarr[1]).val().split('.').pop().toLowerCase();
			if(validExt.indexOf(exten) == -1 && $("#image_"+idarr[1]).val() !=''){
				error	=	1;
				$(".image_error_"+idarr[1]).addClass("error");
				$(".image_error_"+idarr[1]).html("{{ trans('Image should be jpeg,jpeg,png,gif,bmp.') }}");
				$("#loader_img").hide();
			}
		});

		if($("#default_0:checked").val() != '1'){
				error = 1;
				$('#default_0').focus();
				$('.default_error_0').addClass("error");
				$('.default_error_0').html("The default field is required.");
				$("#loader_img").hide();
			}
		if($("#default_1:checked").val() != '1'){
				error = 1;
				$('#default_1').focus();
				$('.default_error_1').addClass("error");
				$('.default_error_1').html("The default field is required.");
				$("#loader_img").hide();
			}

		if(error == 1){
			e.preventDefault();
		}

	});

</script>
@stop
