@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		View {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">View {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Tailor</th>
								<td data-th='Tailor' class="txtFntSze">{{ $model->tailor_name}}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Supplier</th>
								<td data-th='Supplier Name' class="txtFntSze">{{ $model->supplier_name }}</td>
							</tr> 
							<tr>
								<th  width="30%" class="text-right txtFntSze">Product Name</th>
								<td data-th='Name' class="txtFntSze">{{ isset($model->name) ? $model->name :'' }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Category</th>
								<td data-th='Category' class="txtFntSze">{{ $model->categories_name }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Sub Category</th>
								<td data-th='SubCategory' class="txtFntSze">{{ $model->sub_categories_name }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Material Type</th>
								<td data-th='Material Type' class="txtFntSze">{{ $model->material_type_name }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Brand</th>
								<td data-th='Brand' class="txtFntSze">{{ $model->brand_name }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Shipping Cost</th>
								<td data-th='Price' class="txtFntSze">{{ $model->cost_of_shipping }}</td>
							</tr>
							@foreach($model->variations as $variation)
							<tr>
								<th  width="30%" class="text-right txtFntSze">Color</th>
								<td data-th='Color' class="txtFntSze">{{ $variation->colors->name}}</td>
							</tr>

							<tr>
								<th  width="30%" class="text-right txtFntSze">Size</th>
								<td data-th='Size' class="txtFntSze">{{ $variation->size->name}}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Quantity</th>
								<td data-th='Quantity' class="txtFntSze">{{ $variation->quantity }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Price</th>
								<td data-th='Price' class="txtFntSze">{{ $variation->price }}</td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Discounted Price</th>
								<td data-th='DiscountedPrice' class="txtFntSze">{{ $variation->discounted_price }}</td>
							</tr>
							@endforeach
							<tr>
								<th  width="30%" class="text-right txtFntSze">Delivery In Days</th>
								<td data-th='Price' class="txtFntSze">{{ $model->delivery_time }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Status</th>
								<td data-th='Status' class="txtFntSze">
									@if($model->is_active	==1)
									<span class="label label-success" >{{ trans("Activated") }}</span>
									@else
									<span class="label label-warning" >{{ trans("Deactivated") }}</span>
									@endif 
								</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
