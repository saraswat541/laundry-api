
<div class="row images_inner images_inner_{{$counter}}" data-id="{{$counter}}">
	<hr></hr>
	<div class="col-md-6">
		<div class="image_price form-group <?php echo ($errors->first('image_data['.$counter.'][color]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('image_data['.$counter.'][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					{{ Form::select('image_data['.$counter.'][color]',$color,'',['class' => 'form-control chosen-select','placeholder'=>'Select Color']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('image_data['.$counter.'][color]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="image_price form-group <?php echo ($errors->first('image_data['.$counter.'][other_image]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('image_data['.$counter.'][other_image]', trans("Other Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					{{ Form::file('image_data['.$counter.'][other_image][]', ['class' => 'form-control','accept'=>"image/*",'multiple'=>"multiple"]) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('image_data['.$counter.'][other_image]'); ?>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="col-md-6">
		<div class="image_price form-group <?php echo ($errors->first('image_data['.$counter.'][image]')?'has-error':''); ?>">
			<div class="mws-form-row">
				{!! HTML::decode( Form::label('image_data['.$counter.'][image]', trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
				<div class="mws-form-item">
					{{ Form::file('image_data['.$counter.'][image]', ['class' => 'form-control','accept'=>"image/*"]) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('image_data['.$counter.'][image]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group <?php echo ($errors->first('image_data['.$counter.'][is_defaults]')?'has-error':''); ?>">
			{!! HTML::decode( Form::label('image_data['.$counter.'][is_defaults]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
			{{ Form::checkbox('image_data['.$counter.'][is_defaults]','','', ['class' => 'is_defaults']) }}

		</div>
		<div class="form-group ">
			<button type="button" class="btn btn-danger add-row" onclick="remove_image({{$counter}});">Remove</button>
		</div>
	</div>
</div>
