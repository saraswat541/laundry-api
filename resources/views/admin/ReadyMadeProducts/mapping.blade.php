@php
use App\Model\Category;
@endphp
@if(isset($category_id) && !empty($category_id))
@php $subCategoryList = Category::subCategoryList($category_id);
	@endphp

	{!! HTML::decode( Form::label('sub_category_id',trans("Sub Category").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
	<div class="mws-form-item">
		{{ Form::select(
			'sub_category_id',
			[null => 'Select Sub Category'] + $subCategoryList,
			"",
			['class' => 'form-control','id'=>'sub_category_id']
			) 
		}}
		<div class="error-message help-inline">
			<?php echo $errors->first('sub_category_id'); ?>
		</div>
	</div>
@endif
