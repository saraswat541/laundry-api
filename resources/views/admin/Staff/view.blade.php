@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		View {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">View {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							
							<tr>
								<th width="30%" class="text-right txtFntSze">First Name</th>
								<td data-th='First Name' class="txtFntSze">{{ isset($model->first_name) ? $model->first_name :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Last Name</th>
								<td data-th='Last Name' class="txtFntSze">{{ isset($model->last_name) ? $model->last_name :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">E-mail</th>
								<td data-th='E-mail' class="txtFntSze">{{ isset($model->email) ? $model->email :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Contact No.</th>
								<td data-th='Contact No.' class="txtFntSze">{{ isset($model->contact_no) ? $model->contact_no :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Date of Birth</th>
								<td data-th='Date of Birth' class="txtFntSze">{{ isset($model->dob) ? $model->dob :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Address</th>
								<td data-th='Address' class="txtFntSze">{{ isset($model->address) ? $model->address :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Status</th>
								<td data-th='Status' class="txtFntSze">
									@if($model->is_active	==1)
										<span class="label label-success" >{{ trans("Activated") }}</span>
									@else
										<span class="label label-warning" >{{ trans("Deactivated") }}</span>
									@endif
								</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
