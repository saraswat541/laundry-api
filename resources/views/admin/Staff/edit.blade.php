@extends('admin.layouts.default')
@section('content')
<!-- CKeditor start here-->
<script src="{{ WEBSITE_JS_URL }}admin/plugins/ckeditor/ckeditor.js"></script>
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Edit {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">Edit {{ $sectionNameSingular }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="row">
				{{ Form::open(['role' => 'form','url' =>  route("$modelName.edit",$model->id),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"]) }}
				<div class="col-md-6">
					@if(count($languages) > 1)
					<div  class="default_language_color">
						{{ Config::get('default_language.message') }}
					</div>
					<div class="wizard-nav wizard-nav-horizontal">
						<ul class="nav nav-tabs">
							<?php $i = 1 ; ?>
							@foreach($languages as $value)
							<li class=" {{ ($i ==  $language_code )?'active':'' }}">
								<a data-toggle="tab" href="#{{ $i }}div">
									{{ $value -> title }}
								</a>
							</li>
							<?php $i++; ?>
							@endforeach
						</ul>
					</div>
					@endif
					@if(count($languages) > 1)
					<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
						<hr class ="hrLine"/>
						<b>{{ trans("These fields (above seperator line) are same in all languages") }}</b>
					</div>
					@endif
					<div class="mws-panel-body no-padding tab-content">
						<?php $i = 1 ; ?>
						@foreach($languages as $value)
						<div id="{{ $i }}div" class="tab-pane {{ ($i ==  $language_code )?'active':'' }} ">
							<div class="mws-form-inline">
								<div class="form-group <?php if($i == 1){ echo ($errors->first('first_name')?'has-error':'');} ?>">
									@if($i == 1)
									{!! HTML::decode( Form::label($i.'.first_name',trans("First Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									@else
									{!! HTML::decode( Form::label($i.'.first_name',trans("First Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])) !!}
									@endif
									<div class="mws-form-item">
										{{ Form::text("data[$i][first_name]",$model->first_name, ['class' => 'form-control']) }}
										<div class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('first_name') : ''; ?>
										</div>
									</div>
								</div>
								
								<div class="form-group category-container <?php echo ($errors->first('daycare_id')?'has-error':''); ?>">
									{!! HTML::decode( Form::label('daycare_id',trans("Daycare Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
									<div class="mws-form-item">
										{{ Form::select(
											'daycare_id',
											[null => 'Select Daycare'] + $ins_name,
											$model->daycare_id,
											['class' => 'form-control','name'=>'daycare_id']
											) 
										}}
										<div class="error-message help-inline">
											<?php echo $errors->first('daycare_id'); ?>
										</div>
									</div>
								 </div>
								 
							</div>
						</div> 
						<?php $i++ ; ?>
						@endforeach
						
						<div class="form-group <?php echo ($errors->first('contact_no')?'has-error':''); ?>">
							{!! HTML::decode( Form::label('contact_no',trans("Contact No.").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('contact_no',$model->contact_no, ['class' => 'form-control']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('contact_no'); ?>
								</div>
							</div>
						</div>
						
						<div class="form-group <?php echo ($errors->first('dob')?'has-error':''); ?>">
							{!! HTML::decode( Form::label('dob',trans("Date of Birth").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
							<div class="mws-form-item">
								{{ Form::text('dob',$model->dob, ['class' => 'form-control', 'id' => 'dob_edit']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('dob'); ?>
								</div>
							</div>
						</div>
						
						<div class="mws-button-row">
							<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
							<a href='{{ route("$modelName.edit",$model->id)}}' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> {{ trans('Clear') }}</a>
							<a href="{{ route($modelName.'.index') }}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('last_name')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('last_name',trans("Last Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('last_name',$model->last_name, ['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('last_name'); ?>
							</div>
						</div>
					</div>
					<div class="form-group <?php echo ($errors->first('email')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('email',trans("E-mail").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('email',$model->email, ['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('email'); ?>
							</div>
						</div>
					</div>
					<div class="form-group <?php echo ($errors->first('address')?'has-error':''); ?>">
						{!! HTML::decode( Form::label('address',trans("Address").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])) !!}
						<div class="mws-form-item">
							{{ Form::text('address',$model->address, ['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('address'); ?>
							</div>
						</div>
					</div>
				</div>
				{{ Form::close() }} 
			</div>
		</div>
	</div>
</section>
{{ HTML::style('css/admin/bootstrap-datetimepicker.css') }}
{{ HTML::script('js/admin/moment.js') }}
{{ HTML::script('js/admin/bootstrap-datetimepicker.js') }} 
<script>
jQuery(document).ready(function(){
		$('#dob_edit').datetimepicker({
			format: 'YYYY-MM-DD'
		});
	});
</script>
@stop
