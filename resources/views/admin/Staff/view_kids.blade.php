@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		View {{ $sectionNameSingular }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{ route($modelName.'.index')}}">{{ $sectionName }}</a></li>
		<li class="active">View {{ $sectionNameSingular }}</li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							
							<tr>
								<th width="30%" class="text-right txtFntSze">Paraent Name</th>
								<td data-th='First Name' class="txtFntSze">{{ isset($parent_name[0]) ? $parent_name[0] :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Kid Name</th>
								<td data-th='Last Name' class="txtFntSze">{{ isset($model->kid_name) ? $model->kid_name :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Date of Birth</th>
								<td data-th='Date of Birth' class="txtFntSze">{{ isset($model->kid_dob) ? $model->kid_dob :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Age</th>
								<td data-th='Contact No.' class="txtFntSze">{{ isset($age) ? $age :'' }}</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Image</th>
								<td data-th='Image' class="txtFntSze">
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $model->kid_profile_picture; ?>"><img src="{{$model->kid_profile_picture}}" style="width: 50px"></a>
								</td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Relationship</th>
								<td data-th='Address' class="txtFntSze">{{ isset($relation_name[0]) ? $relation_name[0] :'' }}</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
