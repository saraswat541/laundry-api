@extends('admin.layouts.default')
@section('content')
<link href="{{WEBSITE_CSS_URL}}admin/chosen.min.css" rel="stylesheet">
<script src="{{WEBSITE_JS_URL}}admin/chosen.jquery.min.js"></script>
<script src="{{WEBSITE_JS_URL}}admin/jquery-ui-1.9.2.min.js"></script>
<script src="{{WEBSITE_JS_URL}}admin/jquery-ui-timepicker.min.js"></script>
<link href="{{WEBSITE_CSS_URL}}admin/jui/css/jquery.ui.all.css" rel="stylesheet">
<style>
.chosen-container-single .chosen-single{
	height:34px !important;
	padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	  {{ trans("Support") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('adminpnlx/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Support") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		<div class="col-md-3 col-sm-3">
			<div class="info-box">
				<span class="info-box-icon bg-blue">
					<i class="fa fa-ticket" aria-hidden="true"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Open tickets</span>
					<span class="info-box-number"> <?php echo $totalOpen; ?> </span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class="info-box">
				<span class="info-box-icon bg-green">
					<i class="fa fa-ticket" aria-hidden="true"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Resolved tickets</span>
					<span class="info-box-number"> <?php echo $totalResolved; ?> </span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class="info-box">
				<span class="info-box-icon bg-purple">
					<i class="fa fa-calendar" aria-hidden="true"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Average time</span>
					<span class="info-box-number"> <?php echo (!empty($finalResponseTime)?Str::limit($finalResponseTime,20):'0'); ?> </span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class="info-box">
				<span class="info-box-icon bg-blue">
					<i class="fa fa-ticket" aria-hidden="true"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Open ticket/day</span>
					<span class="info-box-number"> <?php echo $openTicketPerDay; ?> </span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class="info-box">
				<span class="info-box-icon bg-blue">
					<i class="fa fa-ticket" aria-hidden="true"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Open ticket/week</span>
					<span class="info-box-number"> <?php echo $openTicketPerWeek; ?> </span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class="info-box">
				<span class="info-box-icon bg-blue">
					<i class="fa fa-ticket" aria-hidden="true"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Open ticket/month</span>
					<span class="info-box-number"> <?php echo $openTicketPerMonth; ?> </span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class="info-box">
				<span class="info-box-icon bg-blue">
					<i class="fa fa-ticket" aria-hidden="true"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Open ticket/year</span>
					<span class="info-box-number"> <?php echo $openTicketPerYear; ?> </span>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		{{ Form::open(['role' => 'form','url' => 'adminpnlx/support','class' => 'mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('search_keyword',((isset($search_keyword)) ? $search_keyword : ''), ['class' => 'form-control ','placeholder'=>'By personal info']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('search_keyword_ticket',((isset($search_keyword_ticket)) ? $search_keyword_ticket : ''), ['class' => 'form-control ','placeholder'=>'By ticket info']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('start_from',((isset($dateS)) ? $dateS : ''), ['class' => 'form-control ','placeholder'=>'Date From','id'=>'start_from',"readonly"]) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('start_to',((isset($dateE)) ? $dateE : ''), ['class' => 'form-control ','placeholder'=>'Date To','id'=>'start_to',"readonly"]) }}
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="pull-right" >
					<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('messages.search.text') }}</button>
					<a href="{{URL::to('adminpnlx/support')}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("messages.reset.text") }}</a>
				</div>
			</div>
			{{ Form::close() }}
		<div class="col-md-5 col-sm-4">
			<div class="form-group">  
				
			</div>
		</div>
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="15%">
							{{
								link_to_route(
									"Support.listSupport",
									trans("Ticket Number"),
									array(
										'sortBy' => 'ticket_number',
										'order' => ($sortBy == 'ticket_number' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'ticket_number' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'ticket_number' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">
							Subject
						</th>
						<th width="15%">
							{{
								link_to_route(
									"Support.listSupport",
									trans("Posted By"),
									array(
										'sortBy' => 'username',
										'order' => ($sortBy == 'username' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'username' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'username' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="15%">
							{{
								link_to_route(
									"Support.listSupport",
									trans("Title"),
									array(
										'sortBy' => 'title',
										'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"Support.listSupport",
									trans("Message"),
									array(
										'sortBy' => 'message',
										'order' => ($sortBy == 'message' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'message' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'message' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th>
							{{
								link_to_route(
									"Support.listSupport",
									trans("Status"),
									array(
										'sortBy' => 'status',
										'order' => ($sortBy == 'status' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'status' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'status' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						
						</th>
						<th>Action</th>
					</tr>
				</thead>
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)
						<?php
							/* $a = new \DateTime($record->requested_date);
							$b = new \DateTime;

							echo $a->diff($b)->days; */
							
							$today 			= 	date('Y-m-d'); // today date
							$diff 			= 	strtotime($today) - strtotime($record->requested_date);
							$days 			= 	(int)$diff/(60*60*24);
							$class			=	"";
							if($record->admin_comment_count >  $record->user_comment_count){
								$class	=	"#FF00FF !important;color:fff !important;";
							}else{
								if(2 >= $days){
										$class	=	"#00FFFF !important;color:black !important;";
								}else if(4 >= $days){
										$class	=	"#ffff27 !important;color:black !important;";
								}else if(4 < $days){
										$class	=	"red !important;";
								}
							}
						?>
						<tr>
							<td> {{ $record->ticket_number }} </td>
							<td>{{ Str::limit(strip_tags($record->subject),30) }}  </td>
							<td> {{ $record->username }} </td>
							<td> {{ Str::limit(strip_tags($record->title),30) }}  </td>
							<td> {{ Str::limit(strip_tags($record->message),30) }} </td>
							<td> 
							@if($record->status == 1)
								<span class="label label-warning" style="background-color:{{$class}} !important;">{{ trans("Open") }} </span>
							@elseif($record->status == 2)
								<span class="label label-success" style="background-color:#00FF00 !important; color: black !important;">{{ trans("Resolved") }} </span>
							@endif
							</td>
							<td>
								@if($record->status==1)
									<a title="Mark As Resolved" href="{{{ URL::to('adminpnlx/support/update-status/'.$record->id.'/2')}}}"class="btn btn-success change_ticket_status">Resolve</a>
								@endif
								
								<a href="{{ URL('adminpnlx/support/view-support/'.$record->id) }}" title="{{ trans('View Detail') }}" class="btn btn-primary " data-id="{{ $record->id }}">
									<i class="fa fa-eye"></i>
								</a>
								<a href="javascript:void(0);" title="{{ trans('Add Notes') }}" class="btn btn-info addNotes" data-id="{{ $record->id }}">
									<i class="fa fa-sticky-note-o" aria-hidden="true"></i>
								</a>
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="9" >{{ trans("messages.user_management.no_record_found_message") }}</td>
						</tr>
					@endif
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Lending Interest</h4>
      </div>
      <div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<label>Interest *</label>
					<input type="text" name="interest" class="form-control interest">
					<span class="help-inline"></span>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success submit_interest" onclick="submit_interest();">Save</button>
      </div>
    </div>
  </div>
</div>

<div id="updateModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Lending Interest</h4>
      </div>
      <div class="modal-body show_content">
			
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="update_interest();">Save</button>
      </div>
    </div>
  </div>
</div>

<div id="addNotesModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Note</h4>
      </div>
	  {{ Form::open(['role' => 'form','url' => 'adminpnlx/support/save-notes','class' => 'mws-form saveNotesForm']) }}
      <div class="modal-body">
			<div class="form-group">
				<label>Note</label>
				<input type="hidden" id="ticket_id" name="ticket_id"/>
				<textarea id="noteId" class="form-control" name="notes"></textarea>
				<span class="help-inline"></span>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="save_notes();">Add</button>
      </div>
	  {{ Form::close() }}
    </div>
  </div>
</div>


<script>
	$(document).on('click', '.change_ticket_status', function(e){
		e.stopImmediatePropagation();
		url = $(this).attr('href');
		bootbox.confirm("Are you sure want to mark this resolved?",
		function(result){
			if(result){
				window.location.replace(url);
			}
		});
		e.preventDefault();
	});
function save_notes(){
	$('#loader_img').show();
	ticket_id 	=	$("#ticket_id").val();
	noteId 		=	$("#noteId").val().trim();
	if(noteId != ""){
		$(".saveNotesForm").submit();
	}else{
		$("#noteId").next().html("This field is required.");
		$("#noteId").css("border","1px solid red");
		$('#loader_img').hide();
	}
	
}
$(".addNotes").click(function(){
	$('#loader_img').show();
	$("#noteId").css("border","1px solid #ccc");
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	$('#noteId').val('');
	id = $(this).attr("data-id");
	$("#ticket_id").val(id);
	$("#addNotesModal").modal({
   show: true,
   keyboard: false,
   backdrop: 'static'
  });
	$('#loader_img').hide();
});

$(".add_interest").click(function(){
	$("#myModal").modal({
   show: true,
   keyboard: false,
   backdrop: 'static'
  });
});

$(".update_interest").click(function(){
	var rel = $(this).attr('data-id');
	$.ajax({
		url:"{{  URL('adminpnlx/update-interest') }}",
		type:'POST',
		data:{'rel':rel},
		success:function(data){
			$(".show_content").html(data);
			$("#updateModal").modal({
   show: true,
   keyboard: false,
   backdrop: 'static'
  });
		}
	});
});

$(document).ready(function() {
	 $( "#start_from" ).datepicker({
		dateFormat 	: 'yy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		onSelect	: function( selectedDate ){ $("#start_to").datepicker("option","minDate",selectedDate); }
	});
	$( "#start_to" ).datepicker({
		dateFormat 	: 'yy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		onSelect	: function( selectedDate ){ $("#start_from").datepicker("option","maxDate",selectedDate); }
	});
})
$(function(){
		$('.date_of_birth').datepicker({
			dateFormat 	: 'yy-mm-dd',
			changeMonth : true,
			changeYear 	: true,
			yearRange	: '-100y:c+nn',
			maxDate		: '-1'
		});	
	});

 $(document).on('click', '.show_profile', function(e){ 
			e.stopImmediatePropagation();
			url = $(this).attr('href');
			var full_name = $(this).attr('data-rel');
			bootbox.confirm("Are you sure want to show "+full_name+ '?',
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
			e.preventDefault();
		});
		
		$(document).on('click', '.hide_profile', function(e){ 
			e.stopImmediatePropagation();
			url = $(this).attr('href');
			var full_name = $(this).attr('data-rel');
			bootbox.confirm("Are you sure want to hide "+full_name+ '?',
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
			e.preventDefault();
		});
	$(".findUsers").change(function(){
		$("#search_users").submit();
	});
	
	function submit_interest(){
		$('#loader_img').show();
		var interest = $(".interest").val();
		$('.help-inline').html('');
		$('.help-inline').removeClass('error');
		$.ajax({
			url: '{{ URL("adminpnlx/addInterest") }}',
			type:'POST',
			data: {'interest':interest},
			success: function(r){
				error_array 	= 	JSON.stringify(r);
				data			=	JSON.parse(error_array);
				if(data['success'] == 1){
					location.reload(true);
				}else if(data['success'] == 2){
					show_message(data['message'],"error");
				}else{
					$.each(data['errors'],function(index,html){
						$(".interest").next().addClass('error');
						$(".interest").next().html(html);
					});
				}
				$('#loader_img').hide();
			}
		});
	}
	
	function update_interest(){
		$('#loader_img').show();
		$('.help-inline').html('');
		$('.help-inline').removeClass('error');
		$.ajax({
			url: '{{ URL("adminpnlx/save-update-interest") }}',
			type:'POST',
			data: $("#update_interest_form").serialize(),
			success: function(r){
				error_array 	= 	JSON.stringify(r);
				data			=	JSON.parse(error_array);
				if(data['success'] == 1){
					location.reload(true);
				}else{
					$.each(data['errors'],function(index,html){
						$("#update_interest_error").addClass('error');
						$("#update_interest_error").html(html);
					});
				}
				$('#loader_img').hide();
			}
		});
	}
</script>
@stop