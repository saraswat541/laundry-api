@extends('admin.layouts.default')
@section('content')
<script src="{{WEBSITE_JS_URL}}bootstrap.min.js"></script>
<link href="{{WEBSITE_JS_URL}}admin/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<script src="{{WEBSITE_JS_URL}}admin/plugins/fancybox/jquery.fancybox.js"></script>
<style>
.table.table-striped td {
    font-size: 14px;
}
.table.table-striped th {
    font-size: 14px;
}
.view{
	background-color:#3c3f44; color:white;"
}
.logs_entries.red {
    border-left: 4px solid #e214e7;
}
.logs_entries.orange {
    border-left: 4px solid orange;
}
.logs_entries.blue {
    border-left: 4px solid blue;
}
.logs_entries.green {
    border-left: 4px solid green;
}
</style>
<?php 
	$controller				=	Request::segment(1);
	$action					=	Request::segment(2);
	$connection_id			=	Request::segment(3);
?>
<section class="content-header">
	<h1>
		Ticket {{ $result->ticket_number }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('adminpnlx/support')}}">Support</a></li>
		<li class="active">{{ trans("Support Ticket Details") }}</li>
	</ol>
</section>
<?php 
	$docarr = array();
	$docarr = explode(',',DOC_EXTENSION);
	$pdfarr = array();
	$pdfarr = explode(',',PDF_EXTENSION);
?>
<section class="content">
	<div class="row">	
		<div class="col-md-12 col-sm-6">
			<div class="row pad">
				<div class="col-md-12 col-sm-12" >	
					<div class="">
						<div id="info1"></div>
						<div >  
							<div class="clearfix"></div>
							<div class="col-md-12 col-sm-8">
								<ul class="nav nav-tabs">
									<?php
										if(Request::segment(5) == "save_comments"){
											$comment_section_active			=	"active";
											$comment_section_active_in		=	"in active";
											
											$other_section_active			=	"";
											$other_section_active_in		=	"";
										}else {
											$comment_section_active			=	"";
											$comment_section_active_in		=	"";
											
											$other_section_active			=	"active";
											$other_section_active_in		=	"in active";
										}
									?>
									<li class="{{$other_section_active}}"><a data-toggle="tab" href="#ticket_detail_data">DETAILS</a></li>
									<li class="{{$comment_section_active}}"><a data-toggle="tab" href="#comment_data">COMMENTS</a></li>
									<li class=""><a data-toggle="tab" href="#logs_data">LOGS</a></li>
									<li class=""><a data-toggle="tab" href="#attachment_data">ATTACHMENT</a></li>
									<li class=""><a data-toggle="tab" href="#NOTES">NOTES</a></li>
								</ul>
							</div>
							<div class="tab_info_container">
								<div class="tab-content">
									<div class="tab_detail_div tab-pane fade {{$other_section_active_in}}" id="ticket_detail_data">
										<div class="col-md-12 col-sm-12" >	
											<div class="">
												<div id="info1"></div>
												<div class="box-header with-border">
													<b><h3 class="box-title">
														About Ticket</h3></b>
												</div>
												<div class="box-body" style="display: block;">  
													<div style="width:70%;">
														<div class="subject_posted_by" id="" style="float:left;width:60%;">
															<div style="display:block;width:100%;">
															<b>Subject</b> : {{ $result->subject }} <br />
															<b>Posted By</b> : {{ (!empty($result->username)) ? $result->username : $result->username }} On {{ date("M d, Y",strtotime($result->created_at))}}<b>@</b>{{date("H:i",strtotime($result->created_at))}}<br />
															</div>
															<div class="clearfix"></div>
															<!--<div style="display:block;width:100%;">
																<b style="display:block;width:100%;">Description : </b>
																<span  style="display:block;width:100%;">{{ isset($result->description) ? str_replace(array("\r\n","\r","\n"),"<br/>",$result->description) :'' }}<br />
																<span>
															</div>-->
														</div>
														<div style="float:right;" class="subject_posted_by">
															<b>Status</b> : 
															@if($result->status == 1)
																<span class="label label-warning">{{ trans("Open") }} </span>
															@elseif($result->status == 2)
																<span class="label label-success">{{ trans("Resolved") }} </span>
															@endif
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12 col-sm-12" >	
											<div id="info1"></div>
											<div class="box-header with-border">
												<b><h3 class="box-title">
													Message</h3></b>
											</div>
											<div style="width:90%;">
												<p class="description_txt"><?php echo isset($result->message) ? str_replace(array("\r\n","\r","\n"),"<br/>",$result->message) :'';?></p>
											</div>
										</div>
										
									</div>
									<div class="tab_detail_div tab-pane fade {{$comment_section_active_in}}" id="comment_data">
											<div class="row pad">
											<div class="col-md-12 col-sm-12" >	
												<div id="info1"></div>
												<div class="box-body" style="display: block;padding:15px;">  
													{{ Form::open(['role' => 'form','url' => 'adminpnlx/support/save-comment/'.$result->id,'class' => 'mws-form', 'files' => true]) }}
														<div class="form-group<?php echo ($errors->first('comment')?'has-error':''); ?>  ">
															<div class="mws-form-row">
																<label class="mws-form-label">{{ trans("Comment") }} <span class="requireRed"> * </span></label>
																<div class="mws-form-item">
																{{ Form::textarea('comment','', ['class' => 'form-control textarea_resize' ,"rows"=>5,"cols"=>5]) 	}}
																	<div class="error-message help-inline">
																		<?php echo $errors->first('comment'); ?>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group display_inline">
															<label class="mws-form-label">{{ trans("Attachment") }} <span class="requireRed">  </span></label>
															
															<div class="mws-form-item">
																{{ Form::file('image',['class' => '',"accept"=>".jpg,.jpeg,.png,.gif,.pdf,.doc,.xls,.docx"] )}}
																<div class="error-message help-inline">
																	<?php echo $errors->first('image'); ?>
																</div>
															</div>
														</div>
														<div class="mws-button-row display_inline" style="float: left;">
															<input type="submit" value="{{ trans('Post Comment') }}" class="btn btn-danger" style="float:right;">
														</div>
													{{ Form::close() }}
												</div>
											</div>
										</div>
										<ul class="comment_box">
											@if(!$result->ticket_comment->isEmpty())
												@foreach($result->ticket_comment as $ticket_comment_detail)
													<?php
														if(Auth::user()->id == $ticket_comment_detail->user_id) {
															$login_user			=		"";
															$login_user_cls		=		"";
														}else {
															$login_user			=		"user_comment_right";
															$login_user_cls			=	"pan_div_right";
														}
													?>
													<li>
														<div class="{{ $login_user }}">
															<div class="pan_div {{ $login_user_cls }}">
																<div class="chat_msg">
																	<p>{{ $ticket_comment_detail->comment}}</p>
																</div>
																<div class="admin_reply">
																	<div class="chat_msg_hd">
																		<div class="chat_msg_tm"></div>
																	</div>
																	@if(!empty($ticket_comment_detail))
																		<div class="admin_attachement">
																		<?php
																		$oldImage	=	Input::old('comment_image');
																		$image		=	isset($oldImage) ? $oldImage : $ticket_comment_detail->comment_image;
																		$extension 	=   File::extension($image); 
																		?>
														@if($ticket_comment_detail->comment_image != '' && File::exists(TICKET_COMMENT_IMAGE_ROOT_PATH.$ticket_comment_detail->comment_image))
																			<a href="<?php echo TICKET_COMMENT_IMAGE_URL.$image; ?>" download>
																				<img style="height:50px; width:50px;" src="<?php echo WEBSITE_IMG_URL;?>/download.jpeg">
																			</a>
																		@endif
																		</div>
																	@endif
																	<div class="chat_msg_nme">
																		Posted By {{ (!empty($ticket_comment_detail->comment_from)) ? $ticket_comment_detail->comment_from : $ticket_comment_detail->comment_email }} On {{ date("M d, Y",strtotime($ticket_comment_detail->created_at))}}<b>@</b>{{date("H:i",strtotime($result->created_at))}}</div>
																</div>
															</div>
														</div>
													</li>
												@endforeach
											@else 
												<li style="text-align:center;" ><h3><b>No comment on this ticket.</b></h3></li>
											@endif
										</ul>
								
									</div>
									<div class="tab_detail_div tab-pane fade" id="logs_data">
										<div class="main_content_logs">
											<div class="log_data">
												<ul class="log_data_ul">
													@if(!$result->ticket_log_details->isEmpty())
														@foreach($result->ticket_log_details as $ticket_log)
															<?php
																$get_ticket_details		=	CustomHelper::get_ticket_details(json_decode($ticket_log->data_string));
																
																$service_board_name		=	!empty($get_ticket_details["service_board_name"]) ? $get_ticket_details["service_board_name"]  : "";
																$agent_name				=	!empty($get_ticket_details["agent_name"]) ? $get_ticket_details["agent_name"]  : "";
																$new_agent_name			=	!empty($get_ticket_details["new_agent_name"]) ? $get_ticket_details["new_agent_name"]  : "";
																$new_priority			=	!empty($get_ticket_details["new_priority"]) ? $get_ticket_details["new_priority"]  : "";
																$old_priority			=	!empty($get_ticket_details["old_priority"]) ? $get_ticket_details["old_priority"]  : "";
																
																$newDatajson_decode		=	json_decode($ticket_log->data_string);
																$status_name			=	!empty($newDatajson_decode->status_name) ? $newDatajson_decode->status_name  : "";
															?>
															@if($ticket_log->action_name == TICKET_CREATED)
																<li class="logs_entries orange">
																	<div class="logs_txt">
																		Ticket created by {{$ticket_log->activity_by}}.
																	</div>
																	<div class="log_date_time">
																		<span class="log_date">{{ date("M d, Y",strtotime($ticket_log->created_at))}}<b>@</b>{{date("H:i",strtotime($ticket_log->created_at))}}</span>
																	</div>
																</li>
															@elseif($ticket_log->action_name == TICKET_CHANGED_STATUS)
																<li class="logs_entries blue">
																	<div class="logs_txt">
																		Ticket status has been changed with "{{$status_name}}" by  {{$ticket_log->activity_by}}.
																	</div>
																	<div class="log_date_time">
																		<span class="log_date">{{ date("M d, Y",strtotime($ticket_log->created_at))}}<b>@</b>{{date("H:i",strtotime($ticket_log->created_at))}}</span>
																	</div>
																</li>
															@elseif($ticket_log->action_name == TICKET_NEW_COMMENT)
																<li class="logs_entries green">
																	<div class="logs_txt">
																		New comment has posted by  {{$ticket_log->activity_by}}.
																	</div>
																	<div class="log_date_time">
																		<span class="log_date">{{ date("M d, Y",strtotime($ticket_log->created_at))}}<b>@</b>{{date("H:i",strtotime($ticket_log->created_at))}}</span>
																	</div>
																</li>
															@elseif($ticket_log->action_name == TICKET_NEW_NOTES)
																<li class="logs_entries red">
																	<div class="logs_txt">
																		New note added by {{$ticket_log->activity_by}}.
																	</div>
																	<div class="log_date_time">
																		<span class="log_date">{{ date("M d, Y",strtotime($ticket_log->created_at))}}<b>@</b>{{date("H:i",strtotime($ticket_log->created_at))}}</span>
																	</div>
																</li>
															@endif
															
														@endforeach
													@endif
												</ul>
											</div>
										</div>
									</div>
									<div class="tab_detail_div tab-pane fade" id="attachment_data">
										<div class="ticket_attachment_inner">
											<div class="ticket_attachment_data">
												<ul class="ticket_attachment_data_ul">
													<?php 
														if(!empty($result->image)){
															$image		=	$result->image;
															$extension 	=   File::extension($image); 
													?>
														<li>
															@if($image != '' && File::exists(TICKETS_IMAGE_ROOT_PATH.$image)) 
																<?php if (in_array( $extension, $docarr)){ ?>
																	<a href="<?php echo TICKETS_IMAGE_URL.$image; ?>" download>
																		<div class="usermgmt_image">
																			<img style="height:150px; width:150px;" src="<?php echo WEBSITE_IMG_URL;?>/admin/xls.png"> 
																		</div>
																	</a>
																<?php } else if (in_array($extension , $pdfarr )){ ?>
																	<a href="<?php echo TICKETS_IMAGE_URL.$image; ?>" download>
																		<div class="usermgmt_image">
																			<img style="height:150px; width:150px;" src="<?php echo WEBSITE_IMG_URL;?>/admin/pdf.png">
																		</div>
																	</a>
																<?php } else {?>
																	<a href="<?php echo TICKETS_IMAGE_URL.$image; ?>" download>
																		<div class="usermgmt_image" id="other_image_<?php echo $result->id ?>">
																			<img id="delete_image_attr" src="<?php echo WEBSITE_URL.'image.php?width=150px&height=150px&cropratio=1:1&image='.TICKETS_IMAGE_URL.'/'.$image ?>" style="margin-left:5px" class="">
																		</div>
																	</a>
																<?php } ?>
															@endif
														</li>
													<?php  
														}else{
													?> 
														<h3 style="text-align:center;">No attachment on this ticket.</h3>
													<?php } ?>
												</ul>
											</div>
										</div>
									</div>
									<div class="tab_detail_div tab-pane fade" id="NOTES">
										<div class="row pad">
											<div class="col-md-12 col-sm-12" >	
												<div id="info1"></div>
												<div class="box-body" style="display: block;padding:15px;">  
													{{ Form::open(['role' => 'form','url' => 'adminpnlx/support/save-notes','class' => 'mws-form saveNotesForm']) }}
														<div class="form-group<?php echo ($errors->first('comment')?'has-error':''); ?>  ">
															<div class="mws-form-row">
																<label class="mws-form-label">{{ trans("Note") }}<span class="requireRed"> * </span></label>
																<div class="mws-form-item">
																<input type="hidden" id="ticket_id" name="ticket_id" value="{{$result->id}}"/>
																{{ Form::textarea('notes','', ['class' => 'form-control textarea_resize noteId' ,"rows"=>5,"cols"=>5]) 	}}
																	<div class="error-message help-inline">
																		<?php echo $errors->first('notes'); ?>
																	</div>
																</div>
															</div>
														</div>
														<div class="mws-button-row display_inline" style="float: left;">
															<input type="button" value="{{ trans('Add') }}" class="btn btn-danger" style="float:right;" onclick="save_notes();">
														</div>
													{{ Form::close() }}
												</div>
											</div>
										</div>
										<ul class="comment_box">
											@if(!$result->ticket_note_details->isEmpty())
												@foreach($result->ticket_note_details as $ticket_comment_detail)
													<?php
														if(Auth::user()->id == $ticket_comment_detail->user_id) {
															$login_user			=		"";
															$login_user_cls		=		"";
														}else {
															$login_user			=		"user_comment_right";
															$login_user_cls			=	"pan_div_right";
														}
													?>
													<li>
														<div class="{{ $login_user }}">
															<div class="pan_div {{ $login_user_cls }}" style="max-width: 100%;">
																<div class="chat_msg">
																	<p><?php echo $ticket_comment_detail->notes; ?></p>
																</div>
																<div class="admin_reply">
																	<div class="chat_msg_hd">
																		<div class="chat_msg_tm"></div>
																	</div>
																	<div class="chat_msg_nme">
																		Notes By {{ (!empty($ticket_comment_detail->notes_by)) ? $ticket_comment_detail->notes_by : $ticket_comment_detail->notes_email }} On {{ date("M d, Y",strtotime($ticket_comment_detail->created_at))}}<b>@</b>{{date("H:i",strtotime($result->created_at))}}</div>
																</div>
															</div>
														</div>
													</li>
												@endforeach
											@else 
												<li style="text-align:center;" ><h3><b>No notes on this ticket.</b></h3></li>
											@endif
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<script>
	
	
	function save_notes(){
		$('#loader_img').show();
		ticket_id 	=	$("#ticket_id").val();
		noteId 		=	$(".noteId").val().trim();
		if(noteId != ""){
			$(".saveNotesForm").submit();
		}else{
			$(".noteId").next().html("This field is required.");
			$(".noteId").css("border","1px solid red");
			$('#loader_img').hide();
		}
		
	}
</script>
@stop