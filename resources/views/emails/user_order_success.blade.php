<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{ Config::get('Site.title') }}</title>
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tbody>
       <tr>
          <td align="center">
             <table cellspacing="0" cellpadding="0" border="0" style="width: 680px; border-collapse: collapse;">
                <tbody>
                   <tr>
                      <td style="padding-left: 15px; padding-right: 15px; padding-top: 15px; padding-bottom: 15px;background:#f5f5f5">
                         <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                            <tbody>
                               <tr>
                                  <td>
                                     <a href="javascript:void(0)">
                                     <img src="{{ url('img/logo.png') }}" style="width: 122px;">
                                     </a>
                                  </td>
                                  <!-- <td style="text-align: right; font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; vertical-align: bottom;">
                                     Owned &amp; Operated By <br> Australia’s Premier Cruise Operator
                                  </td> -->
                               </tr>
                            </tbody>
                         </table>
                      </td>
                   </tr>
                   
                   
                   <tr>
                     @if(!empty($user))
                    <td style="padding-bottom:15px;padding-top:15px;font-family: Arial, Helvetica, sans-serif; font-size: 24px; color: #000;font-weight: bold;">
                        Hello {{ $user->first_name }} {{ $user->last_name }}
                    </td>
                    @endif
                   </tr>
                   <tr>
                    
                    <td style="padding-bottom:30px;font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #000;">
                        Thank your for purchase. Below is your order information
                    </td>
                   </tr>
                   <tr>
                    <td style=" padding-bottom: 15px; background: #fff;">
                       <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                          <tbody>
                            

                             <tr>
                               @if(!empty($user))
                                <td style="padding-right: 13px; vertical-align: top; width: 50%;">
                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;border: solid 1px #162061;">
                                        <tbody>
                                            <tr>
                                                <td style="background:#162061;font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #ffffff; padding: 15px 20px;">
                                                    Order Information
                                                 </td>
                                            </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px; padding-top: 15px; padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Order Number
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                             {{  $user->order_number }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Total Amount
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;font-weight: bold;">
                                                          {{ $user->grand_total }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <!-- <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Shipping Charges
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                             #50
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr> -->
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                            Order Date
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          {{ date(Config::get("Reading.date_format") , strtotime($user->created_at)) }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                            Status
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; ">
                                                          {{ $user->status }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                        </tbody>
                                    </table>

                                </td>
                                @endif

                                @if(!empty($user))
                                <td style="padding-left: 13px; vertical-align: top; width: 50%;">
                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;border: solid 1px #162061;">
                                        <tbody>
                                            <tr>
                                                <td style="background:#162061;font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #ffffff; padding: 15px 20px;">
                                                    Shipping Information
                                                 </td>
                                            </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px; padding-top: 15px; padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Name
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          {{ $user->first_name }} {{ $user->last_name }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Email
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          {{ $user->email }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Mobile Number
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          {{ $user->phone_number }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                            Address
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          {{ $user->address }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                            Post Code
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; ">
                                                          {{ $user->postal_code }}
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                        </tbody>
                                    </table>
                                </td>
                                @endif
                             </tr>
                            
                             
                          </tbody>
                       </table>
                    </td>
                 </tr>
                 <tr>
                    <td style="padding-bottom:15px;padding-top:15px;font-family: Arial, Helvetica, sans-serif; font-size: 24px; color: #000;font-weight: bold;">
                        Order Details
                    </td>
                    
                   </tr>
                 <tr>
                    <td style="font-family: Arial, Helvetica, sans-serif;">
                        <table cellspacing="0" cellpadding="0"  style="width: 100%; border-collapse: collapse; border: solid 1px #a5a5a5;">
                            <tbody>
                                
                                <tr>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px; border: solid 1px #a5a5a5;">
                                        Image
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        Name
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        Size
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        color
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        Quantity
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        Price
                                    </th>
                                   </tr>
                                @foreach($order_details as $order)  
                               <tr>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                    <img src="{{PRODUCT_IMAGE_URL.$order->product->image }}" alt="Prodcut Images" style="width: 100px;">
                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                    {{ $order->product->name }}
                                </td>
                                
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                  @if($order->product_variation->size != null) {{ $order->product_variation->size->name }} @endif
                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                 @if($order->product_variation->color != null) {{ $order->product_variation->color->name }} @endif
                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                    {{ $order->quantity }}
                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                    {{ Config::get('Site.currencyCode').number_format($order->price, 2, '.', ',') }}
                                </td>                                
                               </tr>
                               @endforeach
                               <tr>                                                                
                              
                               </tr>
                            </tbody>
                        </table>
                    </td>
                 </tr>
                </tbody>
             </table>
          </td>
       </tr>
    </tbody>
 </table>
</body>
</html>