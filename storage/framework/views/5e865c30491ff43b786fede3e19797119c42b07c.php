<?php $__env->startSection('content'); ?>
<!-- CKeditor start here-->
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/ckeditor/ckeditor.js"></script>
<?php echo e(HTML::script('js/admin/chosen/chosen.jquery.min.js')); ?>

<?php echo e(HTML::style('css/admin/chosen.min.css')); ?>


<?php echo e(HTML::script('js/admin/bootstrap-multiselect.js')); ?>

<?php echo e(HTML::style('css/admin/bootstrap-multiselect.css')); ?>

<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Edit <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">Edit <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			<?php echo e(Form::open(['role' => 'form','url' =>  route("$modelName.edit",$model->id),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"])); ?>

			<div class="row">
				
				<div class="col-md-6">
					<?php if(count($languages) > 1): ?>
					<div  class="default_language_color">
						<?php echo e(Config::get('default_language.message')); ?>

					</div>
					<div class="wizard-nav wizard-nav-horizontal">
						<ul class="nav nav-tabs">
							<?php $i = 1 ; ?>
							<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li class=" <?php echo e(($i ==  $language_code )?'active':''); ?>">
								<a data-toggle="tab" href="#<?php echo e($i); ?>div">
									<?php echo e($value -> title); ?>

								</a>
							</li>
							<?php $i++; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
					<?php endif; ?>
					<?php if(count($languages) > 1): ?>
					<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
						<hr class ="hrLine"/>
						<b><?php echo e(trans("These fields (above seperator line) are same in all languages")); ?></b>
					</div>
					<?php endif; ?>
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div id="<?php echo e($i); ?>div" class="tab-pane <?php echo e(($i ==  $language_code )?'active':''); ?> ">
							<div class="mws-form-inline">
								<div class="form-group <?php if($i == 1){ echo ($errors->first('name')?'has-error':'');} ?>">
									<?php if($i == 1): ?>
									<?php echo HTML::decode( Form::label($i.'.name',trans("Product Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<?php else: ?>
									<?php echo HTML::decode( Form::label($i.'.name',trans("Product Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])); ?>

									<?php endif; ?>
									<div class="mws-form-item">
										<?php echo e(Form::text("data[$i][name]",isset($multiLanguage[$i]['name'])?$multiLanguage[$i]['name']:'', ['class' => 'form-control'])); ?>

										<div class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('name') : ''; ?>
										</div>
									</div>
								</div>


								<div class="form-group <?php if($i == 1){ echo ($errors->first('description')?'has-error':'');} ?>">
									<?php if($i == 1): ?>
									<?php echo HTML::decode( Form::label($i.'._description',trans("Description").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])); ?>

									<?php else: ?>
									<?php echo HTML::decode( Form::label($i.'._description',trans("Description").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])); ?>

									<?php endif; ?>
									<div class="mws-form-item">
										<?php echo e(Form::textarea("data[$i][description]",isset($multiLanguage[$i]['description'])?$multiLanguage[$i]['description']:'', ['class' => 'form-control textarea_resize','id' => 'description_'.$i ,"rows"=>3,"cols"=>3])); ?>

										<span class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('description') : ''; ?>
										</span>
									</div>
									<script type="text/javascript">
										/* For CKEDITOR */

										CKEDITOR.replace( <?php echo 'description_'.$i; ?>,
										{
											height: 250,
											filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
											filebrowserImageWindowWidth : '640',
											filebrowserImageWindowHeight : '480',
											enterMode : CKEDITOR.ENTER_BR
										});

									</script>
								</div>
							</div>
						</div> 
						<?php $i++ ; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						
					</div>
				</div>
				<div class="col-md-6">
					
					<div class="form-group <?php echo ($errors->first('category_id')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('category_id',trans("Category").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::select(
								'category_id',
								[null => 'Select Category'] + $categories,
								$model->category_id,
								['class' => 'form-control','id'=>'category_id']
								)); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('category_id'); ?>
							</div>
						</div>
					</div>



					<div class="form-group <?php echo ($errors->first('brand_id')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('brand_id',trans("Brand").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::select(
								'brand_id',
								[null => 'Select Brand'] + $brand,
								$model->brand_id,
								['class' => 'form-control']
								)); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('brand_id'); ?>
							</div>
						</div>
					</div>
					

					
					
				</div>
				
			</div>
			<table class="table table-striped table-responsive table-bordered">
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th class="mainth" colspan="3" style="color:white;">Variants
						</tr>
					</thead>
				</table>
				<div class="variations_holder">
					<?php if($variations->isNotEmpty()): ?>
					<?php $i = 0;?>
					<?php $__currentLoopData = $variations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $variation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="row variations_inner variations_inner_<?php echo e($i); ?>" data-id="<?php echo e($i); ?>">
						<?php if($i > 0): ?>
						<hr></hr>
						<?php endif; ?>
						<div class="col-md-6">
							<div class="course_price form-group <?php echo ($errors->first('variation_data['.$i.'][color]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data['.$i.'][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::select('variation_data['.$i.'][color]',$color,$variation->color_id,['class' => 'form-control chosen-select valid','id'=>'color_'.$i,'placeholder'=>'Select Color'])); ?>

										<div class="error-message help-inline color_error_<?php echo e($i); ?>">
											<?php echo $errors->first('variation_data['.$i.'][color]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data['.$i.'][quantity]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data['.$i.'][quantity]',trans("Quantity").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::text('variation_data['.$i.'][quantity]',$variation->quantity, ['class' => 'form-control valid','id'=>'quantity_'.$i,'onkeypress'=>'return isNumberKey(event,this)'])); ?>

										<div class="error-message help-inline quantity_error_<?php echo e($i); ?>">
											<?php echo $errors->first('variation_data['.$i.'][quantity]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data['.$i.'][discounted_price]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data['.$i.'][discounted_price]',trans("Discounted Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::text('variation_data['.$i.'][discounted_price]',$variation->discounted_price, ['class' => 'form-control valid','id'=>'discountedprice_'.$i,'onkeypress'=>'return isNumberKey(event,this)'])); ?>

										<div class="error-message help-inline discountedprice_error_<?php echo e($i); ?>">
											<?php echo $errors->first('variation_data['.$i.'][discounted_price]'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="course_price form-group <?php echo ($errors->first('variation_data['.$i.'][size]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data['.$i.'][size]',trans("Size").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::select('variation_data['.$i.'][size]',$size,$variation->size_id,['class' => 'form-control chosen-select valid','id'=>'size_'.$i,'placeholder'=>'Select Size'])); ?>

										<div class="error-message help-inline size_error_<?php echo e($i); ?>">
											<?php echo $errors->first('variation_data['.$i.'][size]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data['.$i.'][price]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data['.$i.'][price]',trans("Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::text('variation_data['.$i.'][price]',$variation->price, ['class' => 'form-control valid','id'=>'price_'.$i,'onkeypress'=>'return isNumberKey(event,this)'])); ?>

										<div class="error-message help-inline price_error_<?php echo e($i); ?>">
											<?php echo $errors->first('variation_data['.$i.'][price]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group <?php echo ($errors->first('variation_data['.$i.'][is_default]')?'has-error':''); ?>">


								<?php echo HTML::decode( Form::label('variation_data['.$i.'][is_default]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

								<?php echo e(Form::checkbox('variation_data['.$i.'][is_default]','',$variation->is_default, ['class' => 'is_default'])); ?>


							</div>
							<?php if($i > 0): ?>
							<div class="form-group ">
								<button type="button" class="btn btn-danger add-row" onclick="remove_variation(<?php echo e($i); ?>);">Remove</button>
							</div>
							<?php endif; ?>
						</div>
						<input type="hidden" name="variation_data[<?php echo e($i); ?>][entry_id]" id="entry_id_<?php echo e($i); ?>" value="<?php echo e($variation->id); ?>">
					</div>

					<?php $i++;?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
					<div class="row variations_inner variations_inner_0" data-id="0">
						<div class="col-md-6">
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][color]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data[0][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::select('variation_data[0][color]',$color,'',['class' => 'form-control chosen-select valid','id'=>'color_0','placeholder'=>'Select Color'])); ?>

										<div class="error-message help-inline color_error_0">
											<?php echo $errors->first('variation_data[0][color]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][quantity]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data[0][quantity]',trans("Quantity").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::text('variation_data[0][quantity]','', ['class' => 'form-control valid','id'=>'quantity_0','onkeypress'=>'return isNumberKey(event,this)'])); ?>

										<div class="error-message help-inline quantity_error_0">
											<?php echo $errors->first('variation_data[0][quantity]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][discounted_price]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data[0][discounted_price]',trans("Discounted Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::text('variation_data[0][discounted_price]','', ['class' => 'form-control valid','id'=>'discountedprice_0','onkeypress'=>'return isNumberKey(event,this)'])); ?>

										<div class="error-message help-inline discountedprice_error_0">
											<?php echo $errors->first('variation_data[0][discounted_price]'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][size]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data[0][size]',trans("Size").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::select('variation_data[0][size]',$size,'',['class' => 'form-control chosen-select valid','id'=>'size_0','placeholder'=>'Select Size'])); ?>

										<div class="error-message help-inline size_error_0">
											<?php echo $errors->first('variation_data[0][size]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="course_price form-group <?php echo ($errors->first('variation_data[0][price]')?'has-error':''); ?>">
								<div class="mws-form-row">
									<?php echo HTML::decode( Form::label('variation_data[0][price]',trans("Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<div class="mws-form-item">
										<?php echo e(Form::text('variation_data[0][price]','', ['class' => 'form-control valid','id'=>'price_0','onkeypress'=>'return isNumberKey(event,this)'])); ?>

										<div class="error-message help-inline price_error_0">
											<?php echo $errors->first('variation_data[0][price]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group <?php echo ($errors->first('variation_data[0][is_default]')?'has-error':''); ?>">


								<?php echo HTML::decode( Form::label('variation_data[0][is_default]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

								<?php echo e(Form::checkbox('variation_data[0][is_default]','','', ['class' => 'is_default'])); ?>

								
							</div>
						</div>
					</div>
					<?php endif; ?>

				</div>
				<div class="row pad">
					<div class="col-md-12 col-sm-12">	
						<div class="col-md-6">	
							<button type="button" name="add" id="add" class="btn btn-success add-row" onclick="add_more_variation();">Add More</button>
						</div>
					</div>
				</div>	



				<table class="table table-striped table-responsive table-bordered">
					<thead>
						<tr style="background-color:#3c3f44; color:white;">
							<th class="mainth" colspan="3" style="color:white;">Images
							</tr>
						</thead>
					</table>
					<div class="images_holder">
						<?php if($ready_made_product_images->isNotEmpty()): ?>
							<?php $i = 0;?>
							<?php $__currentLoopData = $ready_made_product_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ready_made_product_image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="row images_inner images_inner_<?php echo e($i); ?>" data-id="<?php echo e($i); ?>">
									<?php if($i > 0): ?>
									<hr></hr>
									<?php endif; ?>
									<div class="col-md-6">
										<div class="image_price form-group <?php echo ($errors->first('image_data['.$i.'][color]')?'has-error':''); ?>">
											<div class="mws-form-row">
												<?php echo HTML::decode( Form::label('image_data['.$i.'][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

												<div class="mws-form-item">
													<?php echo e(Form::select('image_data['.$i.'][color]',$color,$ready_made_product_image->color_id,['class' => 'form-control chosen-select','placeholder'=>'Select Color'])); ?>

													<div class="error-message help-inline">
														<?php echo $errors->first('image_data['.$i.'][color]'); ?>
													</div>
												</div>
											</div>
										</div>
										<div class="image_price form-group <?php echo ($errors->first('image_data['.$i.'][other_image]')?'has-error':''); ?>">
											<div class="mws-form-row">
												<?php echo HTML::decode( Form::label('image_data['.$i.'][other_image]',trans("Other Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

												<div class="mws-form-item">
													<?php echo e(Form::file('image_data['.$i.'][other_image][]', ['class' => 'form-control','accept'=>"image/*",'multiple'=>"multiple"])); ?>


													<div class="error-message help-inline">
														<?php echo $errors->first('image_data['.$i.'][other_image]'); ?>
													</div>
													<?php if($ready_made_product_image->other_image->isNotEmpty()): ?>
														<?php $__currentLoopData = $ready_made_product_image->other_image; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ready_made_product_images_lists): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<?php if($ready_made_product_images_lists->image != ""): ?>
																<span>
																	<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $ready_made_product_images_lists->image; ?>">
																	<img class="image_remove_<?php echo e($ready_made_product_images_lists->id); ?>" height="25" width="25" src="<?php echo e($ready_made_product_images_lists->image); ?>" /></a>
																	
																	<a  onclick="remove_image_selected(<?php echo e($ready_made_product_images_lists->id); ?>);" href="javascript:void(0);">
																		<span class="fa fa-trash-o"></span>
																	</a> 
																</span>
															<?php endif; ?>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													<?php endif; ?>
												</div>
											</div>
										</div>
										
									</div>
									<div class="col-md-6">
										<div class="image_price form-group <?php echo ($errors->first('image_data['.$i.'][image]')?'has-error':''); ?>">
											<div class="mws-form-row">
												<?php echo HTML::decode( Form::label('image_data['.$i.'][image]',trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

												<div class="mws-form-item">
													<?php echo e(Form::file('image_data['.$i.'][image]',null,$ready_made_product_image->image, ['class' => 'form-control','id'=>'image_'.$i,'accept'=>"image/*"])); ?>

													<div class="error-message help-inline image_error_<?php echo e($i); ?>">
														<?php echo $errors->first('image_data['.$i.'][image]'); ?>
													</div>
													<?php if($ready_made_product_image->image != ""): ?>
														<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $ready_made_product_image->image; ?>">
														<img height="50" width="50" src="<?php echo e($ready_made_product_image->image); ?>" /></a>
													<?php endif; ?>
												</div>
											</div>
											<div class="form-group <?php echo ($errors->first('image_data['.$i.'][is_defaults]')?'has-error':''); ?>">
												<?php echo HTML::decode( Form::label('image_data['.$i.'][is_defaults]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

												<?php echo e(Form::checkbox('image_data['.$i.'][is_defaults]','',$ready_made_product_image->is_default, ['class' => 'is_defaults'])); ?>


											</div>
											<?php if($i > 0): ?>
											<div class="form-group ">
												<button type="button" class="btn btn-danger add-row" onclick="remove_image(<?php echo e($i); ?>);">Remove</button>
											</div>
											<?php endif; ?>
										</div>
										<input type="hidden" name="image_data[<?php echo e($i); ?>][entry_id]" id="entry_id_<?php echo e($i); ?>" value="<?php echo e($ready_made_product_image->id); ?>">
									</div>

										<?php $i++;?>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php else: ?>
							<div class="row images_inner images_inner_0" data-id="0">
								<div class="col-md-6">
									<div class="image_price form-group <?php echo ($errors->first('image_data[0][color]')?'has-error':''); ?>">
										<div class="mws-form-row">
											<?php echo HTML::decode( Form::label('image_data[0][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

											<div class="mws-form-item">
												<?php echo e(Form::select('image_data[0][color]',$color,'',['class' => 'form-control chosen-select','placeholder'=>'Select Color'])); ?>

												<div class="error-message help-inline">
													<?php echo $errors->first('image_data[0][color]'); ?>
												</div>
											</div>
										</div>
									</div>
									<div class="image_price form-group <?php echo ($errors->first('image_data[0][other_image]')?'has-error':''); ?>">
										<div class="mws-form-row">
											<?php echo HTML::decode( Form::label('image_data[0][other_image]',trans("Other Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

											<div class="mws-form-item">
												<?php echo e(Form::file('image_data[0][other_image][]', ['class' => 'form-control','accept'=>"image/*",'multiple'=>"multiple"])); ?>

												<div class="error-message help-inline">
													<?php echo $errors->first('image_data[0][other_image]'); ?>
												</div>

											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="image_price form-group ">
										<div class="mws-form-row">
											<?php echo HTML::decode( Form::label('image_data[0][other_image]',trans("Main Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

											<div class="mws-form-item">
												<?php echo e(Form::file('image_data[0][image]', ['class' => 'form-control','id'=>'image_0','accept'=>"image/*"])); ?>

												<div class="error-message help-inline image_error_0">
													
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-group <?php echo ($errors->first('image_data[0][is_defaults]')?'has-error':''); ?>">
										<?php echo HTML::decode( Form::label('image_data[0][is_defaults]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

										<?php echo e(Form::checkbox('image_data[0][is_defaults]','','', ['class' => 'is_defaults'])); ?>

									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
					<div class="row pad">
						<div class="col-md-12 col-sm-12">	
							<div class="col-md-6">	
								<button type="button" name="add" id="add" class="btn btn-success add-row" onclick="add_more_images();">Add More</button>
							</div>
						</div>
					</div>	

					<div class="mws-button-row">
						<input type="submit" id="edit_product" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
						<a href='<?php echo e(route("$modelName.edit",$model->id)); ?>' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> <?php echo e(trans('Clear')); ?></a>
						<a href="<?php echo e(route($modelName.'.index')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
					</div>
					<?php echo e(Form::close()); ?> 
			</div>
		</section>
		<style>
		.textarea_resize {
			resize: vertical;
		}
		.error{
			color: red;
		}
	</style>
	<script>
		function add_more_variation(){
			$("#loader_img").show();
			var get_last_id  = $('.variations_holder > div.variations_inner').last().attr('data-id');
		// alert(get_last_id);
		var counter  	 		=  	parseInt(get_last_id) + 1;
		//alert(counter);
		$.ajax({
			headers				:	{"X-CSRF-TOKEN":"<?php echo e(csrf_token()); ?>"},
			url					:	"<?php echo e(route('ReadyMadeProducts.addMoreVariation')); ?>",
			type				:	"POST",
			data				:	{'counter':counter},
			success				:	function(response){
				$("#loader_img").hide();
				$('.variations_holder').append(response);
			}
		});
	}
	function remove_variation(id){
		bootbox.confirm("Are you sure want to remove this ?",
			function(result){
				if(result){
					var entry_id = $("#entry_id_"+id).val();
					if(typeof entry_id === "undefined"){
						$('.variations_inner_'+id).remove();
					}else{
						$.ajax({
							headers				:	{"X-CSRF-TOKEN":"<?php echo e(csrf_token()); ?>"},
							url					:	"<?php echo e(route('ReadyMadeProducts.removeVariation')); ?>",
							type				:	"POST",
							data				:	{'entry_id':entry_id},
							success				:	function(response){
								if(response == 1){
									$('.variations_inner_'+id).remove();
								}
								$("#loader_img").hide();
							}
						});
					}
				}
			}
			);
	}
	$(document).on('change', '.is_default', function(){
		if($(this).prop("checked") == true){
			$('.is_default').not(this).prop('checked', false);  
		}
	});

	function add_more_images(){
		$("#loader_img").show();
		var get_last_id  = $('.images_holder > div.images_inner').last().attr('data-id');
		// alert(get_last_id);
		var counter  	 		=  	parseInt(get_last_id) + 1;
		//alert(counter);
		$.ajax({
			headers				:	{"X-CSRF-TOKEN":"<?php echo e(csrf_token()); ?>"},
			url					:	"<?php echo e(route('ReadyMadeProducts.addMoreImage')); ?>",
			type				:	"POST",
			data				:	{'counter':counter},
			success				:	function(response){
				$("#loader_img").hide();
				$('.images_holder').append(response);
			}
		});
	}
	
	function remove_image(id){
		bootbox.confirm("Are you sure want to remove this ?",
			function(result){
				if(result){
					var entry_id = $("#entry_id_"+id).val();
					if(typeof entry_id === "undefined"){
						$('.images_inner_'+id).remove();
					}else{
						$.ajax({
							headers				:	{"X-CSRF-TOKEN":"<?php echo e(csrf_token()); ?>"},
							url					:	"<?php echo e(route('ReadyMadeProducts.removeImage')); ?>",
							type				:	"POST",
							data				:	{'entry_id':entry_id},
							success				:	function(response){
								if(response == 1){
									$('.images_inner_'+id).remove();
								}
								$("#loader_img").hide();
							}
						});
					}
				}
			}
			);
	}

	function remove_image_selected(id){
		bootbox.confirm("Are you sure want to remove this image ?",
			function(result){
				if(result){
					var entry_id = id;
					if(typeof entry_id === "undefined"){
						$('.image_remove_'+id).parent().parent().remove();
					}else{
						$.ajax({
							headers				:	{"X-CSRF-TOKEN":"<?php echo e(csrf_token()); ?>"},
							url					:	"<?php echo e(route('ReadyMadeProducts.removeMainImage')); ?>",
							type				:	"POST",
							data				:	{'entry_id':entry_id},
							success				:	function(response){
								if(response == 1){
									$('.image_remove_'+id).parent().parent().remove();
								}
								$("#loader_img").hide();
							}
						});
					}
				}
			}
			);
	}
	$(document).on('change', '.is_defaults', function(){
		if($(this).prop("checked") == true){
			$('.is_defaults').not(this).prop('checked', false);  
		}
	});

	<?php if($model->category_id): ?>

	$.ajax({
		method  :'GET',
		url     :"<?php echo e(route('ReadyMadeProducts.subCategory')); ?>",
		data    :   { category_id:"<?php echo e($model->category_id); ?>", "_token": $('meta[name="csrf-token"]').attr('content') },

		
		success: function (data)
		{
			$("#loader_img").hide();
			$(".sub-category-container").remove();    

			if(data.status){

				$('.category-container').after(data.msg);
				$("#sub_category_id").val("<?php echo e($model->sub_category_id); ?>");
			}
			else{

			}
		},
		error :function( data ) {
		}
	});
	<?php endif; ?>

	$('#edit_product').on('click',function(e){
		$(".help-inline").removeClass("error");
		$(".help-inline").html("");
		$("#loader_img").show();

		var error 				= 	0;
		let validExt = ['jpg','jpeg','png','gif','bmp'];
		$(".valid").each(function(){ 
			var value			=	$(this).val();
			var id				=	$(this).attr("id"); 
			var idarr			=	id.split("_"); 
			if(value == ''){
				error = 1;
				$("."+idarr[0]+"_"+idarr[1]).focus();
				$("."+idarr[0]+"_error_"+idarr[1]).addClass("error");
				$("."+idarr[0]+"_error_"+idarr[1]).html("This field is required.");
				$("#loader_img").hide();				
			}
			if($('#quantity_'+idarr[1]).val() == 0 && $('#quantity_'+idarr[1]).val() != ''){
				error = 1;
				$(".quantity_error_"+idarr[1]).addClass("error");
				$(".quantity_error_"+idarr[1]).html("Quantity should be greater then 0.");
				$("#loader_img").hide();
			}
			if($('#price_'+idarr[1]).val() == 0 && $('#price_'+idarr[1]).val() != ''){
				error = 1;
				$(".price_error_"+idarr[1]).addClass("error");
				$(".price_error_"+idarr[1]).html("Price should be greater then 0.");
				$("#loader_img").hide();
			}
			var exten = $("#image_"+idarr[1]).val().split('.').pop().toLowerCase();					
			if(validExt.indexOf(exten) == -1 && $("#image_"+idarr[1]).val() !=''){
				error	=	1;
				$(".image_error_"+idarr[1]).addClass("error");
				$(".image_error_"+idarr[1]).html("<?php echo e(trans('Image should be jpeg,jpeg,png,gif,bmp.')); ?>");
				$("#loader_img").hide();
			}			
		});

		if(error == 1){
			e.preventDefault();
		}

	});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/ReadyMadeProducts/edit.blade.php ENDPATH**/ ?>