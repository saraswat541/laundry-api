<?php $__env->startSection('content'); ?>
<script>
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});
</script>
<section class="content-header">
	<h1>
	  <?php echo e($sectionName); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li class="active"> <?php echo e($sectionName); ?></li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => route("$modelName.index"),'class' => 'row mws-form'])); ?>

			<?php echo e(Form::hidden('display')); ?>>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => ' form-control','placeholder'=>'Name'])); ?>

				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href='<?php echo e(route("$modelName.index")); ?>'  class="btn btn-primary"> <i class="fa fa-refresh "></i> <?php echo e(trans('Clear Search')); ?></a>
			</div>
			<?php echo e(Form::close()); ?> 
		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title"><?php echo e($sectionName); ?>'s List</h3>
				<div class="listing-btns">
					<!--<a href='<?php echo e(route("$modelName.add")); ?>'  class="btn btn-success btn-small pull-right"> <?php echo e(trans("Add New ")); ?><?php echo e($sectionNameSingular); ?> </a>-->
				</div>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="20%">
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Page Name"),
									array(
									'sortBy' => 'page_name',
									'order' => ($sortBy == 'page_name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'page_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'page_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> 
						<th width="20%">
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Name"),
									array(
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> 
						<th width="30%">
							Description
						</th>  
						<th width="10%">
							Image
						</th> 
						<th width="8%" class="action-th"> 
							<?php echo e(link_to_route(
								"$modelName.index",
								trans("Order"),
								array(
								'sortBy' => 'block_order',
								'order' => ($sortBy == 'block_order' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'block_order' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'block_order' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="8%"><?php echo e(trans("Action")); ?></th>
					</tr>
				</thead>
					<tbody id="powerwidgets">
						<?php if(!$results->isEmpty()): ?>
							<?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr class="items-inner">
									<td data-th='page_name'><?php echo e($result->page_name); ?></td>
									<td data-th='name'><?php echo e($result->name); ?></td>
									</td>
									<td data-th='name'><?php echo e(strip_tags(Str::limit($result->description, 300))); ?></td>	
									<td data-th='name'>
										<?php if($result->image != ""): ?>
											<br />
											<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $result->image; ?>">
												<img class="" src="<?php echo $result->image; ?>" width="50px" height="50px">
											</a>
										<?php endif; ?>
									</td>
									<td data-th='name'><?php echo e($result->block_order); ?></td>
									<td data-th='' class="action-td">
										<a href='<?php echo e(route("$modelName.edit",[$result->id])); ?>' class="btn btn-primary" title="Edit"> <span class="fa fa-pencil"></span></a>
									</td>
								</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
						<?php else: ?>
							<tr>
								<td colspan="12" class="alignCenterClass"> <?php echo e(trans("Record not found.")); ?></td>
							</tr>
						<?php endif; ?> 
					</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $results], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/Blocks/index.blade.php ENDPATH**/ ?>