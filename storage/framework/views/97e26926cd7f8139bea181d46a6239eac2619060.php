<?php $__env->startSection('content'); ?>
<!-- CKeditor start here-->
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/ckeditor/ckeditor.js"></script>

<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Add New <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">Add New <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
	    <div class="box-body">
		    <div class="row">
				<?php echo e(Form::open(['role' => 'form','url' =>  route("$modelName.add"),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"])); ?>

				<div class="col-md-6">
					<?php if(count($languages) > 1): ?>
						<div  class="default_language_color">
							<?php echo e(Config::get('default_language.message')); ?>

						</div>
						<div class="wizard-nav wizard-nav-horizontal">
							<ul class="nav nav-tabs">
								<?php $i = 1 ; ?>
								<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li class=" <?php echo e(($i ==  $language_code )?'active':''); ?>">
										<a data-toggle="tab" href="#<?php echo e($i); ?>div">
											<?php echo e($value -> title); ?>

										</a>
									</li>
									<?php $i++; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					<?php endif; ?>
					<?php if(count($languages) > 1): ?>
						<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
							<hr class ="hrLine"/>
							<b><?php echo e(trans("These fields (above seperator line) are same in all languages")); ?></b>
						</div>
					<?php endif; ?>
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div id="<?php echo e($i); ?>div" class="tab-pane <?php echo e(($i ==  $language_code )?'active':''); ?> ">
								<div class="mws-form-inline">
									<div class="form-group <?php if($i == 1){ echo ($errors->first('name')?'has-error':'');} ?>">
										<?php if($i == 1): ?>
											<?php echo HTML::decode( Form::label($i.'.name',trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

										<?php else: ?>
											<?php echo HTML::decode( Form::label($i.'.name',trans("Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])); ?>

										<?php endif; ?>
										<div class="mws-form-item">
											<?php echo e(Form::text("data[$i][name]",'', ['class' => 'form-control'])); ?>

											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('name') : ''; ?>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group <?php if($i == 1){ echo ($errors->first('description')?'has-error':'');} ?>">
									<?php if($i == 1): ?>
									<?php echo HTML::decode( Form::label($i.'._description',trans("Description").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])); ?>

									<?php else: ?>
									<?php echo HTML::decode( Form::label($i.'._description',trans("Description").'<span class="requireRed"></span>', ['class' => 'mws-form-label'])); ?>

									<?php endif; ?>
									<div class="mws-form-item">
										<?php echo e(Form::textarea("data[$i][description]",'', ['class' => 'form-control textarea_resize','id' => 'description_'.$i ,"rows"=>3,"cols"=>3])); ?>

										<span class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('description') : ''; ?>
										</span>
									</div>
									<script type="text/javascript">
									/* For CKEDITOR */
										
										CKEDITOR.replace( <?php echo 'description_'.$i; ?>,
										{
											height: 250,
											filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
											filebrowserImageWindowWidth : '640',
											filebrowserImageWindowHeight : '480',
											enterMode : CKEDITOR.ENTER_BR
										});
											
									</script>
								</div>
							</div> 
						<?php $i++ ; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<div class="mws-button-row">
							<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
							<a href="<?php echo e(route($modelName.'.add')); ?>" class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> <?php echo e(trans('Clear')); ?></a>
							<a href="<?php echo e(route($modelName.'.index')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
						</div>
					</div>
					
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('page_name')?'has-error':''); ?>">
						<div class="mws-form-row">
							<?php echo HTML::decode( Form::label('page_name',trans("Page Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::text('page_name','', ['class' => 'form-control'])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('page_name'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group <?php echo ($errors->first('image')?'has-error':''); ?>">
						<div class="mws-form-row">
							<?php echo HTML::decode( Form::label('image',trans("Image").'<span class="requireRed"> </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::file('image', ['class' => ''])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('image'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php echo e(Form::close()); ?> 
			</div>
		</div>
	</div>
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/Blocks/add.blade.php ENDPATH**/ ?>