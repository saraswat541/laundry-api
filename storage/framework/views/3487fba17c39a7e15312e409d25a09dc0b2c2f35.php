<!DOCTYPE html>
<html>
	<head>
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
		<link rel="shortcut icon" href="<?php echo e(WEBSITE_IMG_URL); ?>fav.png">
		<title><?php echo e(Config::get("Site.title")); ?></title>
		<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/jquery.min.js"></script>
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/ionicons.min.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/morris/morris.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/themify-icons.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/bootmodel.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/notification/jquery.toastmessage.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/AdminLTE.css" rel="stylesheet">
		<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/custom_admin.css" rel="stylesheet">
		<script src="<?php echo e(WEBSITE_CSS_URL); ?>admin/notification/jquery.toastmessage.js"></script>
		<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/jquery.peity.js"></script>
		<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/apexcharts.min.js"></script>
		<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/jquery.slimscroll.min.js"></script>
		<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/vendors/match-height/jquery.equalheights.js"></script>
	</head>
	<body class="skin-blue">
		<header class="header"> <a href="<?php echo e(URL::to('adminpnlx/dashboard')); ?>" class="logo"> 
		  <!-- Add the class icon to your logo image or logo icon to add the margining --> 
			<?php echo e(Config::get("Site.title")); ?>

		  </a> 
		  <!-- Header Navbar: style can be found in header.less -->
		  <nav class="navbar navbar-static-top" role="navigation"> 
			<!-- Sidebar toggle button--> 
			<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
			<div class="navbar-right">
			  <ul class="nav navbar-nav">
				<li class="dropdown user user-menu"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> <span><?php echo e(Auth::user()->email); ?> <i class="caret"></i></span> </a>
				  <ul class="dropdown-menu">
					<!-- User image -->
					<li class="user-header"> 
						<?php echo HTML::image('img/logo.png','Admin Image',array("class"=>"img-circle")); ?>						
					</li>
				   
					<!-- Menu Footer-->
					<li class="user-footer">
						<div class="pull-left"><a class="btn btn-default btn-flat" href="<?php echo e(URL::to('adminpnlx/myaccount')); ?>"><?php echo e(trans("Edit Profile")); ?> </a> </div>
					  
					  	<div class="pull-right"> <a class="btn btn-default btn-flat" href="<?php echo e(URL::to('adminpnlx/logout')); ?>"><?php echo e(trans("Logout")); ?> </a></div>
					</li>
				  </ul>
				</li>
			  </ul>
			</div>
		  </nav>
		</header>
		
		<div class="wrapper row-offcanvas row-offcanvas-left">
			<?php 
				$segment2	=	Request::segment(1);
				$segment3	=	Request::segment(2); 
				$segment4	=	Request::segment(3); 
				$segment5	=	Request::segment(4); 
			?>
			<aside class="left-side sidebar-offcanvas"> 
				<section class="sidebar"> 
					<ul class='sidebar-menu'>
						<li class="<?php echo e(($segment3 == 'dashboard') ? 'active' : ''); ?> "><a href="<?php echo e(URL::to('adminpnlx/dashboard')); ?>"><i class="fa fa-home  <?php echo e(($segment3 == 'dashboard') ? '' : ''); ?>"></i><?php echo e(trans("Dashboard")); ?> </a></li>
						
						<li class="treeview <?php echo e(in_array($segment3 ,array('users')) ? 'active in' : 'offer-reports'); ?>">
							<a href="javascript::void(0)"><i class="fa fa-users  <?php echo e(in_array($segment3 ,array('users')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Daycare")); ?> </a>
							<ul class="treeview-menu <?php echo e(in_array($segment3 ,array('users')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('users')) ? 'display:block;' : 'display:none;'); ?>">
								
								<li <?php if($segment4!='add-new-user' && ($segment3=='users' || $segment4=='view-user' || $segment4=='edit-user')): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(route('Users.index')); ?>"><i class='fa fa-angle-right'></i><?php echo e(trans("Daycare List")); ?> </a>
								</li>
								<?php /* <li @if($segment4=='add-new-user') class="active" @endif>
									<a href="{{ route('Users.add')}}"><i class='fa fa-angle-right'></i>{{ trans("Add User") }} </a>
								</li> */ ?>
							</ul>
						</li>

						<li class="<?php echo e(($segment3 == 'plans') ? 'active' : ''); ?> "><a href="<?php echo e(URL::to('adminpnlx/plans')); ?>"><i class="fa fa-archive  <?php echo e(($segment3 == 'plans') ? '' : ''); ?>"></i><?php echo e(trans("Plan Management")); ?> </a></li>

						<li class="<?php echo e(($segment3 == 'members') ? 'active' : ''); ?> "><a href="<?php echo e(URL::to('adminpnlx/members')); ?>"><i class="fa fa-user-secret  <?php echo e(($segment3 == 'members') ? '' : ''); ?>"></i><?php echo e(trans("Membership Management")); ?> </a></li>

						<li class="treeview <?php echo e(in_array($segment3 ,array('readymadeproducts','colors','sizes','design','use','brand')) ? 'active in' : 'offer-reports'); ?>">
							<a href="javascript::void(0)"><i class="fa fa-files-o  <?php echo e(in_array($segment3 ,array('readymadeproducts','colors','design','use','brand')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("ReadyMade Products")); ?> </a>
							<ul class="treeview-menu <?php echo e(in_array($segment3 ,array('readymadeproducts','colors','sizes','design','use','brand')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('readymadeproducts','colors','sizes','design','use','brand')) ? 'display:block;' : 'display:none;'); ?>">

								<li <?php if($segment4!='add-new-readymadeproduct' && ($segment3=='readymadeproducts' || $segment4=='view-readymadeproduct' || $segment4=='edit-readymadeproduct')): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(route('ReadyMadeProducts.index')); ?>"><i class='fa fa-angle-right'></i><?php echo e(trans("Products")); ?> </a>
								</li>
								<!-- <li <?php if($segment4=='add-new-readymadeproduct'): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(route('ReadyMadeProducts.add')); ?>"><i class='fa fa-angle-right'></i><?php echo e(trans("Add Ready Made Product")); ?> </a>
								</li> -->
								<li <?php if(($segment3=='colors' || $segment4=='view-color' || $segment4=='edit-color'|| $segment4=='add-new-color')): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(route('Colors.index')); ?>"><i class='fa fa-angle-right'></i><?php echo e(trans("Colors")); ?> </a>
								</li>

								<li <?php if(($segment3=='sizes' || $segment4=='view-size' || $segment4=='edit-size'|| $segment4=='add-new-size')): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(route('Sizes.index')); ?>"><i class='fa fa-angle-right'></i><?php echo e(trans("Sizes")); ?> </a>
								</li>
								<li <?php if(($segment3=='brand' || $segment4=='view-brand' || $segment4=='edit-brand'|| $segment4=='add-new-brand')): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(route('Brand.index')); ?>"><i class='fa fa-angle-right'></i><?php echo e(trans("Brands")); ?> </a>
								</li>
							</ul>
						</li>
						
						<li class="treeview <?php echo e(in_array($segment3 ,array('order')) ? 'active in' : 'offer-reports'); ?>">
							<a href="javascript::void(0)"><i class="fa fa-users  <?php echo e(in_array($segment3 ,array('order')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Order Management")); ?> </a>
							<ul class="treeview-menu <?php echo e(in_array($segment3 ,array('order')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('order')) ? 'display:block;' : 'display:none;'); ?>">
								
								<li <?php if($segment3=='order' || $segment4=='view-order'): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(route('Order.index')); ?>"><i class='fa fa-angle-right'></i><?php echo e(trans("Orders")); ?> </a>
								</li>
								<?php /* <li @if($segment4=='add-new-user') class="active" @endif>
									<a href="{{ route('Users.add')}}"><i class='fa fa-angle-right'></i>{{ trans("Add User") }} </a>
								</li> */ ?>
							</ul>
						</li>
						
						<li <?php if($segment3=='staff'|| $segment4=='view-staff' || $segment4=='edit-staff'|| $segment4=='add-new-staff'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('Staff.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Staff Management")); ?> </a>
						</li>

						<li <?php if($segment3=='categories'|| $segment4=='view-category' || $segment4=='edit-category'|| $segment4=='add-new-category'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('Categories.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Categories")); ?> </a>
						</li>
						
						<li <?php if($segment3=='grades'|| $segment4=='view-grade' || $segment4=='edit-grade'|| $segment4=='add-new-grade'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('Grades.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Grades")); ?> </a>
						</li>
						
						<li <?php if($segment3=='content-management'|| $segment4=='view-content-management' || $segment4=='edit-content-management'|| $segment4=='add-new-content-management'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('ContentManagement.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Content Management")); ?> </a>
						</li>
						
						<li <?php if($segment3=='relationship'|| $segment4=='view-relationship' || $segment4=='edit-relationship'|| $segment4=='add-new-relationship'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('Relationship.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Relationship Management")); ?> </a>
						</li>
						
						<li <?php if($segment3=='banner'|| $segment4=='view-banner' || $segment4=='edit-banner'|| $segment4=='add-new-banner'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('Banner.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Shopping Banner Management")); ?> </a>
						</li>
						
						<li <?php if($segment3=='daycare-management'|| $segment4=='view-daycare-management' || $segment4=='edit-daycare-management'|| $segment4=='add-new-daycare-management'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('DaycareManagement.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Daycare & Institute Management")); ?> </a>
						</li>
						
						<li <?php if($segment3=='offer-management'|| $segment4=='view-offer-management' || $segment4=='edit-offer-management'|| $segment4=='add-new-offer-management'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('OfferManagement.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Deal/Offer Management")); ?> </a>
						</li>
						
						<!--<li <?php if($segment3=='payments'): ?>) class="active" <?php endif; ?>>
							<a href="<?php echo e(route('Payments.index')); ?>"><i class='fa fa-list-alt'></i><?php echo e(trans("Payments & Invoices Management")); ?> </a>
						</li>-->

						<li class="<?php echo e(($segment3 == 'advertise') ? 'active' : ''); ?> "><a href="<?php echo e(URL::to('adminpnlx/advertise')); ?>"><i class="fa fa-industry  <?php echo e(($segment3 == 'advertise') ? '' : ''); ?>"></i><?php echo e(trans("Ad Management")); ?> </a></li>

						<li class="<?php echo e(($segment3 == 'testimonials') ? 'active' : ''); ?> "><a href="<?php echo e(URL::to('adminpnlx/testimonials')); ?>"><i class="fa fa-users  <?php echo e(($segment3 == 'testimonials') ? '' : ''); ?>"></i><?php echo e(trans("Testimonials")); ?> </a></li>
						
						<li class="treeview <?php echo e(in_array($segment3 ,array('blocks','banners','system-documents','cms-manager','email-manager','email-logs','news-letter','no-cms-manager','faqs-manager')) ? 'active in' : 'offer-reports'); ?>">
							<a href="javascript::void(0)"><i class="fa fa-desktop  <?php echo e(in_array($segment3 
							,array('blocks','banners','system-documents','cms-manager','email-manager','email-logs','news-letter','no-cms-manager','faqs-manager')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("System Management")); ?> </a>
							<ul class="treeview-menu <?php echo e(in_array($segment3 
							,array('blocks','banners','system-documents','cms-manager','email-manager','email-logs','news-letter','no-cms-manager','faqs-manager')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('blocks','banners','system-documents','cms-manager','email-manager','email-logs','news-letter','no-cms-manager','faqs-manager')) ? 'display:block;' : 'display:none;'); ?>">
								<li <?php if($segment3 =='cms-manager'): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(URL::to('adminpnlx/cms-manager')); ?>"><i class=''></i><?php echo e(trans("Cms Pages")); ?> </a>
								</li>
								<?php /* 
								<li  @if($segment3 =='no-cms-manager') class="active" @endif>
									<a href="{{URL::to('adminpnlx/no-cms-manager')}}">{{ trans("SEO Pages") }} </a>
								</li> */ ?>
								<li <?php if($segment3 =='email-manager'): ?> class="active" <?php endif; ?> ><a href="<?php echo e(URL::to('adminpnlx/email-manager')); ?>"><i class=''></i><?php echo e(trans("Email Templates")); ?> </a></li>

								<li <?php if($segment3=='email-logs'): ?> class="active" <?php endif; ?>><a href="<?php echo e(URL::to('adminpnlx/email-logs')); ?>"><i class=''></i><?php echo e(trans("Email Logs")); ?> </a></li>
								<!--<li  <?php if($segment3 =='news-letter'): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(URL::to('adminpnlx/news-letter/newsletter-templates')); ?>"><?php echo e(trans("Newsletters")); ?> </a>
								</li>-->
								<?php /* 
								<li @if($segment3=='faqs-manager') class="active" @endif><a href="{{URL::to('adminpnlx/faqs-manager')}}">{{ trans("FAQ") }} </a></li>
								
								<li @if($segment3=='banners') class="active" @endif><a href="{{URL::to('adminpnlx/banners')}}">{{ trans("Banners") }} </a></li> */ ?>
								
								<!--<li <?php if($segment3=='system-documents'): ?> class="active" <?php endif; ?>><a href="<?php echo e(URL::to('adminpnlx/system-documents')); ?>"><?php echo e(trans("System Documents")); ?> </a></li>-->
								<li <?php if($segment3=='blocks'): ?> class="active" <?php endif; ?>><a href="<?php echo e(URL::to('adminpnlx/blocks')); ?>"><?php echo e(trans("Blocks")); ?> </a></li>
							</ul>
						</li>
						<?php /*<li class="treeview {{ in_array($segment3 ,array('dropdown-manager')) ? 'active in' : 'faq' }}">
							<a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('dropdown-manager')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Masters") }}</a>
							<ul class="treeview-menu {{ in_array($segment3 ,array('dropdown-manager')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('dropdown-manager')) ? 'display:block;' : 'display:none;' }}">
								
								<!-- 								
								<li  @if($segment4 =='question-category' || $segment5 =='question-category') class="active" @endif>
									<a href="{{URL::to('trpanelx/dropdown-manager/question-category')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Question Category") }} </a>
								</li> -->
								
								<li  @if($segment3 =='dropdown-manager' && $segment4 =='blog-category') class="active" @endif>
								<a href="{{URL::to('adminpnlx/dropdown-manager/diabetes-type')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Diabetes Type") }} </a>
								</li>
								
								<li  @if($segment3 =='dropdown-manager' && $segment4 =='faq') class="active" @endif>
								<a href="{{URL::to('adminpnlx/dropdown-manager/faq')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Faq Category") }} </a>
								</li>
								<li  @if($segment3 =='dropdown-manager' && $segment4 =='faq') class="active" @endif>
								<a href="{{URL::to('adminpnlx/dropdown-manager/bug-category')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Bug Category") }} </a>
								</li>
								<li  @if($segment3 =='dropdown-manager' && $segment4 =='support-subject') class="active" @endif>
								<a href="{{URL::to('adminpnlx/dropdown-manager/support-subject')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Support Subject Category") }} </a>
								</li>
							</ul>
						</li> */ ?>
						<li class="treeview <?php echo e(in_array($segment3 ,array('settings')) ? 'active in' : 'offer-reports'); ?>">
							<a href="javascript::void(0)"><i class="fa fa-cogs  <?php echo e(in_array($segment3 ,array('settings')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Settings")); ?> </a>
							<ul class="treeview-menu <?php echo e(in_array($segment3 ,array('settings')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('settings')) ? 'display:block;' : 'display:none;'); ?>">
								
								<li  <?php if($segment3=='settings' && Request::segment(4)=='Site'): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(URL::to('adminpnlx/settings/prefix/Site')); ?>"><i class=''></i><?php echo e(trans("Site Setting")); ?> </a>
								</li>
								
								<li  <?php if($segment3=='settings' && Request::segment(4)=='Reading'): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(URL::to('adminpnlx/settings/prefix/Reading')); ?>"><i class=''></i><?php echo e(trans("Reading Setting")); ?> </a>
								</li>
								
								<li  <?php if($segment3=='settings' && Request::segment(4)=='Social'): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(URL::to('adminpnlx/settings/prefix/Social')); ?>"><i class=''></i><?php echo e(trans("Social Setting")); ?> </a>
								</li>
								<li  <?php if($segment3=='settings' && Request::segment(4)=='Contact'): ?> class="active" <?php endif; ?>>
									<a href="<?php echo e(URL::to('adminpnlx/settings/prefix/Contact')); ?>"><i class=''></i><?php echo e(trans("Contact Setting")); ?> </a>
								</li>
							</ul>
						</li>
						
						<?php /*<li class="treeview {{ in_array($segment3 ,array('language','language-settings')) ? 'active in' : 'offer-reports' }}">
							<a href="javascript::void(0)"><i class="fa fa-list  {{ in_array($segment3 ,array('language','language-settings')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Language Settings") }} </a>
							<ul class="treeview-menu {{ in_array($segment3 ,array('language','language-settings')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('language','language-settings')) ? 'display:block;' : 'display:none;' }}">
								
								<li @if($segment3 =='language-settings') class="active" @endif ><a href="{{URL::to('adminpnlx/language-settings')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Language Settings") }} </a></li>
							</ul>
						</li> 

						<li class="treeview {{ in_array($segment3 ,array('gallery')) ? 'active in' : 'offer-reports' }}">
							<a href="javascript::void(0)"><i class="fa fa-users  {{ in_array($segment3 ,array('gallery')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Gallery")  }} </a>
							<ul class="treeview-menu {{ in_array($segment3 ,array('gallery')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('images')) ? 'display:block;' : 'display:none;' }}">
								<li @if($segment4!='add-new-user' && ($segment3=='gallery')) class="active" @endif>
									<a href="{{ route('Gallery.index')}}"><i class='fa fa-angle-right'></i>{{ trans("Gallery") }} </a>
								</li>
							</ul>
						</li>

						<li class="treeview {{ in_array($segment3 ,array('language','blog')) ? 'active in' : 'offer-reports' }}">
							<a href="javascript::void(0)"><i class="fa fa-list  {{ in_array($segment3 ,array('language','blog')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Blog") }} </a>
							<ul class="treeview-menu {{ in_array($segment3 ,array('language','blog')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('language','blog')) ? 'display:block;' : 'display:none;' }}">
								
								<li @if($segment3 =='blog') class="active" @endif ><a href="{{URL::to('adminpnlx/blog')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Blog") }} </a></li>
							</ul>
						</li>

						<li class="treeview {{ in_array($segment3 ,array('language','team')) ? 'active in' : 'offer-reports' }}">
							<a href="javascript::void(0)"><i class="fa fa-list  {{ in_array($segment3 ,array('language','team')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Teams") }} </a>
							<ul class="treeview-menu {{ in_array($segment3 ,array('language','team')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('language','team')) ? 'display:block;' : 'display:none;' }}">
								
								<li @if($segment3 =='team') class="active" @endif ><a href="{{URL::to('adminpnlx/team')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Teams") }} </a></li>
							</ul>
						</li>
						<li class="{{ ($segment3 == 'stories') ? 'active' : '' }} "><a href="{{URL::to('adminpnlx/stories')}}"><i class="fa fa-odnoklassniki  {{ ($segment3 == 'stories') ? '' : '' }}"></i>{{ trans("Success Stories") }} </a></li>
						
						*/ ?>

					


					</ul>
				</section>
			</aside>
			  <!-- Main Container Start -->
				<aside class="right-side"> 
					<?php if(Session::has('error')): ?>
						<script type="text/javascript"> 
							$(document).ready(function(e){
								
								show_message("<?php echo e(Session::get('error')); ?>",'error');
							});
						</script>
					<?php endif; ?>
					
					<?php if(Session::has('success')): ?>
						<script type="text/javascript"> 
							$(document).ready(function(e){
								show_message("<?php echo e(Session::get('success')); ?>",'success');
							});
						</script>
					<?php endif; ?>

					<?php if(Session::has('flash_notice')): ?>
						<script type="text/javascript"> 
							$(document).ready(function(e){
								show_message("<?php echo e(Session::get('flash_notice')); ?>",'success');
							});
						</script>
					<?php endif; ?>
					
					<?php echo $__env->yieldContent('content'); ?>
				</aside>
		</div>
		<?php echo Config::get("Site.copyright_text"); ?>
	</body>
</html>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/bootbox.js"></script>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/core/mws.js"></script>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/core/themer.js"></script>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/bootstrap.js"></script>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/app.js"></script>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/fancybox/jquery.fancybox.js"></script>
<link href="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="<?php echo e(WEBSITE_CSS_URL); ?>admin/bootmodel.css" rel="stylesheet">
<script type="text/javascript">
	function show_message(message,message_type) {
		$().toastmessage('showToast', {	
			text: message,
			sticky: false,
			position: 'top-right',
			type: message_type,
		});
	}
			
	$(function(){
		$('.fancybox').fancybox();
		$('.fancybox-buttons').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',
		});
		
		$(document).on('click', '.delete_any_item', function(e){ 
			e.stopImmediatePropagation();
			url = $(this).attr('href');
			bootbox.confirm("Are you sure you want to delete this ?",
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
			e.preventDefault();
		});
		
		/**
		 * Function to change status
		 *
		 * @param  null
		 *
		 * @return  void
		 */
		$(document).on('click', '.status_any_item', function(e){ 
			e.stopImmediatePropagation();
			url = $(this).attr('href');
			bootbox.confirm("Are you sure you want to change status ?",
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
			e.preventDefault();
		});
		
		$('.open').parent().addClass('active');
		$('.fancybox').fancybox();
		$('.fancybox-buttons').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',
		});
			

		$('.skin-black .sidebar > .sidebar-menu > li > a').click(function(e) {
			if(!($(this).next().hasClass("open"))) { 
				$(".treeview-menu").addClass("closed");
				$(".treeview-menu").removeClass("open");
				$(".treeview-menu.open").slideUp();
				$('.skin-black .sidebar > .sidebar-menu > li').removeClass("active");
			  
				$(this).next().slideDown();
				$(this).next().addClass("open");  
				$(this).parent().addClass("active"); 
				 
			}else {  
				e.stopPropagation(); 
				return false;  
			}
		}); 
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});

		$('.sidebar').slimscroll({
			height: '100%',
			size: '5px',
			color: '#ec7e07',
			alwaysVisible: false,
			distance: '0px',
      		opacity: 0.5
		});

		$('.data-equal').equalHeights();
	});

	function isNumberKey(evt, element) { 
		var charCode = (evt.which) ? evt.which : window.event.keyCode;
		if(charCode == 8){
			return true;
		}else if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
			return false;
		else {
			var len = $(element).val().length;
			var index = $(element).val().indexOf('.');
			if (index > 0 && charCode == 46) {
				return false;
			}
			if (index > 0) {
				var CharAfterdot = (len + 1) - index;
				if (CharAfterdot > 5) {
					return false;
				}
			}
		}
		return true;
	}
</script>
<?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/layouts/default.blade.php ENDPATH**/ ?>