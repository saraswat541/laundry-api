<?php $__env->startSection('content'); ?>
<?php
	$userInfo	=	Auth::user();
	$full_name	=	(isset($userInfo->full_name)) ? $userInfo->full_name : '';
	$email		=	(isset($userInfo->email)) ? $userInfo->email : '';
?>
<section class="content-header">
	<h1 class="text-center">My Account
		<span class="pull-right">
			<a href="<?php echo e(URL::to('adminpnlx/change-password')); ?>" class="btn btn-danger"><i class=\"icon-refresh\"></i>Change Password</a>
		</span>
	</h1>
	<div class="clearfix"></div>
</section>
<section class="content">
    <div class="box">
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
				<?php echo e(Form::open(['role' => 'form','url' => 'adminpnlx/myaccount','class' => 'mws-form','files'=>'true'])); ?>

					<!-- <div class="form-group <?php echo (!empty($errors->first('first_name'))?"has-error":''); ?>">
						<?php echo HTML::decode( Form::label('full_name', trans("Full Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('full_name', $full_name, ['class' => 'form-control'])); ?>  
							<div class="error-message help-inline">
								<?php echo $errors->first('full_name'); ?>
							</div>
						</div>
					</div> -->
					<div class="form-group <?php echo (!empty($errors->first('email'))?"has-error":''); ?>">
						<?php echo HTML::decode( Form::label('email', trans("Email").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('email', $email, ['class' => 'form-control'])); ?>  
							<div class="error-message help-inline">
								<?php echo $errors->first('email'); ?>
							</div>
						</div>
					</div>
					<div class="mws-button-row">
						<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
						<a href="<?php echo e(URL::to('adminpnlx/myaccount')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans('Reset')); ?></a>
					</div>
				<?php echo e(Form::close()); ?>

				</div>
			</div> 
	    </div>
	</div> 
</section>
<style>
#MyaccountAddress {
	resize: vertical; /* user can resize vertically, but width is fixed */
}
.error-message{
	color: #f56954 !important;
}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/dashboard/myaccount.blade.php ENDPATH**/ ?>