
<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
		View <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">View <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Name</th>
								<td data-th='Name' class="txtFntSze"><?php echo e(isset($model->name) ? $model->name :''); ?></td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Price</th>
								<td data-th='Category Name' class="txtFntSze"><?php echo e($model->price); ?></td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">No. of Month</th>
								<td data-th='Category Name' class="txtFntSze"><?php echo e($model->no_of_month); ?></td>
							</tr> 
							<tr>
								<th  width="30%" class="text-right txtFntSze">Description</th>
								<td data-th='Category Name' class="txtFntSze"><?php echo $model->description; ?></td>
							</tr>  
							<?php if(!empty($model->user_image)): ?>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Image</th>
								<td data-th='Category Name' class="txtFntSze"><img src="<?php echo e(url('public/team/'.$model->user_image)); ?>" alt="" width="100" height="50"></td>
							</tr>
							<?php endif; ?>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Registered On</th>
								<td data-th='Category Name' class="txtFntSze"><?php echo e(date(config::get("Reading.date_format"),strtotime($model->created_at))); ?></td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Status</th>
								<td data-th='Status' class="txtFntSze">
									<?php if($model->is_active	==1): ?>
									<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
									<?php else: ?>
									<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
									<?php endif; ?> 
								</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\daycare\resources\views/admin/Plan/view.blade.php ENDPATH**/ ?>