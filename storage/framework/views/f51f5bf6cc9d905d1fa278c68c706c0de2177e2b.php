

<?php $__env->startSection('content'); ?>

<style>
.bg-olive {
    background-color: #ff8400 !important;
}
.btn {
    padding: 12px 9px;
    margin-bottom: 15px !important;
}
.form-box {
  margin:0 !important;
}
.form-box {
  width: 420px;
  height: 400px;
}
.form-box .body input.form-control:focus {
  border-bottom: 1px solid rgba(255, 132, 0, 0.4196078431372549) !important;
}
.box-body01 {
  max-width: 300px;
  right: 0;
}
</style>

<div class="loginpat1"></div>
<div class="loginpat2"></div>
<div class="loginpat3"></div>

<div class="form-box" id="login-box">
  <div class="logo-block">
  <img src="<?php echo e(WEBSITE_IMG_URL); ?>logo-small.png" alt="">
  </div>
	<div class="header">Login</div>
	<?php echo e(Form::open(['role' => 'form','url' => 'adminpnlx/login'])); ?>    
	<div class="body pt-0">
		<div class="form-group relative">
      <i class="icon ion-ios-email-outline"></i>
			<?php echo e(Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control'])); ?>

			<div class="error-message help-inline">
				<?php echo $errors->first('email'); ?>
			</div>
		</div>
		<div class="form-group relative">
      <i class="icon ion-ios-locked-outline"></i>
		   <?php echo e(Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control','autocomplete'=>false])); ?>

		   <div class="error-message help-inline">
				<?php echo $errors->first('password'); ?>
			</div>
		</div>
	</div>
	<div class="footer">                                                               
		<button type="submit" class="btn bg-olive btn-block">Login</button> 
		<a href="<?php echo e(URL::to('adminpnlx/forget_password')); ?>">Forgot your password?</a>
	</div>
	<?php echo e(Form::close()); ?>

</div>
<style>
	body {
  background: #e9e9e9;
  color: #666666;
  font-family: 'RobotoDraft', 'Roboto', sans-serif;
  font-size: 14px;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

/* Pen Title */
.pen-title {
  padding: 50px 0;
  text-align: center;
  letter-spacing: 2px;
}
.pen-title h1 {
  margin: 0 0 20px;
  font-size: 48px;
  font-weight: 300;
}
.pen-title span {
  font-size: 12px;
}
.pen-title span .fa {
  color: #33b5e5;
}
.pen-title span a {
  color: #33b5e5;
  font-weight: 600;
  text-decoration: none;
}

/* Form Module */
.form-module {
  position: relative;
  background: #ffffff;
  max-width: 320px;
  width: 100%;
  border-top: 5px solid #33b5e5;
  -webkit-box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);
          box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);
  margin: 0 auto;
}
.form-module .toggle {
  cursor: pointer;
  position: absolute;
  top: -0;
  right: -0;
  background: #33b5e5;
  width: 30px;
  height: 30px;
  margin: -5px 0 0;
  color: #ffffff;
  font-size: 12px;
  line-height: 30px;
  text-align: center;
}
.form-module .toggle .tooltip {
  position: absolute;
  top: 5px;
  right: -65px;
  display: block;
  background: rgba(0, 0, 0, 0.6);
  width: auto;
  padding: 5px;
  font-size: 10px;
  line-height: 1;
  text-transform: uppercase;
}
.form-module .toggle .tooltip:before {
  content: '';
  position: absolute;
  top: 5px;
  left: -5px;
  display: block;
  border-top: 5px solid transparent;
  border-bottom: 5px solid transparent;
  border-right: 5px solid rgba(0, 0, 0, 0.6);
}
.form-module .form {
  display: none;
  padding: 40px;
}
.form-module .form:nth-child(2) {
  display: block;
}
.form-module h2 {
  margin: 0 0 20px;
  color: #33b5e5;
  font-size: 18px;
  font-weight: 400;
  line-height: 1;
}
.form-module input {
  outline: none;
  display: block;
  width: 100%;
  border: 1px solid #d9d9d9;
  margin: 0 0 20px;
  padding: 10px 15px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  font-wieght: 400;
  -webkit-transition: 0.3s ease;
  transition: 0.3s ease;
}
.form-module input:focus {
  border: 1px solid #33b5e5;
  color: #333333;
}
.form-module button {
  cursor: pointer;
  background: #33b5e5;
  width: 100%;
  border: 0;
  padding: 10px 15px;
  color: #ffffff;
  -webkit-transition: 0.3s ease;
  transition: 0.3s ease;
}
.form-module button:hover {
  background: #178ab4;
}
.form-module .cta {
  background: #f2f2f2;
  width: 100%;
  padding: 15px 40px;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  color: #666666;
  font-size: 12px;
  text-align: center;
}
.form-module .cta a {
  color: #333333;
  text-decoration: none;
}

	</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.login_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\daycare\resources\views/admin/login/index.blade.php ENDPATH**/ ?>