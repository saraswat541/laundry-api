<?php $__env->startSection('content'); ?>
<!-- CKeditor start here-->
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/ckeditor/ckeditor.js"></script>
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Edit <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">Edit <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
	    <div class="box-body">
		    <div class="row">
				<?php echo e(Form::open(['role' => 'form','url' =>  route("$modelName.edit",$model->id),'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"])); ?>

				<div class="col-md-6">
					<?php if(count($languages) > 1): ?>
						<div  class="default_language_color">
							<?php echo e(Config::get('default_language.message')); ?>

						</div>
						<div class="wizard-nav wizard-nav-horizontal">
							<ul class="nav nav-tabs">
								<?php $i = 1 ; ?>
								<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li class=" <?php echo e(($i ==  $language_code )?'active':''); ?>">
										<a data-toggle="tab" href="#<?php echo e($i); ?>div">
											<?php echo e($value -> title); ?>

										</a>
									</li>
									<?php $i++; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					<?php endif; ?>
					<?php if(count($languages) > 1): ?>
						<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
							<hr class ="hrLine"/>
							<b><?php echo e(trans("These fields (above seperator line) are same in all languages")); ?></b>
						</div>
					<?php endif; ?>
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div id="<?php echo e($i); ?>div" class="tab-pane <?php echo e(($i ==  $language_code )?'active':''); ?> ">
								<div class="mws-form-inline">
									<div class="form-group <?php if($i == 1){ echo ($errors->first('name')?'has-error':'');} ?>">
										<?php if($i == 1): ?>
											<?php echo HTML::decode( Form::label($i.'.name',trans("Color Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

										<?php else: ?>
											<?php echo HTML::decode( Form::label($i.'.name',trans("Color Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])); ?>

										<?php endif; ?>
										<div class="mws-form-item">
											<?php echo e(Form::text("data[$i][name]",isset($multiLanguage[$i]['name'])?$multiLanguage[$i]['name']:'', ['class' => 'form-control'])); ?>

											<div class="error-message help-inline">
												<?php echo ($i ==  $language_code ) ? $errors->first('name') : ''; ?>
											</div>
										</div>
									</div>							
																		
																		
																	
								</div>
							</div> 
						<?php $i++ ; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<div class="mws-button-row">
							<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
							<a href='<?php echo e(route("$modelName.edit",$model->id)); ?>' class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> <?php echo e(trans('Clear')); ?></a>
							<a href="<?php echo e(route($modelName.'.index')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group <?php echo ($errors->first('color_code')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('color_code',trans("Color Code").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::color('color_code',$model->color_code, ['class' => 'form-control','id'=>'color_code'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('color_code'); ?>
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-3">
					<div class="form-group">
						<div style="margin-top: 30px" id="demo"></div>
					</div>
				</div>
				<?php echo e(Form::close()); ?> 
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	var y = document.getElementById("color_code").value;
	document.getElementById("demo").innerHTML = y;
	
	$('#color_code').on('change', function(){
		var x = document.getElementById("color_code").value;
		document.getElementById("demo").innerHTML = x;
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/Colors/edit.blade.php ENDPATH**/ ?>