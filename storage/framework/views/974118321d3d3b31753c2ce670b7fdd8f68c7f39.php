<?php $__env->startSection('content'); ?>
<!-- CKeditor start here-->
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/ckeditor/ckeditor.js"></script>

<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Add New <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">Add New <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			<?php echo e(Form::open(['role' => 'form','route' => "$modelName.add",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"])); ?>

			<div class="row">
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('name',trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('name','', ['class' => 'form-control'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('name'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<?php if(count($languages) > 1): ?>
					<div  class="default_language_color">
						<?php echo e(Config::get('default_language.message')); ?>

					</div>
					<div class="wizard-nav wizard-nav-horizontal">
						<ul class="nav nav-tabs">
							<?php $i = 1 ; ?>
							<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li class=" <?php echo e(($i ==  $language_code )?'active':''); ?>">
								<a data-toggle="tab" href="#<?php echo e($i); ?>div">
									<?php echo e($value -> institute_name); ?>

								</a>
							</li>
							<?php $i++; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
					<?php endif; ?>

					<?php if(count($languages) > 1): ?>
					<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
						<hr class ="hrLine"/>
						<b><?php echo e(trans("These fields (above seperator line) are same in all languages")); ?></b>
					</div>
					<?php endif; ?>
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div id="<?php echo e($i); ?>div" class="tab-pane <?php echo e(($i ==  $language_code )?'active':''); ?> ">
							<div class="mws-form-inline">
								<div class="form-group <?php if($i == 1){ echo ($errors->first('institute_name')?'has-error':'');} ?>">
									<?php if($i == 1): ?>
									<?php echo HTML::decode( Form::label($i.'.institute_name',trans("Institute Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<?php else: ?>
									<?php echo HTML::decode( Form::label($i.'.institute_name',trans("Institute Name").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])); ?>

									<?php endif; ?>
									<div class="mws-form-item">
										<?php echo e(Form::text("data[$i][institute_name]",'', ['class' => 'form-control'])); ?>

										<div class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('institute_name') : ''; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $i++ ; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>						
					</div>
				</div>
					
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('plan')) ? 'has-error' : ''; ?>">
						<?php echo e(Form::label('plan',trans("Plans"), ['class' => 'mws-form-label'])); ?><span class="requireRed"> * </span>
						<div class="mws-form-item">
							<?php echo e(Form::select('plan',$plans,'',['class' => 'form-control','placeholder'=>'Choose Plan'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('plan'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('institute_email')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('institute_email',trans("Institute Email").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('institute_email','',array('autocomplete'=>'off','class' => 'form-control'))); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('institute_email'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('products')) ? 'has-error' : ''; ?>">
						<?php echo e(Form::label('products',trans("Products"), ['class' => 'mws-form-label'])); ?><span class="requireRed"> * </span>
						<div class="mws-form-item">
							<?php echo e(Form::select('products[]',$products,'',['class' => 'form-control','id'=>'products','multiple' => 'multiple'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('products'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('add_image')) ? 'has-error' : ''; ?>">
						<?php echo HTML::decode( Form::label('add_image', trans("Images").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::file('add_image[]',array('multiple'=>true,'class'=>'form-control'))); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('add_image'); ?>
							</div>
						</div>
					</div>
				</div>
					
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('password')) ? 'has-error' : ''; ?>">
						<?php echo HTML::decode( Form::label('password', trans("Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::password('password',['class'=>'userPassword form-control', 'readonly'=>'readonly', 'onfocus'=>"this.removeAttribute('readonly')", 'autocomplete'=>'false'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('password'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('confirm_password')) ? 'has-error' : ''; ?>">
						<?php echo HTML::decode( Form::label('confirm_password', trans("Confirm Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::password('confirm_password',['class'=>'form-control'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('confirm_password'); ?>
							</div>
						</div>
					</div>
				</div>
					
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('institute_number')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('institute_number',trans("Institute Contact No.").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('institute_number','', ['class' => 'form-control'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('institute_number'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('institute_address')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('institute_address',trans("Institute Address").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('institute_address','', ['class' => 'form-control'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('institute_address'); ?>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(route($modelName.'.add')); ?>" class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> <?php echo e(trans('Clear')); ?></a>
				<a href="<?php echo e(route($modelName.'.index')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
			<?php echo e(Form::close()); ?>

		</div>
	</div>
</section>
<?php echo e(HTML::style('css/admin/select2/select2.min.css')); ?>

<?php echo e(HTML::script('js/admin/select2/select2.min.js')); ?>

<script>
$(document).ready(function() {
    $('#products').select2();
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/DaycareManagement/add.blade.php ENDPATH**/ ?>