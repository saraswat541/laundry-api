
<?php $__env->startSection('content'); ?> 
<?php
  $automation = [
    ''    => 'Select Gender',
    '1'    => 'Male',
    '2'     => 'Female',
    '3'         => 'Other',
  ];
?>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/ckeditor/ckeditor.js"></script>
<section class="content-header">
	<h1>
		Add New <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">Add New <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<?php echo e(Form::open(['role' => 'form','route' => "$modelName.add",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"])); ?>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('add_image')) ? 'has-error' : ''; ?>">
						<?php echo HTML::decode( Form::label('add_image', trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::file('add_image',['class'=>'form-control'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('add_image'); ?>
							</div>
						</div>
					</div>
				</div>		
			</div>
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(route($modelName.'.add')); ?>" class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> <?php echo e(trans('Clear')); ?></a>
				<a href="<?php echo e(route($modelName.'.index')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
			<?php echo e(Form::close()); ?> 
		</div>
	</div>  	
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/Advertise/add.blade.php ENDPATH**/ ?>