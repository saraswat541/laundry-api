<?php $__env->startSection('content'); ?>
<script>
	$(function(){
		/**
		 * For match height of div 
		 */
		 $('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		 var tooltips = $( "[title]" ).tooltip({
		 	position: {
		 		my: "right bottom+50",
		 		at: "right+5 top-5"
		 	}
		 });
		});
	</script>
	<section class="content-header">
		<h1>
			<?php echo e($sectionName); ?>

		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
			<li class="active"> <?php echo e($sectionName); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="box search-panel collapsed-box">
			<div class="box-header with-border outer-padding">
				<h3 class="box-title">Search here</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
				</div>
			</div>
			<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
				<?php echo e(Form::open(['method' => 'get','role' => 'form','route' => "$modelName.index",'class' => 'row mws-form'])); ?>

				<?php echo e(Form::hidden('display')); ?>

				<div class="col-md-2 col-sm-2">
					<div class="form-group ">  
						<?php echo e(Form::select('is_active',array(''=>trans('All'),1=>trans('Active'),0=>trans('Inactive')),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control'])); ?>

					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="form-group ">  
						<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => ' form-control','placeholder'=>'Product Name'])); ?>

					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="form-group ">  
						<?php echo e(Form::select(
							'brand_id',
							[null => 'Select Brand'] + $brand,
							((isset($searchVariable['brand_id'])) ? $searchVariable['brand_id'] : ''),
							['class' => 'form-control']
							)); ?>

					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="form-group ">  
						<?php echo e(Form::select(
							'category_id',
							[null => 'Select Category'] + $categories,
							((isset($searchVariable['category_id'])) ? $searchVariable['category_id'] : ''),
							['class' => 'form-control']
							)); ?>

					</div>
				</div>
				

				<div class="col-md-3 col-sm-3">
					<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
					<a href='<?php echo e(route("$modelName.index")); ?>'  class="btn btn-primary"> <i class="fa fa-refresh "></i> <?php echo e(trans('Clear Search')); ?></a>
				</div>
				<?php echo e(Form::close()); ?> 
			</div>
		</div>
		<div class="box">
			<div class="box-body">
				<div class="box-header with-border pd-custom">
					<h3 class="box-title"><?php echo e($sectionName); ?>'s List</h3>
					<div class="listing-btns">
						<a href='<?php echo e(route("$modelName.add")); ?>'  class="btn btn-success btn-small"> <?php echo e(trans("Add New ")); ?><?php echo e($sectionNameSingular); ?> </a>
					</div>
				</div>
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th width="10%">
								<?php echo e(link_to_route(
									"$modelName.index",
									trans("Product Name"),
									array(
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
									)); ?>

							</th> 
							<th width="8%">
								<?php echo e(link_to_route(
									"$modelName.index",
									trans("Brand"),
									array(
									'sortBy' => 'brand_id',
									'order' => ($sortBy == 'brand_id' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'brand_id' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'brand_id' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
									)); ?>

							</th>
							<th width="8%">
								<?php echo e(link_to_route(
									"$modelName.index",
									trans("Category"),
									array(
									'sortBy' => 'category_id',
									'order' => ($sortBy == 'category_id' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'category_id' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'category_id' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
									)); ?>

							</th> 
							<th width="8%">
								<?php echo e(link_to_route(
									"$modelName.index",
									trans("Available Colors"),
									array($query_string),
									array('class' => (($sortBy == 'available_colors' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'available_colors' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
									)); ?>

							</th>	
							<th width="8%">
								<?php echo e(link_to_route(
									"$modelName.index",
									trans("Available Sizes"),
									array($query_string),
									array('class' => (($sortBy == 'available_sizes' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'available_sizes' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
									)); ?>

							</th>
							<th width="8%" class="action-th"> 
								<?php echo e(link_to_route(
									"$modelName.index",
									trans("In Feature"),
									array(
									'sortBy' => 'is_featured',
									'order' => ($sortBy == 'is_featured' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'is_featured' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_featured' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
									)); ?>

							</th>
							<th width="11%" class="action-th"> 
								<?php echo e(link_to_route(
									"$modelName.index",
									trans("Status"),
									array(
									'sortBy' => 'is_active',
									'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
									)); ?>

							</th>
							<th width="15%"><?php echo e(trans("Action")); ?></th>
						</tr>
					</thead>
					<tbody id="powerwidgets">
						<?php if(!$results->isEmpty()): ?>
						<?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr class="items-inner">
							<td data-th='name'><?php echo e($result->name); ?></td>
							<td data-th='brand_name'><?php echo e($result->brand_name); ?></td>

							<td data-th='category_id'><?php echo e($result->categories_name); ?></td>
							
							<td data-th='available_colors'><?php echo e($result->comma_seperated_colors); ?></td>
							<td data-th='available_sizes'><?php echo e($result->comma_seperated_sizes); ?></td>
							<td data-th='title' class="action-td ">
								<input type="checkbox" <?php echo ($result->is_featured == 1) ? "checked='checked'" : "";?> class="is_featured" data-id="<?php echo e($result->id); ?>" />
							</td>
							<td data-th=''>
								<?php if($result->is_active	== 1): ?>
								<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
								<?php else: ?>
								<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
								<?php endif; ?>
							</td>								
							<td data-th='' class="action-td">
								<?php if($result->is_active == 1): ?>
								<a  title="Click To Deactivate" href='<?php echo e(route("$modelName.status",array($result->id,0))); ?>' class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
								</a>
								<?php else: ?>
								<a title="Click To Activate" href='<?php echo e(route("$modelName.status",array($result->id,1))); ?>' class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
								</a> 
								<?php endif; ?> 
								<a href='<?php echo e(route("$modelName.edit","$result->id")); ?>' class="btn btn-primary" title="Edit"> <span class="fa fa-pencil"></span></a>
								<a href='<?php echo e(route("$modelName.delete","$result->id")); ?>' data-delete="delete" class="delete_any_item btn btn-danger" title="Delete">
									<span class="fa fa-trash-o"></span>
								</a> 
								<!--<a href='<?php echo e(route("$modelName.view","$result->id")); ?>' class="btn btn-primary" title="View Product Details"> <span class="fa fa-eye"></span></a> -->

							</td>
						</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
						<?php else: ?>
						<tr>
							<td colspan="12" class="alignCenterClass"> <?php echo e(trans("Record not found.")); ?></td>
						</tr>
						<?php endif; ?> 
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">	
				<div class="col-md-3 col-sm-4 "></div>
				<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $results], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
			</div>
		</div>
	</section>
	<script>
		$(document).on('click','.is_featured',function(){		
			var id	=	$(this).attr("data-id");		
			if($(this).prop("checked") == true){
				data	=	1;
			}
			else if($(this).prop("checked") == false){
				data	=	0;
			}
			$.ajax({
				url: '<?php echo e(URL::route("ReadyMadeProducts.featuredata")); ?>'+"?id="+id+"&data="+data,
				type:'GET',
				success: function(r){
					
				}
			});
		});
		
	</script>
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/ReadyMadeProducts/index.blade.php ENDPATH**/ ?>