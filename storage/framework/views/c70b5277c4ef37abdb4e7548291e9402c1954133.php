<?php $__env->startSection('content'); ?>
<script>
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});
</script>
<section class="content-header">
	<h1>
	  <?php echo e($sectionName); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li class="active"> <?php echo e($sectionName); ?></li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => route("$modelName.index"),'class' => 'row mws-form'])); ?>

			<?php echo e(Form::hidden('display')); ?>

			
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => ' form-control','placeholder'=>'Name'])); ?>

				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href='<?php echo e(route("$modelName.index")); ?>'  class="btn btn-primary"> <i class="fa fa-refresh "></i> <?php echo e(trans('Clear Search')); ?></a>
			</div>
			<?php echo e(Form::close()); ?> 
		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title"><?php echo e('Testimonial'); ?>'s List</h3>
				<div class="listing-btns">
					 <a href='<?php echo e(route("$modelName.add")); ?>'  class="btn btn-success btn-small pull-right"> <?php echo e(trans("Add New ")); ?><?php echo e($sectionNameSingular); ?> </a> 
				</div>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="10%">
							<?php echo e(trans('Image')); ?>

						</th>
						<th width="10%">
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Name"),
									array(
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> 
						<th width="10%">
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Designaton"),
									array(
									'sortBy' => 'designation',
									'order' => ($sortBy == 'designation' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'designation' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'designation' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Description"),
									array(
									'sortBy' => 'description',
									'order' => ($sortBy == 'description' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'description' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'description' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						
						<th width="10%">
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="8%"><?php echo e(trans("Action")); ?></th>
					</tr>
				</thead>
					<tbody id="powerwidgets">
						<?php if(!$results->isEmpty()): ?>
							<?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

								<tr class="items-inner">
								
									<td data-th='image'>
										<?php if($result->image != ""): ?>	
											<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $result->image; ?>"><img height="50" width="50" src="<?php echo e($result->image); ?>" /></a>
										<?php endif; ?>
									</td>
									<td data-th='name'><?php echo e($result->name); ?></td>
									<td data-th='name'><?php echo e($result->designation); ?></td>

									<td data-th='<?php echo e(trans("messages.$modelName.description")); ?>'> 
									<span>
									<?php echo e(strip_tags(Str::limit($result->description, 150))); ?>

									<?php if((strlen($result->description))>150): ?>
										<a class="description_<?php echo e($result->id); ?>" href="javascript:void(0);"> Read More</a></span>
										<span style="display:none;"><?php echo e(strip_tags($result->description)); ?>

										<a class="descriptionhide_<?php echo e($result->id); ?>" href="javascript:void(0);">Hide</a></span>
									<?php endif; ?>
									</td>
									
									<td data-th='<?php echo e(trans("status")); ?>'>
									<?php if($result->is_active	== 1): ?>
										<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
									<?php else: ?>
										<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
									<?php endif; ?>
								</td>
									<td data-th='' class="action-td">
										<?php if($result->is_active == 1): ?>
											<a  title="Click To Deactivate" href='<?php echo e(route("$modelName.status",array($result->id,0))); ?>' class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
											</a>
										<?php else: ?>
											<a title="Click To Activate" href='<?php echo e(route("$modelName.status",array($result->id,1))); ?>' class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
											</a> 
										<?php endif; ?> 
										<a href='<?php echo e(route("$modelName.edit",[$result->id])); ?>' class="btn btn-primary" title="Edit"> <span class="fa fa-pencil"></span></a>
										<a href='<?php echo e(route("$modelName.delete","$result->id")); ?>' data-delete="delete" class="delete_any_item btn btn-danger" title="Delete">
											<span class="fa fa-trash-o"></span>
										</a> 
									</td>

								</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
						<?php else: ?>
							<tr>
								<td colspan="12" class="alignCenterClass"> <?php echo e(trans("Record not found.")); ?></td>
							</tr>
						<?php endif; ?> 
					</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $results], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section>
<script type="text/javascript">
 $(function(){
		$("[class^=descriptionhide_]").on("click",function(){
			var id	=	$(this).attr('class').replace('descriptionhide_','');
			//alert(id);
			$(".description_"+id).parent().show();
			$(".descriptionhide_"+id).parent().hide();
		});
		$("[class^=description_]").on("click",function(){
			var id	=	$(this).attr('class').replace('description_','');
			$(".description_"+id).parent().hide();
			$(".descriptionhide_"+id).parent().show();
		});
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/Testimonials/index.blade.php ENDPATH**/ ?>