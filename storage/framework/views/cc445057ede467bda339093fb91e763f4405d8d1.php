<?php $__env->startSection('content'); ?>

<style>
.bg-olive {
    background-color: #ff8400 !important;
}
.btn {
    padding: 12px 9px;
    margin-bottom: 15px !important;
}
.form-box {
  margin:0 !important;
}
.form-box {
  width: 420px;
  height: auto;
}
.form-box .body input.form-control:focus {
  border-bottom: 1px solid rgba(255, 132, 0, 0.4196078431372549) !important;
}
.box-body01 {
  max-width: 300px;
  right: 0;
}
.bg-olive {
    background-color: #ff8400 !important;
}
.btn {
    padding: 12px 9px;
    margin-bottom: 15px !important;
}
.login-page .icon {
	top:11px
}
</style>

<div class="loginpat1"></div>
<div class="loginpat2"></div>
<div class="loginpat3"></div>

<div class="form-box" id="login-box">
	<div class="logo-block">
    	<img src="<?php echo e(WEBSITE_IMG_URL); ?>logo-small.png" alt="">
  	</div>
	<div class="header">
		Reset Password
	</div>
	<div class="body">
		<div class="form-group relative">
			<?php echo e($msg); ?>

		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin.layouts.login_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/login/front_reset_password_msg.blade.php ENDPATH**/ ?>