<?php $__env->startSection('content'); ?>
<!-- CKeditor start here-->
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/ckeditor/ckeditor.js"></script>

<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		Add New <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">Add New <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			<?php echo e(Form::open(['role' => 'form','route' => "$modelName.add",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"])); ?>

			<div class="row">
				
				<div class="col-md-6">
					<?php if(count($languages) > 1): ?>
					<div  class="default_language_color">
						<?php echo e(Config::get('default_language.message')); ?>

					</div>
					<div class="wizard-nav wizard-nav-horizontal">
						<ul class="nav nav-tabs">
							<?php $i = 1 ; ?>
							<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li class=" <?php echo e(($i ==  $language_code )?'active':''); ?>">
								<a data-toggle="tab" href="#<?php echo e($i); ?>div">
									<?php echo e($value -> offer_title); ?>

								</a>
							</li>
							<?php $i++; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>
					</div>
					<?php endif; ?>

					<?php if(count($languages) > 1): ?>
					<div class="text-right mws-form-item" style="margin-right:20px; padding-top:10px; font-size: 12px;">
						<hr class ="hrLine"/>
						<b><?php echo e(trans("These fields (above seperator line) are same in all languages")); ?></b>
					</div>
					<?php endif; ?>
					<div class="mws-panel-body no-padding tab-content"> 
						<?php $i = 1 ; ?>
						<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div id="<?php echo e($i); ?>div" class="tab-pane <?php echo e(($i ==  $language_code )?'active':''); ?> ">
							<div class="mws-form-inline">
								<div class="form-group <?php if($i == 1){ echo ($errors->first('offer_title')?'has-error':'');} ?>">
									<?php if($i == 1): ?>
									<?php echo HTML::decode( Form::label($i.'.offer_title',trans("Offer Title").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

									<?php else: ?>
									<?php echo HTML::decode( Form::label($i.'.offer_title',trans("Offer Title").'<span class="requireRed">  </span>', ['class' => 'mws-form-label'])); ?>

									<?php endif; ?>
									<div class="mws-form-item">
										<?php echo e(Form::text("data[$i][offer_title]",'', ['class' => 'form-control'])); ?>

										<div class="error-message help-inline">
											<?php echo ($i ==  $language_code ) ? $errors->first('offer_title') : ''; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $i++ ; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>						
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('offer_percent')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('offer_percent',trans("Offer Percent").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::number('offer_percent','',array('class' => 'form-control small', 'min' => '0'))); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('offer_percent'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('offer_description')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('offer_description',trans("Offer Description").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::textarea('offer_description','', ['class' => 'form-control small'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('offer_description'); ?>
							</div>
						</div>
						<script type="text/javascript">
						/* For CKEDITOR */
							
							CKEDITOR.replace( <?php echo 'offer_description'; ?>,
							{
								height: 150,
								width: 485,
								filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
								filebrowserImageWindowWidth : '640',
								filebrowserImageWindowHeight : '480',
								enterMode : CKEDITOR.ENTER_BR
							});
								
						</script>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('promo_code')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('promo_code',trans("Promo Code").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('promo_code','',array('class' => 'form-control'))); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('promo_code'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('user_id')) ? 'has-error' : ''; ?>">
						<?php echo e(Form::label('daycare_id',trans("Daycare List"), ['class' => 'mws-form-label'])); ?><span class="requireRed"> * </span>
						<div class="mws-form-item">
							<?php echo e(Form::select('user_id',$users,'',['class' => 'form-control','placeholder'=>'Choose Name'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('user_id'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('offer_startdate')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('offer_startdate',trans("Offer Start Date").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('offer_startdate','',['class' => 'form-control', 'id' => 'start_date'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('offer_startdate'); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('offer_enddate')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('offer_enddate',trans("Offer End Date").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::text('offer_enddate','',['class' => 'form-control', 'id' => 'end_date'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('offer_enddate'); ?>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(route($modelName.'.add')); ?>" class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> <?php echo e(trans('Clear')); ?></a>
				<a href="<?php echo e(route($modelName.'.index')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
			<?php echo e(Form::close()); ?>

		</div>
	</div>
</section>
<?php echo e(HTML::style('css/admin/bootstrap-datetimepicker.css')); ?>

<?php echo e(HTML::script('js/admin/moment.js')); ?>

<?php echo e(HTML::script('js/admin/bootstrap-datetimepicker.js')); ?>

<script>
jQuery(document).ready(function(){
		$('#start_date').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		
		$('#end_date').datetimepicker({
			format: 'YYYY-MM-DD'
		});
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/OfferManagement/add.blade.php ENDPATH**/ ?>