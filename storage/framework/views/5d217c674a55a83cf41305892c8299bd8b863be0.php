<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
		View <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">View <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border">
				<h3 class="box-title">Basic Info</h3>				
			</div>	
			<div class="row">		
				<div class="col-md-12 col-sm-6">		
					<div id="info1"></div>						 						
					<table class="table table-striped table-responsive"> 
						<tbody>
							
							<tr>
								<th width="30%" class="text-right txtFntSze">Brand Name</th>
								<td data-th='Name' class="txtFntSze"><?php echo e(isset($model->name) ? $model->name :''); ?></td>
							</tr>
							<tr>
								<th  width="30%" class="text-right txtFntSze">Image</th>
								<td data-th='Image' class="txtFntSze">
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $model->image; ?>"><img src="<?php echo e($model->image); ?>" style="width: 50px"></a></td>
							</tr>
							<tr>
								<th width="30%" class="text-right txtFntSze">Status</th>
								<td data-th='Status' class="txtFntSze">
									<?php if($model->is_active	==1): ?>
										<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
									<?php else: ?>
										<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
									<?php endif; ?> 
								</td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/Brand/view.blade.php ENDPATH**/ ?>