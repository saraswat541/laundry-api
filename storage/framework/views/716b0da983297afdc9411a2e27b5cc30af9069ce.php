
<?php $__env->startSection('content'); ?> 

<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/ckeditor/ckeditor.js"></script>
<section class="content-header">
	<h1>
		Add New <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">Add New <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<?php echo e(Form::open(['role' => 'form','route' => "$modelName.add",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"])); ?>

			<div class="row">
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
						<div class="mws-form-row">
						<?php echo HTML::decode( Form::label('name', trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::text('name','', ['class' => 'form-control small'])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('name'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('price')?'has-error':''); ?>">
						<div class="mws-form-row">
						<?php echo HTML::decode( Form::label('price', trans("Price").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::number('price','', ['class' => 'form-control small'])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('price'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('month')?'has-error':''); ?>">
						<div class="mws-form-row">
						<?php echo HTML::decode( Form::label('month', trans("No. of Month").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::number('month','', ['class' => 'form-control small', 'min' => 1])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('month'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group <?php  echo ($errors->first('description')?'has-error':''); ?>">
						<?php echo HTML::decode( Form::label('description', trans("Description").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::textarea('description','', ['class' => 'form-control small'])); ?>

							<span class="error-message help-inline">
								<?php echo $errors->first('description'); ?>
							</span>
						</div>
						<script type="text/javascript">
						/* For CKEDITOR */
							
							CKEDITOR.replace( <?php echo 'description'; ?>,
							{
								height: 150,
								width: 485,
								filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
								filebrowserImageWindowWidth : '640',
								filebrowserImageWindowHeight : '480',
								enterMode : CKEDITOR.ENTER_BR
							});
								
						</script>
					</div>
				</div>
				
			</div>
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(route($modelName.'.add')); ?>" class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> <?php echo e(trans('Clear')); ?></a>
				<a href="<?php echo e(route($modelName.'.index')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
			<?php echo e(Form::close()); ?> 
		</div>
	</div>  	
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\daycare\resources\views/admin/Package/add.blade.php ENDPATH**/ ?>