<?php $__env->startSection('content'); ?>
<script type="text/javascript"> 
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});	
	var action_url = '<?php echo WEBSITE_URL; ?>admin/cms-manager/multiple-action';
</script>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Manage Cms Pages")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('adminpnlx/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Manage Cms Pages")); ?></li>
	</ol>
</section>

<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'adminpnlx/cms-manager','class' => 'row mws-form'])); ?>

			<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
				
					<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => 'form-control' , 'placeholder' => 'Page Name'])); ?>

				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href='<?php echo e(route("$modelName.index")); ?>'  class="btn btn-primary"> <i class="fa fa-refresh "></i> <?php echo e(trans('Clear Search')); ?></a>
			</div>
			<?php echo e(Form::close()); ?> 
		</div>
	</div>

	<div class="box">
		<div class="box-body ">
		    <div class="box-header with-border pd-custom">
			<h3 class="box-title"><?php echo e($sectionName); ?> List</h3>
		        <?php if(Config::get('app.debug')): ?>
					<div class="listing-btns">
						<a href="<?php echo e(URL::to('adminpnlx/cms-manager/add-cms')); ?>" class="btn btn-success btn-small pull-right"> <?php echo e(trans("Add New Cms")); ?> </a>
					</div>
				<?php endif; ?>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="30%">
							<?php echo e(link_to_route(
								"Cms.index",
								trans("Page Name"),
								array(
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="40%"><?php echo e(trans("Page Description")); ?></th>
						<?php /* <th>
							{{
								link_to_route(
								"Cms.index",
								trans("messages.system_management.status"),
								array(
									'sortBy' => 'is_active',
									'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th> */ ?>
						<th>
							<?php echo e(link_to_route(
								"Cms.index",
								trans("Modified"),
								array(
									'sortBy' => 'updated_at',
									'order' => ($sortBy == 'updated_at' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'updated_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'updated_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th><?php echo e(trans("Action")); ?></th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr class="items-inner">
						<td data-th='<?php echo e(trans("Page Name")); ?>'><?php echo e($record->name); ?></td>
						<td data-th='<?php echo e(trans("messages.system_management.page_description")); ?>'><?php echo strip_tags(Str::limit($record->body, 300)); ?></td>
						<?php /* <td data-th='{{ trans("messages.system_management.status") }}'>
						@if($record->is_active	== 1)
							<span class="label label-success" >{{ trans("messages.user_management.activated") }}</span>
						@else
							<span class="label label-warning" >{{ trans("messages.user_management.deactivated") }}</span>
						@endif
						</td> */ ?>
						<td data-th='<?php echo e(trans("messages.system_management.modified")); ?>'>
						<?php echo e(date(Config::get("Reading.date_format") , strtotime($record->updated_at))); ?>

						</td>
						<td data-th='<?php echo e(trans("messages.system_management.action")); ?>'>
							<a title="Edit" href="<?php echo e(URL::to('adminpnlx/cms-manager/edit-cms/'.$record->id)); ?>" class="btn btn-primary"><span class="fa fa-pencil"></span></a>
							<?php /* @if($record->is_active == 1)
								<a  title="Click To Deactivate" href="{{URL::to('adminpnlx/cms-manager/update-status/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
								</a>
							@else
								<a title="Click To Activate" href="{{URL::to('adminpnlx/cms-manager/update-status/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
								</a> 
							@endif  */ ?>
						</td>
					</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="6" ><?php echo e(trans("No Record Found")); ?></td>
						</tr>
					<?php endif; ?> 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/Cms/index.blade.php ENDPATH**/ ?>