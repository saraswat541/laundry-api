
<?php $__env->startSection('content'); ?>
<?php echo e(HTML::style('css/admin/bootstrap-datetimepicker.css')); ?>

<?php echo e(HTML::script('js/admin/moment.js')); ?>

<?php echo e(HTML::script('js/admin/bootstrap-datetimepicker.js')); ?> 
<script>
	jQuery(document).ready(function(){
		$('#start_from').datetimepicker({
			format: 'YYYY-MM-DD'
		}); 
		$('#start_to').datetimepicker({
			format: 'YYYY-MM-DD'
		}); 
		
	});
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});
</script>
<section class="content-header">
	<h1>
	  <?php echo e($sectionName); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"> <?php echo e($sectionName); ?></li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			<?php echo e(Form::open(['method' => 'get','role' => 'form','route' => "$modelName.index",'class' => 'row mws-form'])); ?>

			<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select('is_active',array(''=>trans('All'),1=>trans('Active'),0=>trans('Inactive')),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => ' form-control','placeholder'=>'Name'])); ?>

				</div>
			</div>
			 
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('date_from',((isset($searchVariable['date_from'])) ? $searchVariable['date_from'] : ''), ['class' => 'form-control','id'=>'start_from','placeholder'=>trans('Date From')])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('date_to',((isset($searchVariable['date_to'])) ? $searchVariable['date_to'] : ''), ['class' => 'form-control','id'=>'start_to','placeholder'=>trans('Date To')])); ?>

				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href='<?php echo e(route("$modelName.index")); ?>'  class="btn btn-primary"> <i class="fa fa-refresh "></i> <?php echo e(trans('Clear Search')); ?></a>
			</div>
			<?php echo e(Form::close()); ?> 
		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title"><?php echo e($sectionName); ?>'s List</h3>
				<div class="listing-btns">
					<a href='<?php echo e(route("$modelName.add")); ?>'  class="btn btn-success btn-small pull-right"> <?php echo e(trans("Add New ")); ?><?php echo e($sectionNameSingular); ?> </a>
				</div> 
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Name"),
									array(
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> 
						<th>
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Price"),
									array(
									'sortBy' => 'price',
									'order' => ($sortBy == 'price' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'price' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'price' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> 

						<th>
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("No. of Month"),
									array(
									'sortBy' => 'no_of_month',
									'order' => ($sortBy == 'no_of_month' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'no_of_month' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'no_of_month' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> 
						<th>
							<?php echo e(link_to_route(
									"$modelName.index",
									trans("Registered On"),
									array(
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
									),
									array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						
						<th class="action-th"> 
							<?php echo e(link_to_route(
								"$modelName.index",
								trans("Status"),
								array(
								'sortBy' => 'is_active',
								'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="15%"><?php echo e(trans("Action")); ?></th>
					</tr>
				</thead>
					<tbody id="powerwidgets">
						<?php if(!$results->isEmpty()): ?>
							<?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr class="items-inner">
									<td data-th='Name'><?php echo e($result->name); ?></td>
									<td data-th='Designation'><?php echo e($result->price); ?></td>
									<td data-th='No. of Month'>
									<?php if(!empty($result->no_of_month == 1)): ?>
										<?php echo e('3 Month'); ?>

									<?php elseif(!empty($result->no_of_month == 2)): ?>
									<?php echo e('6 Month'); ?>	
									<?php elseif(!empty($result->no_of_month == 3)): ?>
									<?php echo e('9 Month'); ?>

									<?php elseif(!empty($result->no_of_month == 4)): ?>
									<?php echo e('1 Year'); ?>

									<?php endif; ?>

									</td>
									<td data-th='REGISTERED ON'><?php echo e(date(config::get("Reading.date_format"),strtotime($result->created_at))); ?></td>
									<td  data-th=''>
										<?php if($result->is_active	== 1): ?>
											<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
										<?php else: ?>
											<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
										<?php endif; ?>
									</td>								
									<td data-th='' class="action-td">
											<?php if($result->is_active == 1): ?>
											<a  title="Click To Deactivate" href='<?php echo e(route("$modelName.status",array($result->id,0))); ?>' class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
											</a>
										<?php else: ?>
											<a title="Click To Activate" href='<?php echo e(route("$modelName.status",array($result->id,1))); ?>' class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
											</a> 
										<?php endif; ?> 
										<a href='<?php echo e(route("$modelName.edit","$result->id")); ?>' class="btn btn-primary" title="Edit"> <span class="fa fa-pencil"></span></a> 
										<a href='<?php echo e(route("$modelName.view","$result->id")); ?>' class="btn btn-info" title="View"> <span class="fa fa-eye"></span></a> 
										<a href='<?php echo e(route("$modelName.delete","$result->id")); ?>' data-delete="delete" class="delete_any_item btn btn-danger" title="Delete">
											<span class="fa fa-trash-o"></span>
										</a> 
									</td>
								</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
						<?php else: ?>
							<tr>
								<td colspan="8" class="alignCenterClass"> <?php echo e(trans("Record not found.")); ?></td>
							</tr>
						<?php endif; ?> 
					</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $results], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/lightbox.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\daycare\resources\views/admin/Package/index.blade.php ENDPATH**/ ?>