
<?php $__env->startSection('content'); ?>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/chartJs/Chart.js"></script> 
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box dashboard-main-box">
				<div class="box-header">
					<h3 class="box-title">STATISTICS</h3>
				</div>
				<div class="box-body pad">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-xl-2">
							<div class="bg-primary dashboard-card rounded overflow-hidden">
								<div class="pd-25 d-flex align-items-center">
									<i class="ion ion-android-people tx-60 lh-0 tx-white op-7"></i>
									<div class="mg-l-20">
										<p class="tx-15 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Total Users</p>
										<p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo e(number_format($totalUsers)); ?></p>
										<a class="small-box-footer" href="<?php echo e(route('Users.index')); ?>">
											More Info <i class="ion-arrow-right-c"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-xl-2">
							<div class="bg-danger dashboard-card rounded overflow-hidden">
								<div class="pd-25 d-flex align-items-center">
									<i class="ion ion-ios-book tx-60 lh-0 tx-white op-7"></i>
									<div class="mg-l-20">
										<p class="tx-15 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Total Active Users</p>
										<p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1"><?php echo e(number_format($totalActiveUsers)); ?></p>
										<a class="small-box-footer" href="<?php echo e(route('Users.index')); ?>?is_active=1">
											More Info <i class="ion-arrow-right-c"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 	
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border mg-b-0">
					<h3 class="box-title">USERS</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<div id="newBarChart"></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="box dashboard-main-box2">
				<div class="box-body">
					<div class="row no-gutters">
						<div class="col-sm-12 col-lg-12">
							<div class="card card-body rounded-0" >
								<h6 class="tx-inverse tx-14 mg-b-5">RECENT USERS</h6>
								<div class="table-responsive">
									<table class="table table-striped text-left data-equal">
										<tbody>
											<tr>
												<th class="txtFntSze">Name</th>
												<th class="txtFntSze">Email</th>
												<th class="txtFntSze">Phone Number</th>
												<th class="txtFntSze">Status</th>
											</tr>
											<?php if(!empty($lastThreeCustomers)): ?>
												<?php $__currentLoopData = $lastThreeCustomers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lastThreeCustomer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<tr>
														<td class="txtFntSze"><?php echo e($lastThreeCustomer['name']); ?></td>
														<td class="txtFntSze"><?php echo e($lastThreeCustomer['email']); ?></td>
														<td class="txtFntSze"><?php echo e($lastThreeCustomer['phone_number']); ?></td>
														<?php if($lastThreeCustomer['is_active'] == 1): ?>
														<td class="txtFntSze"><?php echo e('Active'); ?></td>
														<?php else: ?>
														<td class="txtFntSze"><?php echo e('Inactive'); ?></td>
														<?php endif; ?>
													</tr>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											<?php else: ?>
												<tr>
													<td colspan="4" style="text-align: center;">
														<div>
														<img src="<?php echo e(WEBSITE_IMG_URL.'no_image.png'); ?>" alt="" >
														</div>
														<?php echo e(trans("Record not found.")); ?>

													</td>
												</tr>
											<?php endif; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
</div>

<script>
	$('.peity-line').peity('line');
	$('.peity-donut').peity('donut');
	
	var options = {
      chart: {
        height: 350,
        type: 'area',
        stacked: true,
        events: {
          selection: function(chart, e) {
            console.log(new Date(e.xaxis.min) )
          }
        },

      },
      colors: ['#e83e8c'],
      dataLabels: {
          enabled: false
      },
      stroke: {
        curve: 'smooth'
      },

      series: [
        {
          name: 'Total',
          data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 20, {
            min: 10,
            max: 15
          })
        }
      ],
      fill: {
        type: 'gradient',
        gradient: {
          opacityFrom: 0.6,
          opacityTo: 0.8,
        }
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left'
      },
      xaxis: {
        type: 'datetime'
      },
    }

    var chart = new ApexCharts(
      document.querySelector("#earning_chart"),
      options
    );

    chart.render();

    function generateDayWiseTimeSeries(baseval, count, yrange) {
      var i = 0;
      var series = [];
      while (i < count) {
        var x = baseval;
        var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

        series.push([x, y]);
        baseval += 86400000;
        i++;
      }
      return series;
	}
	
	
	
	
	
	var data = [
		<?php
			if(!empty($allUsers)){
				foreach($allUsers as $allUserss){
					?>
					 [<?php echo $allUserss['month']?>, <?php echo$allUserss['users']; ?>],
					<?php
				}
			}
		?>
		];
	var options = {
      chart: {
        height: 350,
        type: 'bar',
        stacked: true,
        events: {
          selection: function(chart, e) {
            console.log(new Date(e.xaxis.min) )
          }
        },

      },
      colors: ['#e83e8c'],
      dataLabels: {
          enabled: false
      },
      stroke: {
        curve: 'smooth'
      },

      series: [
        {
          name: 'Total',
          data: data
        }
      ],
      fill: {
        type: 'gradient',
        gradient: {
          opacityFrom: 0.6,
          opacityTo: 0.8,
        }
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left'
      },
      xaxis: {
		  labels: {
			offsetX: 10,
			formatter: function (value, timestamp) {
				var now = new Date(timestamp);
				day 	= "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
				var montharray=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
				return day+" "+montharray[now.getMonth()];
			}, 
		  },
        type: 'datetime'
      },
	   yaxis: {
		   labels: {
			  /**
			  * Allows users to apply a custom formatter function to yaxis labels.
			  *
			  * @param  { String } value - The generated value of the y-axis tick
			  * @param  { index } index of the tick / currently executing iteration in yaxis labels array
			  */
			  formatter: function(val, index) {
				return Math.round(val);
			  }
			}
	   }
    }

    var chart = new ApexCharts(
      document.querySelector("#newBarChart"),
      options
    );

    chart.render();

	
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\daycare\resources\views/admin/dashboard/dashboard.blade.php ENDPATH**/ ?>