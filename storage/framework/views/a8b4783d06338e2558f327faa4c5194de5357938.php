
<div class="row images_inner images_inner_<?php echo e($counter); ?>" data-id="<?php echo e($counter); ?>">
	<hr></hr>
	<div class="col-md-6">
		<div class="image_price form-group <?php echo ($errors->first('image_data['.$counter.'][color]')?'has-error':''); ?>">
			<div class="mws-form-row">
				<?php echo HTML::decode( Form::label('image_data['.$counter.'][color]',trans("Color").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select('image_data['.$counter.'][color]',$color,'',['class' => 'form-control chosen-select','placeholder'=>'Select Color'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('image_data['.$counter.'][color]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="image_price form-group <?php echo ($errors->first('image_data['.$counter.'][other_image]')?'has-error':''); ?>">
			<div class="mws-form-row">
				<?php echo HTML::decode( Form::label('image_data['.$counter.'][other_image]', trans("Other Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::file('image_data['.$counter.'][other_image][]', ['class' => 'form-control','accept'=>"image/*",'multiple'=>"multiple"])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('image_data['.$counter.'][other_image]'); ?>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-6">
		<div class="image_price form-group <?php echo ($errors->first('image_data['.$counter.'][image]')?'has-error':''); ?>">
			<div class="mws-form-row">
				<?php echo HTML::decode( Form::label('image_data['.$counter.'][image]', trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::file('image_data['.$counter.'][image]', ['class' => 'form-control','accept'=>"image/*"])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('image_data['.$counter.'][image]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group <?php echo ($errors->first('image_data['.$counter.'][is_defaults]')?'has-error':''); ?>">
			<?php echo HTML::decode( Form::label('image_data['.$counter.'][is_defaults]',trans("Default").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

			<?php echo e(Form::checkbox('image_data['.$counter.'][is_defaults]','','', ['class' => 'is_defaults'])); ?>


		</div>
		<div class="form-group ">
			<button type="button" class="btn btn-danger add-row" onclick="remove_image(<?php echo e($counter); ?>);">Remove</button>
		</div>
	</div>
</div>
<?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/ReadyMadeProducts/addMoreImage.blade.php ENDPATH**/ ?>