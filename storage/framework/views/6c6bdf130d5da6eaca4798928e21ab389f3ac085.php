<?php $__env->startSection('content'); ?>
<script>
	$(function(){
		/**
		 * For match height of div 
		 */
		 $('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		 var tooltips = $( "[title]" ).tooltip({
		 	position: {
		 		my: "right bottom+50",
		 		at: "right+5 top-5"
		 	}
		 });
		});
</script>
<section class="content-header">
	<h1>
		<?php echo e($sectionName); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li class="active"> <?php echo e($sectionName); ?></li>
	</ol>
</section>
<section class="content">
	<div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
			<?php echo e(Form::open(['method' => 'get','role' => 'form','route' => "$modelName.index",'class' => 'row mws-form'])); ?>

			<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select('is_active',array(''=>trans('All'),1=>trans('Active'),0=>trans('Inactive')),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('order_number',((isset($searchVariable['order_number'])) ? $searchVariable['order_number'] : ''), ['class' => ' form-control','placeholder'=>'Order Number'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('first_name',((isset($searchVariable['first_name'])) ? $searchVariable['first_name'] : ''), ['class' => ' form-control','placeholder'=>'First Name'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('last_name',((isset($searchVariable['last_name'])) ? $searchVariable['last_name'] : ''), ['class' => ' form-control','placeholder'=>'Last Name'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('email',((isset($searchVariable['email'])) ? $searchVariable['email'] : ''), ['class' => ' form-control','placeholder'=>'Email'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('address',((isset($searchVariable['address'])) ? $searchVariable['address'] : ''), ['class' => ' form-control','placeholder'=>'Address'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('city',((isset($searchVariable['city'])) ? $searchVariable['city'] : ''), ['class' => ' form-control','placeholder'=>'City'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('state',((isset($searchVariable['state'])) ? $searchVariable['state'] : ''), ['class' => ' form-control','placeholder'=>'State'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('country',((isset($searchVariable['country'])) ? $searchVariable['country'] : ''), ['class' => ' form-control','placeholder'=>'Country'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('postal_code',((isset($searchVariable['postal_code'])) ? $searchVariable['postal_code'] : ''), ['class' => ' form-control','placeholder'=>'Postal Code'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">
					<?php echo e(Form::text('phone_number',((isset($searchVariable['phone_number'])) ? $searchVariable['phone_number'] : ''), ['class' => ' form-control','placeholder'=>'Phone Number'])); ?>

				</div>
			</div>
			
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href='<?php echo e(route("$modelName.index")); ?>' class="btn btn-primary"> <i class="fa fa-refresh "></i> <?php echo e(trans('Clear Search')); ?></a>
			</div>
			<?php echo e(Form::close()); ?>

		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="box-header with-border pd-custom">
				<h3 class="box-title"><?php echo e($sectionName); ?></h3>
				<div class="listing-btns">
					<span data-href='<?php echo e(route("$modelName.export-csv")); ?>' id="export" class="btn btn-success btn-small pull-right" onclick="exportTasks(event.target);"> <?php echo e(trans("Export ")); ?><?php echo e($sectionNameSingular); ?> </span>
				</div>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="15%">
							<?php echo e(link_to_route(
								"$modelName.index",
								trans("Order Number"),
								array(
								'sortBy' => 'order_number ',
								'order' => ($sortBy == 'order_number ' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'order_number ' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'order_number ' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="15%">
							<?php echo e(link_to_route(
								"$modelName.index",
								trans("Full Name"),
								array(
								'sortBy' => 'first_name',
								'order' => ($sortBy == 'first_name' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'first_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'first_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="15%">
							<?php echo e(link_to_route(
								"$modelName.index",
								trans("E-mail"),
								array(
								'sortBy' => 'email	',
								'order' => ($sortBy == 'email' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'email' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'email' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="15%">
							<?php echo e(link_to_route(
								"$modelName.index",
								trans("Item Count"),
								array(
								'sortBy' => 'item_count	',
								'order' => ($sortBy == 'item_count' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'item_count' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'item_count' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="15%">
							<?php echo e(link_to_route(
								"$modelName.index",
								trans("Total amount"),
								array(
								'sortBy' => 'grand_total	',
								'order' => ($sortBy == 'grand_total' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'grand_total' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'grand_total' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%" class="action-th">
							<?php echo e(link_to_route(
								"$modelName.index",
								trans("Status"),
								array(
								'sortBy' => 'status',
								'order' => ($sortBy == 'status' && $order == 'desc') ? 'asc' : 'desc',
								$query_string
								),
								array('class' => (($sortBy == 'status' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'status' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="15%"><?php echo e(trans("Action")); ?></th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					<?php if(!$results->isEmpty()): ?>
					<?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr class="items-inner">
						<td data-th='order_number'><?php echo e($result->order_number); ?></td>
						<td data-th='first_name'><?php echo e($result->first_name); ?> <?php echo e($result->last_name); ?></td>
						<td data-th='email'><?php echo e($result->email); ?></td>
						<td data-th='item_count'><?php echo e($result->item_count); ?></td>
						<td data-th='grand_total'><?php echo e($result->grand_total); ?></td>
						<td  data-th=''>
							<?php if($result->status	== 'pending'): ?>
							<span class="label label-warning" ><?php echo e(trans("Pending")); ?></span>
							<?php endif; ?>
							<?php if($result->status	== 'processing'): ?>
							<span class="label label-warning" ><?php echo e(trans("Processing")); ?></span>
							<?php endif; ?>
							<?php if($result->status	== 'completed'): ?>
							<span class="label label-success" ><?php echo e(trans("Completed")); ?></span>
							<?php endif; ?>
							<?php if($result->status	== 'decline'): ?>
							<span class="label label-danger" ><?php echo e(trans("Decline")); ?></span>
							<?php endif; ?>
						</td>
						<td data-th='' class="action-td">
							<?php if($result->status == 'pending'): ?>
							<a  title="Click To Activate" href='<?php echo e(route("$modelName.status",array($result->id,0))); ?>' class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
							</a>
							<?php else: ?>
							<a title="Click To Pending" href='<?php echo e(route("$modelName.status",array($result->id,1))); ?>' class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
							</a> 
							<?php endif; ?> 
							
							<a href='<?php echo e(route("$modelName.delete","$result->id")); ?>' data-delete="delete" class="delete_any_item btn btn-danger" title="Delete">
								<span class="fa fa-trash-o"></span>
							</a> 
							<a href='<?php echo e(route("$modelName.view","$result->id")); ?>' class="btn btn-primary" title="View Order Details"> <span class="fa fa-eye"></span></a> 
						</td>
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
					<?php else: ?>
					<tr>
						<td colspan="12" class="alignCenterClass"> <?php echo e(trans("Record not found.")); ?></td>
					</tr>
					<?php endif; ?> 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $results], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section>
<script>
function exportTasks(_this) {
      let _url = $(_this).data('href');
      window.location.href = _url;
   }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/Order/index.blade.php ENDPATH**/ ?>