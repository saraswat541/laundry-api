<?php $__env->startSection('content'); ?>
<?php echo e(HTML::style('css/admin/bootstrap-datetimepicker.css')); ?>

<?php echo e(HTML::script('js/admin/moment.js')); ?>

<?php echo e(HTML::script('js/admin/bootstrap-datetimepicker.js')); ?> 
<script>
	jQuery(document).ready(function(){
		$('#start_from').datetimepicker({
			format: 'YYYY-MM-DD'
		}); 
		$('#start_to').datetimepicker({
			format: 'YYYY-MM-DD'
		}); 
		
	});
</script>
<!--pop js start here-->
<script type="text/javascript">
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});	
	/* For open Email detail popup */
	function getPopupClient(id){
		$.ajax({
			url: '<?php echo URL::to('adminpnlx/email-logs/email_details')?>/'+id,
			type: "POST",
			headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},

			success : function(r){
				$("#getting_basic_list_popover").html(r);
				$("#getting_basic_list_popover").modal('show');
			}
		});
	}
	
</script>
<!--pop js end here-->

<!--pop div start here-->
<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="getting_basic_list_popover" class="modal fade in" style="display: none;">
</div>
<!-- popup div end here-->
<section class="content-header">
	<h1>
	  <?php echo e(trans("Email Logs")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('adminpnlx/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"> <?php echo e(trans("Email Logs")); ?></li>
	</ol>
</section>
<section class="content"> 
    <div class="box search-panel collapsed-box">
		<div class="box-header with-border outer-padding">
			<h3 class="box-title">Search here</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:<?php echo !empty($searchVariable) ? 'block' : 'none'; ?>">
		   <?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'adminpnlx/email-logs','class' => 'row mws-form'])); ?>

			<?php echo e(Form::hidden('display')); ?>

			
				<div class="col-md-3 col-sm-3">
					<div class="form-group ">  
						<?php echo e(Form::text(
								'email_to', 
								 ((isset($searchVariable['email_to'])) ? $searchVariable['email_to'] : ''), 
								 ['class' =>'form-control','id'=>'country','placeholder'=>'Email'])); ?>

					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="form-group ">  
						<?php echo e(Form::text(
								'subject', 
								((isset($searchVariable['subject'])) ? $searchVariable['subject'] : ''), 
								 ['class' =>'form-control','id'=>'country','placeholder'=>'Subject'])); ?>

					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="form-group ">  
						<?php echo e(Form::text('date_from',((isset($searchVariable['date_from'])) ? $searchVariable['date_from'] : ''), ['class' => 'form-control','id'=>'start_from','placeholder'=>trans('Date From')])); ?>

					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="form-group ">  
						<?php echo e(Form::text('date_to',((isset($searchVariable['date_to'])) ? $searchVariable['date_to'] : ''), ['class' => 'form-control','id'=>'start_to','placeholder'=>trans('Date To')])); ?>

					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<button class="btn btn-primary"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
					<a href="<?php echo e(URL::to('adminpnlx/email-logs')); ?>"  class="btn btn-primary"><i class="fa fa-refresh "></i> <?php echo e(trans('Clear Search')); ?></a>
				</div>
			<?php echo e(Form::close()); ?>

		</div>
	</div>
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="20%">
							<?php echo e(trans('Email To')); ?>

						</th>
						<th width="20%">
							<?php echo e(trans('Email From')); ?>

						</th>
						<th width="20%">
							<?php echo e(trans('Subject')); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									'EmailLogs.listEmail',
									'Mail Sent On',
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?> 
						</th>
						<th width="10%">
							<?php echo e(trans('Action')); ?>

						</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td data-th="<?php echo e(trans('Email To')); ?>"><?php echo e($data->email_to); ?></td>
							<td data-th="<?php echo e(trans('Email From')); ?>"><?php echo e($data->email_from); ?></td>
							<td data-th="<?php echo e(trans('Subject')); ?>"><?php echo e($data->subject); ?></td>
							<td data-th="<?php echo e(trans('messages.system_management.created')); ?>"><?php echo e(date(Config::get("Reading.date_format"),strtotime($data->created_at))); ?></td>
							<td data-th="<?php echo e(trans('Action')); ?>">
								<a href='javascript:void(0);' class="btn btn-info" title='<?php echo e(trans("View")); ?>' onclick="getPopupClient(<?php echo e($data->id); ?>)"> <i class="fa fa-eye" ></i> </a>
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
						<tr>
							<td colspan="5" style="text-align:center;font-weight:bold;">
								<?php echo e(trans("Record not found.")); ?>

							</td>
						</tr>
					<?php endif; ?> 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section> 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/admin/emaillogs/index.blade.php ENDPATH**/ ?>