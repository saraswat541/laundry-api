<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo e(Config::get('Site.title')); ?></title>
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tbody>
       <tr>
          <td align="center">
             <table cellspacing="0" cellpadding="0" border="0" style="width: 680px; border-collapse: collapse;">
                <tbody>
                   <tr>
                      <td style="padding-left: 15px; padding-right: 15px; padding-top: 15px; padding-bottom: 15px;background:#f5f5f5">
                         <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                            <tbody>
                               <tr>
                                  <td>
                                     <a href="javascript:void(0)">
                                     <img src="<?php echo e(url('img/logo.png')); ?>" style="width: 122px;">
                                     </a>
                                  </td>
                                  <!-- <td style="text-align: right; font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; vertical-align: bottom;">
                                     Owned &amp; Operated By <br> Australia’s Premier Cruise Operator
                                  </td> -->
                               </tr>
                            </tbody>
                         </table>
                      </td>
                   </tr>
                   
                   
                   <tr>
                     <?php if(!empty($user)): ?>
                    <td style="padding-bottom:15px;padding-top:15px;font-family: Arial, Helvetica, sans-serif; font-size: 24px; color: #000;font-weight: bold;">
                        Hello <?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?>

                    </td>
                    <?php endif; ?>
                   </tr>
                   <tr>
                    
                    <td style="padding-bottom:30px;font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #000;">
                        Thank your for purchase. Below is your order information
                    </td>
                   </tr>
                   <tr>
                    <td style=" padding-bottom: 15px; background: #fff;">
                       <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                          <tbody>
                            

                             <tr>
                               <?php if(!empty($user)): ?>
                                <td style="padding-right: 13px; vertical-align: top; width: 50%;">
                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;border: solid 1px #162061;">
                                        <tbody>
                                            <tr>
                                                <td style="background:#162061;font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #ffffff; padding: 15px 20px;">
                                                    Order Information
                                                 </td>
                                            </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px; padding-top: 15px; padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Order Number
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                             <?php echo e($user->order_number); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Total Amount
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;font-weight: bold;">
                                                          <?php echo e($user->grand_total); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <!-- <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Shipping Charges
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                             #50
                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr> -->
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                            Order Date
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          <?php echo e(date(Config::get("Reading.date_format") , strtotime($user->created_at))); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                            Status
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; ">
                                                          <?php echo e($user->status); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                        </tbody>
                                    </table>

                                </td>
                                <?php endif; ?>

                                <?php if(!empty($user)): ?>
                                <td style="padding-left: 13px; vertical-align: top; width: 50%;">
                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;border: solid 1px #162061;">
                                        <tbody>
                                            <tr>
                                                <td style="background:#162061;font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #ffffff; padding: 15px 20px;">
                                                    Shipping Information
                                                 </td>
                                            </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px; padding-top: 15px; padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Name
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          <?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Email
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          <?php echo e($user->email); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                             Mobile Number
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          <?php echo e($user->phone_number); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                            Address
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; border-bottom: 1px solid #a5a5a5; padding-bottom: 10px;">
                                                          <?php echo e($user->address); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                           <tr>
                                            <td style=" width: 50%; padding-bottom: 15px;padding-left: 15px; padding-right: 15px;">
                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                                    <tbody>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #737373; padding-bottom: 2px;">
                                                            Post Code
                                                          </td>
                                                       </tr>
                                                       <tr>
                                                          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; ">
                                                          <?php echo e($user->postal_code); ?>

                                                          </td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                               </td>
                                           </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <?php endif; ?>
                             </tr>
                            
                             
                          </tbody>
                       </table>
                    </td>
                 </tr>
                 <tr>
                    <td style="padding-bottom:15px;padding-top:15px;font-family: Arial, Helvetica, sans-serif; font-size: 24px; color: #000;font-weight: bold;">
                        Order Details
                    </td>
                    
                   </tr>
                 <tr>
                    <td style="font-family: Arial, Helvetica, sans-serif;">
                        <table cellspacing="0" cellpadding="0"  style="width: 100%; border-collapse: collapse; border: solid 1px #a5a5a5;">
                            <tbody>
                                
                                <tr>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px; border: solid 1px #a5a5a5;">
                                        Image
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        Name
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        Size
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        color
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        Quantity
                                    </th>
                                    <th style="font-family: Arial, Helvetica, sans-serif;background: #162061;font-size: 16px;
                                    color: #ffffff; padding: 15px 20px;border: solid 1px #a5a5a5;">
                                        Price
                                    </th>
                                   </tr>
                                <?php $__currentLoopData = $order_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                               <tr>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                    <img src="<?php echo e(PRODUCT_IMAGE_URL.$order->product->image); ?>" alt="Prodcut Images" style="width: 100px;">
                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                    <?php echo e($order->product->name); ?>

                                </td>
                                
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                  <?php if($order->product_variation->size != null): ?> <?php echo e($order->product_variation->size->name); ?> <?php endif; ?>
                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                 <?php if($order->product_variation->color != null): ?> <?php echo e($order->product_variation->color->name); ?> <?php endif; ?>
                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                    <?php echo e($order->quantity); ?>

                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif;padding: 10px 15px;border: solid 1px #a5a5a5;">
                                    <?php echo e(Config::get('Site.currencyCode').number_format($order->price, 2, '.', ',')); ?>

                                </td>                                
                               </tr>
                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                               <tr>                                                                
                              
                               </tr>
                            </tbody>
                        </table>
                    </td>
                 </tr>
                </tbody>
             </table>
          </td>
       </tr>
    </tbody>
 </table>
</body>
</html><?php /**PATH /home/daycare/web/daycarepanel.stage02.obdemo.com/public_html/resources/views/emails/user_order_success.blade.php ENDPATH**/ ?>