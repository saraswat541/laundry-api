
<?php $__env->startSection('content'); ?> 
<?php
  $automation = [
    ''    => 'Select Gender',
    '1'    => 'Male',
    '2'     => 'Female',
    '3'         => 'Other',
  ];
?>
<script src="<?php echo e(WEBSITE_JS_URL); ?>admin/plugins/ckeditor/ckeditor.js"></script>
<section class="content-header">
	<h1>
		Add New <?php echo e($sectionNameSingular); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(route('dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route($modelName.'.index')); ?>"><?php echo e($sectionName); ?></a></li>
		<li class="active">Add New <?php echo e($sectionNameSingular); ?></li>
	</ol>
</section>

<section class="content"> 
	<div class="box">
		<div class="box-body">
			<?php echo e(Form::open(['role' => 'form','route' => "$modelName.add",'class' => 'mws-form', 'files' => true,"autocomplete"=>"off"])); ?>

			<div class="row">
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('plan')) ? 'has-error' : ''; ?>">
						<?php echo e(Form::label('plan',trans("Plans"), ['class' => 'mws-form-label'])); ?><span class="requireRed"> * </span>
						<div class="mws-form-item">
							<?php echo e(Form::select('plan',$plans,'',['class' => 'form-control','placeholder'=>'Choose Plan'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('plan'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('daycare')?'has-error':''); ?>">
						<div class="mws-form-row">
						<?php echo HTML::decode( Form::label('daycare', trans("Daycare Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::text('daycare','', ['class' => 'form-control small'])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('daycare'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">	
					<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
						<div class="mws-form-row">
						<?php echo HTML::decode( Form::label('name', trans("Name").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::text('name','', ['class' => 'form-control small'])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('name'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('email')?'has-error':''); ?>">
						<div class="mws-form-row">
						<?php echo HTML::decode( Form::label('email', trans("Email").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::text('email','', ['class' => 'form-control small'])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('email'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('phone_number')?'has-error':''); ?>">
						<div class="mws-form-row">
						<?php echo HTML::decode( Form::label('phone_number', trans("Phone Number").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

							<div class="mws-form-item">
								<?php echo e(Form::text('phone_number','', ['class' => 'form-control small'])); ?>

								<div class="error-message help-inline">
									<?php echo $errors->first('phone_number'); ?>
								</div>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('password')) ? 'has-error' : ''; ?>">
						<?php echo HTML::decode( Form::label('password', trans("Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::password('password',['class'=>'userPassword form-control', 'readonly'=>'readonly', 'onfocus'=>"this.removeAttribute('readonly')", 'autocomplete'=>'false'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('password'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('confirm_password')) ? 'has-error' : ''; ?>">
						<?php echo HTML::decode( Form::label('confirm_password', trans("Confirm Password").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::password('confirm_password',['class'=>'form-control'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('confirm_password'); ?>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group <?php echo ($errors->first('user_image')) ? 'has-error' : ''; ?>">
						<?php echo HTML::decode( Form::label('user_image', trans("Image").'<span class="requireRed"> * </span>', ['class' => 'mws-form-label'])); ?>

						<div class="mws-form-item">
							<?php echo e(Form::file('user_image',['class'=>'form-control'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('user_image'); ?>
							</div>
						</div>
					</div>
				</div>		
			</div>
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(route($modelName.'.add')); ?>" class="btn btn-primary reset_form"><i class=\"icon-refresh\"></i> <?php echo e(trans('Clear')); ?></a>
				<a href="<?php echo e(route($modelName.'.index')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
			<?php echo e(Form::close()); ?> 
		</div>
	</div>  	
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\daycare\resources\views/admin/Users/add.blade.php ENDPATH**/ ?>