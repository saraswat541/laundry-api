<?php
if(!function_exists('productPrice')) {
    function productPrice($price, $discounted_percent)
    {
        $discounted_price = $price;
        if($discounted_percent > 0){
            $discounted_price = $price - ($price * $discounted_percent/100);
        }
        if($discounted_percent > 0){
            echo '<span class="line">'.Config::get('Site.currencyCode').' '.number_format($price, 2, '.', ',').'</span>';
        }
        echo Config::get('Site.currencyCode').' '.number_format($discounted_price, 2, '.', ',');
        
    }
}

if(!function_exists('productDetailPrice')) {
    function productDetailPrice($price, $discounted_percent)
    {
        $discounted_price = $price;
        $discount = '';
        if($discounted_percent > 0){
            $discounted_price = $price - ($price * $discounted_percent/100);
            $discount = '<span class="off-tag">'.number_format($discounted_percent, 2, '.', ',').'% '.trans('messages.off').'</span>';
        }
        echo '<h2>'.Config::get('Site.currencyCode').' '.number_format($discounted_price, 2, '.', ',').$discount;
        if($discounted_percent > 0){
            echo '<span class="cut-price">'.Config::get('Site.currencyCode').' '.number_format($price, 2, '.', ',').'</span>';
        }        
        echo '</h2>';
        
    }
}

if(!function_exists('getLangugeId')) {
    function getLangugeId(){
        $lang		    =	App::getLocale();
        $language_id	=	DB::table("languages")
                            ->where("lang_code",$lang)
                            ->value("id");
        return $language_id;
    }
}

if(!function_exists('cart_total_price')) {
    function cart_total_price($data){
        $total_price = 0;
        foreach($data as $item){
            $price = $item->price;
            $discount = $item->discount;
            $quantity = $item->quantity;
            $discounted_price = ($price - ($price * $discount/100)) * $quantity;
            $total_price += $discounted_price;
        }        
        return $total_price;        
    }
}

if(!function_exists('item_base_price')){
    function item_base_price($price, $discount, $quantity=1){
        $discounted_price = ($price - ($price * $discount/100)) * $quantity;
        return $discounted_price;
    }
}

if(!function_exists('cart_total_quantity')){
    function cart_total_quantity($data){
        $total_quantity = 0;
        foreach($data as $item){           
            $quantity = $item->quantity;            
            $total_quantity += $quantity;
        }        
        return $total_quantity;
    }
}

function createQRCode($qr_sring = '') {
    $data						=	 $qr_sring;
    $size						=	"112";
    $encoding					=	"UTF-8";
    $errorCorrectionLevel		=	"L";
    $marginInRows				=	"0";
    $QRLink = "https://chart.googleapis.com/chart?cht=qr&chs=".$size."x".$size."&chl=" . $data .  "&choe=" . $encoding . "&chld=" . $errorCorrectionLevel . "|" . $marginInRows; 
    return $QRLink;
    die;
}

function UserWalletAddress($wallet_address = null) { 
    if(!empty($wallet_address)){
        $data						=	 $wallet_address;
        $size						=	"300";
        $encoding					=	"UTF-8";
        $errorCorrectionLevel		=	"L";
        $marginInRows				=	"0";
        $QRLink = "https://chart.googleapis.com/chart?cht=qr&chs=".$size."x".$size."&chl=" . $data .  "&choe=" . $encoding . "&chld=" . $errorCorrectionLevel . "|" . $marginInRows; 
        $response	=	array(
            'success'	 	=> 1,
            'qr_string' 	=> $QRLink,
            'wallet_address' 		=> $wallet_address
        );
        return $response; 
        die;
    }else{
        $response	=	array(
            'success'	 	=> 0,
        );
        return $response;
        die;
    }
}