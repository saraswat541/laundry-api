<?php
config::set("Contact.address", "333 Address Street,777 New York");
config::set("Contact.phone_number", "1-234-567-8910");
config::set("Contact.time", "Mon-Sat 8.00am - 6.00pm");
config::set("Reading.date_format", "m-d-Y");
config::set("Reading.date_time_format", "m-d-Y h:i A");
config::set("Reading.records_per_page", "10");
config::set("Site.from_email", "owebest01@gmail.com");
config::set("Site.title", "Laundry");
config::set("Social.facebook_link", "https://www.facebook.com/");
config::set("Social.googleplus_link", "https://www.google.com/");
config::set("Social.twitter_link", "https://twitter.com/");
config::set("Social.youtube_link", "https://www.youtube.com/");
