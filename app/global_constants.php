<?php
/* Global constants for site */
define('FFMPEG_CONVERT_COMMAND', '');

define("ADMIN_FOLDER", "admin/");
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', base_path());
define('APP_PATH', app_path());

define('FRONT_WEBSITE_URL', 'http://laundry.stage02.obdemo.com/');

define("IMAGE_CONVERT_COMMAND", "");
define('WEBSITE_URL', url('/').'/');
define('WEBSITE_JS_URL', WEBSITE_URL . 'js/');
define('WEBSITE_CSS_URL', WEBSITE_URL . 'css/');
define('WEBSITE_IMG_URL', WEBSITE_URL . 'img/');
define('WEBSITE_UPLOADS_ROOT_PATH', ROOT . DS . 'uploads' .DS );
define('WEBSITE_UPLOADS_URL', WEBSITE_URL . 'uploads/');

define('WEBSITE_ADMIN_URL', WEBSITE_URL.ADMIN_FOLDER );
define('WEBSITE_ADMIN_IMG_URL', WEBSITE_ADMIN_URL . 'img/');
define('WEBSITE_ADMIN_JS_URL', WEBSITE_ADMIN_URL . 'js/');
define('WEBSITE_ADMIN_FONT_URL', WEBSITE_ADMIN_URL . 'fonts/');
define('WEBSITE_ADMIN_CSS_URL', WEBSITE_ADMIN_URL . 'css/');

define('SETTING_FILE_PATH', APP_PATH . DS . 'settings.php');

define('CK_EDITOR_URL', WEBSITE_UPLOADS_URL . 'ck_editor_images/');
define('CK_EDITOR_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'ck_editor_images' . DS);

define('USER_IMAGE_URL', WEBSITE_UPLOADS_URL . 'users/');
define('USER_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'users' . DS);

/**  System document url path **/
if (!defined('SEO_PAGE_IAMGE_URL')) {
    define('SEO_PAGE_IAMGE_URL', WEBSITE_UPLOADS_URL . 'seo_pages/');
}

/**  System document upload directory path **/
if (!defined('SEO_PAGE_IAMGE_ROOT_PATH')){
    define('SEO_PAGE_IAMGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH . 'seo_pages' . DS);
}

//////////////// extension
define('IMAGE_EXTENSION','jpeg,jpg,png,gif,bmp');
define('IMAGE_EXTENSION_DOCUMENTS','jpeg,jpg,png,gif,bmp,pdf,docx,doc,xls,excel');
define('CAREER_FORM_DOCUMENTS','pdf,docx,doc');


$config	=	array();

define('ALLOWED_TAGS_XSS', '<a><strong><b><p><br><i><font><img><h1><h2><h3><h4><h5><h6><span><div><em><table><ul><li><section><thead><tbody><tr><td><figure><article>');

define('ADMIN_ID', 1);
define('SUPER_ADMIN_ROLE_ID', 1);
define('CUSTOMER_ROLE_ID', 2);
define('MERCHANT_ROLE',3);
define('DRIVER_ROLE',4);
define('CONTACT_ROLE',5);

Config::set('default_language.folder_code', 'eng');
Config::set('default_language.language_code', '1');
Config::set('default_language.name', 'English');
