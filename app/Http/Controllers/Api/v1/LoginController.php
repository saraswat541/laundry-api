<?php
namespace App\Http\Controllers\Api\v1;
use App\Model\User;
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\BaseController;
use Nexmo\Laravel\Facade\Nexmo;
use App,Auth,Config,Validator;
use App\Model\Contact;

/**
* Login Controller
*
* Add your methods in the class below
*
* This file will render views from views/api
*/
class LoginController extends BaseController {


	public function login(Request $request){
		$formData	=	$request->all();
		$response	=	array();

		if(!empty($formData)){
			$validator = Validator::make(
				$request->all(),
				array(
					"phone_number"          => 'required',
					'password'				=> 'required',
				),array(
					"phone_number.required"		=>	trans("The phone number is required."),
					"password.required"		    =>	trans("Password is required"),
				)
			);
			if ($validator->fails()){
				$response["status"]			=	"error";
				$response["message"]		=	$validator->errors();
				$response["data"]			=	null;
			}else{
				$number						=	$request->get('phone_number');
                $password					=	$request->get('password');
                $userDetails				= 	User::where('phone_number',$number)->first();
					if(!empty($userDetails)){
						if(Hash::check($password,$userDetails->password)){
							if($userDetails->is_active == 0){
								$response["status"]			=	"error";
								$response["message"]		=	trans('User is not active');
								$response["data"]			=	array();
							}else{
								if($userDetails->is_deleted == 1){
									$response["status"]			=	"error";
									$response["message"]		=	trans('PleaseSignup Again.');
									$response["data"]			=	array();
								}else{
										Auth::loginUsingId($userDetails->id);
										$user          			= 	 Auth::user();
										$response["status"]		=	"success";
										$response["message"]	=	trans('Login Successfully');
										$response["data"]		=	$userDetails;
								}
							}
						}else{
							$response["status"]			=	"error";
							$response["message"]		=	trans('Password not Match');
							$response["data"]			=	array();
						}
					}else{
						$response["status"]			=	"error";
						$response["message"]		=	trans('User not registered');
						$response["data"]			=	null;
					}


			}
		}else{
			$response["status"]		=	"error";
			$response["message"]	=	trans('Invalid Request.');
			$response["data"]		=	array();
		}
			return json_encode($response,JSON_PRETTY_PRINT);
	}

	public function ForgetPassword(Request $request){
		$formData	=	$request->all();
		$response	=	array();

		if(!empty($formData)){
			$validator = Validator::make(
				$request->all(),
				array(
					"email" 				=> "required|email",
				),array(
					"email.required"		=>	trans("The email is required."),
					"email.email"			=>	trans("Please enter valid email."),
				)
			);
			if ($validator->fails()){
				$response["status"]			=	"error";
				$response["message"]		=	$validator->errors();
				$response["data"]			=	null;
			}else{
				$email		=	$request->input('email');
				$userDetail	=	User::select("id","is_active","name","email","phone_number","user_role_id","image")->where('email',$email)->where("deleted_at",NULL)->first();
				if(!empty($userDetail)){
					if($userDetail->is_active == 1){
						$forgot_password_validate_string	= 	md5($userDetail->email.time().time());
						User::where('email',$email)->update(array('forgot_password_validate_string'=>$forgot_password_validate_string));
						//send email to user for reset password
						$settingsEmail 		=   Config::get('Site.email');
						$email 				=   $userDetail->email;
						$full_name			=   $userDetail->name;
						$route_url      	=   WEBSITE_URL."reset-password/".$forgot_password_validate_string;
						$varify_link   		=   $route_url;

						$emailActions		=	EmailAction::where('action','=','forgot_password')->get()->toArray();
						$emailTemplates		=	EmailTemplate::where('action','=','forgot_password')->get(array('name','subject','action','body'))->toArray();
						$cons 				= 	explode(',',$emailActions[0]['options']);
						$constants 			= 	array();

						foreach($cons as $key=>$val){
							$constants[] = '{'.$val.'}';
						}
						$subject 			=  $emailTemplates[0]['subject'];
						$rep_Array 			= 	array($full_name,$varify_link,$route_url);
						$messageBody		=  str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
						$this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);

						$response["status"]			=	"success";
						$response["message"]		=	trans('Reset password verification email has been sent on your email address');
						$response["data"]			=	array();

					}else{
						$response["status"]		=	"error";
						$response["msg"]		=	trans("Your account has been temporarily disabled");
						$response["data"]		=	array();
					}
				}else{
					$response["status"]		=	"error";
					$response["msg"]		=	trans("The email is not registered with us");
					$response["data"]		=	array();
				}
			}
		}else{
			$response["status"]		=	"error";
			$response["message"]	=	trans('Invalid Request.');
			$response["data"]		=	array();
		}
			return json_encode($response);
    }

    public function signUp(Request $request){
		$formData	=	$request->all();
		$response	=	array();
		Validator::extend("custom_password", function($attribute, $value, $parameters) {
			if (preg_match("#[0-9]#", $value) && preg_match("#[a-zA-Z]#", $value)) {
				return true;
			} else {
				return false;
			}
		});

		if(!empty($formData)){
			$validator = Validator::make(
				$request->all(),
				array(
                    "first_name" 					=> "required",
                    "last_name"                     => "required",
					"email" 				        => "required|email|unique:users,email,null,id,deleted_at,null",
					'phone_number' 			        => 'required|numeric|unique:users,phone_number,null,id,deleted_at,null',
					'password'				        => 'required|min:8|custom_password',
                    'confirm_password'  	        => 'required|min:8|same:password',
				),array(
					"email.required"			=>	trans("The email field is required."),
                    "email.email"				=>	trans("Please enter valid email."),
                    "email.unique"              =>  trans("The email is already taken."),
					"phone_number.required"		=>	trans("The phone number field is required."),
					"phone_number.unique"		=>	trans("The phone number is already taken."),
					"phone_number.numeric"		=>	trans("The phone number must be numeric."),
                    "first_name.required"		=>	trans("The first name field is required."),
                    "last_name.required"		=>	trans("The last name field is required."),
					"password.required"			=>	trans("Password is required"),
					"password.min"				=>	trans("Password must be 8 characters long."),
					"password.custom_password"	=>	trans("Password must have be a combination of numeric, alphabet and special characters."),
					"confirm_password.required"	=>	trans("Confirm password is required"),
					"confirm_password.min	"	=>	trans("Confirm password must be 8 characters long."),
                    "confirm_password.same	"	=>	trans("Confirm password should match with password"),
				)
			);
			if ($validator->fails()){
				$response["status"]			=	"error";
				$response["message"]		=	$validator->errors();
				$response["data"]			=	null;
			}else{
				$obj					        = 	new User();
                $obj->first_name		 		=	$request->get('first_name');
                $obj->last_name		 		    =	$request->get('last_name');
				$obj->email				        =	$request->get('email');
                $obj->phone_number 		        =   $request->get('phone_number');
                $obj->is_active                 =   1;
                $obj->user_role_id              =   CUSTOMER_ROLE_ID;
				$obj->password	 		        =   Hash::make($request->get('password'));
				$validateString		            =   md5(time() . $obj->email);
				$obj->validate_string 	        =   $validateString;

                if($request->get('phone_number')){
                    $client 	= 	new \GuzzleHttp\Client();
                    $otp 		   = rand(100000, 999999);
                    $mobileno	   = $request->get('phone_number');

                    $send_msg	   = $client->request('POST','https://www.vonage.com/',  [
                        Nexmo::message()->send([
                            'to'   => '918619473517',
                            'from' => 'Vonage APIs',
                            'text' => 'OTP for varificaion is'.$otp
                            ]),
                    ]);
                    $obj->remember_token = $otp;

                    //send email to user
                    $emailActions		=  EmailAction::where('action','=','account_verification')->get()->toArray();
                    $emailTemplates		=  EmailTemplate::where('action','=','account_verification')->get(array('name','subject','action','body'))->toArray();
                    $cons 				=  explode(',',$emailActions[0]['options']);
                    $constants 			=  array();
                    foreach($cons as $key=>$val){
                        $constants[] = '{'.$val.'}';
                    }
                    $route_url          =  	FRONT_WEBSITE_URL.'account-verification/'.$obj->validate_string;
                    $click_link   		=   "<a href='".$route_url."' target='_blank'> Click Here </a>";
                    $full_name			=	$request->get('first_name');
                    $email				=	$request->get('email');
                    $subject 			=   $emailTemplates[0]['subject'];
                    $rep_Array 			=   array($full_name,$click_link,$route_url, $otp);
                    $messageBody		=   str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
                    $settingsEmail 		= 	Config::get('Site.email');
                    $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);

                    $response["status"]			=	"success";
                    $response["message"]		=	trans('OTP send successfully.');
                    $response["data"]			=	null;
                }else{
                    $response["status"]			=	"error";
                    $response["message"]		=	trans('Please enter a valid mobile no.');
                    $response["data"]			=	null;
                }

                $obj->save();

				$response["status"]			=	"success";
				$response["message"]		=	trans('You account has been registered successfully. Please enter otp verify your account.');
                $response["data"]			=	array();

			}
		}else{
			$response["status"]		=	"error";
			$response["message"]	=	trans('Invalid Request.');
			$response["data"]		=	array();
        }

        if($request->otp){
         $obj = User::where('deleted_at',Null)->get();
            foreach($obj as $ob){
            if($ob->remember_token == $request->get('otp')){
            $ob->is_mobile_verified = 1;
            $ob->save();

                $response["status"]			=	"success";
                $response["message"]		=	trans('User Verified.');
                $response["data"]			=	null;
            }else{
                $response["status"]			=	"error";
                $response["message"]		=	trans('Please enter correct otp');
                $response["data"]			=	null;
            }
        }
        }
			return json_encode($response);
    }

    public function changePassword(Request $request,$validate_string = ""){
		$formData = $request->all();
		$response = array();

		if(!empty($formData)){
			Validator::extend('custom_password', function($attribute, $value, $parameters) {
				if (preg_match('#[0-9]#', $value) && preg_match('#[a-zA-Z]#', $value)) {
				return true;
				} else {
				return false;
				}
			});
			$validator = Validator::make(
				$request->all(),
				array(
					'password' 			=> 'required|custom_password|min:8',
					'confirm_password' 	=> 'required|same:password',
				),
				array(
					"password.required"			=>	trans("Password is required"),
					"password.min"				=>	trans("Password must be 8 characters long."),
					"password.custom_password"	=>	trans("Password must have be a combination of numeric, alphabet and special characters."),
					"confirm_password.required"	=>	trans("Confirm password is required"),
					"confirm_password.same	"	=>	trans("Confirm password should match with password"),
				)
			);
			if ($validator->fails()){
				$response["status"] = "error";
				$response["message"] = $validator->errors();
				$response["data"] = null;
			}else{
				$userInfo = User::select('email','first_name','last_name','phone_number')->where('forgot_password_validate_string',$validate_string)->first();

                if(!empty($userInfo)){
					$password 					= 	$request->input('password');
					User::where('forgot_password_validate_string',$validate_string)
					->update(array(
						'password'							=>	Hash::make($password),
						'forgot_password_validate_string'	=>	'',
					));
					$response["status"]		=	"success";
					$response["msg"]		=	trans("Your password has been updated successfully.");
					$response["data"]		=	array();

					$settingsEmail 			= 	Config::get('Site.email');
					$action 				= 	"reset_password";
					$emailActions 			= 	EmailAction::where('action','=','reset_password')->get()->toArray();
					$emailTemplates 		= 	EmailTemplate::where('action','=','reset_password')->get(array('name','subject','action','body'))->toArray();
					$cons 					= 	explode(',',$emailActions[0]['options']);
					$constants			 	= 	array();
					foreach($cons as $key=>$val){
						$constants[] = '{'.$val.'}';
					}
					$subject 				= 	$emailTemplates[0]['subject'];
					$rep_Array 				= 	array($userInfo->first_name);
					$messageBody 			= 	str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
					$this->sendMail($userInfo->email,$userInfo->first_name,$subject,$messageBody,$settingsEmail);
				}else{
					$response["status"] = "error";
					$response["message"] = trans('Invalid Request.');
					$response["data"] = array();
				}
			}
		}else {
			$response["status"] = "error";
			$response["message"] = trans('Invalid Request.');
			$response["data"] = array();
		}
		return json_encode($response);
    }

    public function updateProfile(Request $request){
		$formData	=	$request->all();
		$response	=	array();

		if(!empty($formData)){
			$validator = Validator::make(
				$request->all(),
				array(
                    "first_name" 					=> "required",
                    "last_name"                     => "required",
					"email" 				        => "required|email",
					"phone_number"			        => "required",
				),array(
					"email.required"		        =>	trans("The email is required."),
					"email.email"			        =>	trans("Please enter valid email."),
                    "first_name.required"			=>	trans("The first name is required."),
                    "last_name"                     =>  trans("The Last name is required."),
                    "phone_number.required"         =>  trans("The phone number field is required."),
				)
			);
			if ($validator->fails()){
				$response["status"]			=	"error";
				$response["message"]		=	$validator->errors();
				$response["data"]			=	null;
			}else{
				$obj					         = 	User::find(Auth::guard('api')->user()->id);
                $obj->first_name			 	 =	$request->get('first_name');
                $obj->last_name                  =  $request->get('last_name');
				$obj->email				         =	$request->get('email');
				$obj->phone_number		         =	$request->get('phone_number');
				$obj->save();

				$response["status"]			=	"success";
				$response["message"]		=	trans('Profile updated Successfully');
				$response["data"]			=	$obj;
			}
		}else{
			$response["status"]		=	"error";
			$response["message"]	=	trans('Invalid Request');
			$response["data"]		=	array();
		}
			return json_encode($response,JSON_PRETTY_PRINT);
    }

    public function addContact(Request $request){
        $formData   =   $request->all();
        $response   =   array();
        if(!empty($formData)){
            $validator  =   Validator::make(
                $request->all(),
                array(
                    "name"          =>  "required",
                    "email"         =>  "required|email",
                    "subject"       =>  "required",
                    "message"       =>  "required",
                ),
                array(
                    "name.required"             =>  trans("The name field is required."),
                    "email.required"            =>  trans("The email field is required."),
                    "email.email"               =>  trans("The email must be valid email address."),
                    "subject.required"          =>  trans("The subject field is required."),
                    "message.required"          =>  trans("The message field is required.")
                )
            );
            if($validator->fails()){
                $response["status"]			=	"error";
				$response["message"]		=	$validator->errors();
				$response["data"]			=	null;
            }else{
                $model						        = 	new Contact;
				$model->name  	    		        = 	$request->get('name');
                $model->email               		= 	$request->get('email');
                $model->subject                     =   $request->get('subject');
                $model->message                     =   $request->get('message');
                $model->laundry_id                  =   auth('api')->user()->id;

				$model->save();

				$response["status"]		=	"success";
				$response["message"]	=	trans('Contact added successfully.');
				$response["data"]		=	array();
            }
        }else{
                $response["status"]		=	"error";
				$response["message"]	=	trans('Invalid Request.');
				$response["data"]		=	null;
        }
        return json_encode($response);
    }

    public function resendOtp(Request $request){

		if($request->get('mobile_no')){
			$client 	= 	new \GuzzleHttp\Client();
			$otp 		   = rand(100000, 999999);
			$mobileno	   = $request->mobile_no;

			$send_msg	   = $client->request('POST','https://www.vonage.com/',  [
				Nexmo::message()->send([
					'to'   => '918619473517',
					'from' => 'Vonage APIs',
					'text' => 'OTP for varificaion is'.$otp
					]),
			]);

			$response["status"]			=	"success";
			$response["message"]		=	trans('OTP send successfully.');
			$response["data"]			=	null;
		}else{
			$response["status"]			=	"error";
			$response["message"]		=	trans('Please enter a valid mobile no.');
			$response["data"]			=	null;
		}

		return json_encode($response);
	}

}//end LoginController
