<?php
namespace App\Http\Controllers\Api\v1;
use App\Model\User;
use App\Model\Category;
use Illuminate\Http\Request;
use App,Auth,Config,Validator;
use App\Http\Controllers\BaseController;
use App\Model\Service;

/**
* Category Controller
*
* Add your methods in the class below
*
* This file will render views from views/api
*/
class CategoryController extends BaseController {

    public function addCategory(Request $request){
        $formData	=	$request->all();
		$response	=	array();
		if(!empty($formData)){
			$validator = Validator::make(
				$request->all(),
				array(
					"category_name" 			=> 'required|unique:categories,name,null,id,deleted_at,NULL',
				),array(
					"category_name.required"			    =>	trans("The name field is required."),
                    "category_name.unique"                  =>  trans("The category already exists"),
				)
			);
			if ($validator->fails()){
				$response["status"]			=	"error";
				$response["message"]		=	$validator->errors();
				$response["data"]			=	null;
			}else{
				$model						        = 	new Category;
				$model->name  	    		        = 	$request->get('category_name');
				$model->category_slug        		= 	$request->input('name').'_'.'slug';

				$model->save();

				$response["status"]		=	"success";
				$response["message"]	=	trans('Category added successfully.');
				$response["data"]		=	array();
			}
		}else{
			$response["status"]		=	"error";
			$response["message"]	=	trans('Invalid Request.');
			$response["data"]		=	array();
		}
		return json_encode($response);
    }

    public function addService(Request $request){
        $formData	=	$request->all();
		$response	=	array();
		if(!empty($formData)){
			$validator = Validator::make(
				$request->all(),
				array(
                    "category_id" 	    		=> 'required',
                    "service_name"              => 'required|unique:services,service_name,null,id,deleted_at,NULL',
				),array(
                    "category_id.required"	    		    =>	trans("The category id field is required."),
                    "service_name.required"                 =>  trans("The service name is required."),
                    "service_name.unique"                   =>  trans("The service already exists"),
				)
            );

			if ($validator->fails()){
				$response["status"]			=	"error";
				$response["message"]		=	$validator->errors();
				$response["data"]			=	null;
			}else{

                $obj = Category::all();
                foreach($obj as $ob){
                    if($ob->id == $request->category_id){
                        $model						        = 	new Service;
                        $model->category_id  	    		= 	$ob->id;
                        $model->service_name        		= 	$request->input('service_name');
                        $model->is_active                   =   1;

                        $model->save();

                        $response["status"]		=	"success";
                        $response["message"]	=	trans('Category added successfully.');
                        $response["data"]		=	array();
                    }
                }
            }

		}else{
			$response["status"]		=	"error";
			$response["message"]	=	trans('Invalid Request.');
			$response["data"]		=	array();
		}
		return json_encode($response);
    }



}
