<?php
namespace App\Http\Controllers\front;
use App\Http\Controllers\BaseController;
use App\Model\User;
use App\Model\LoginHistory;
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use App\Model\AuthorizeDevice;
use App\Model\AccountActivity;
use App\Model\Block;
use App\Model\SystemDoc;
use App\Model\PartnerDocument;
use App\Model\NewsLettersubscriber;
use Illuminate\Support\Facades\Redis;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Routing\Controller;
use Socialite;


class AuthController extends BaseController
{
   

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	 /**
	 * Redirect the user to the facebook authentication page.
	 *
	 * @return Response
	 */
	public function redirectToProvider($provider)
    {
		
		if(Session::has('SocialUserRole')){
			Session::forget('SocialUserRole');
		}
		
		$userRoleId			=	$provider;
		Session::put('SocialUserRole',$userRoleId);
		switch ($provider) {
			case 'facebook':
				  return Socialite::driver($provider)->fields(['first_name', 'last_name', 'email', 'gender', 'verified','birthday','address'])->redirect();
				break;
			case 'twitter':
				 return Socialite::driver($provider)->redirect();
				break;
			case 'google':
				 return Socialite::driver($provider)->redirect();
				break;
		}
    }

    /**
     * Obtain the user information from facebook.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {	
		
		$errorFacebook	=	Input::get('error');
		$errorTwitter	=	Input::get('denied');
		if($errorFacebook!='' || $errorTwitter!=''){
			return Redirect::to('/');
		}
        $user 			=	Socialite::driver($provider)->user();
		$socialField	=	$provider.'_id';
		$first_name 	= 	'';
		$last_name 		= 	'';
		$email 			= 	'';
		$gender 		= 	'';
		
		switch($provider){
			case 'facebook':
				$full_name_array			=	(!empty($user->name)) ? explode(" ",$user->name) : "";
				$first_name 				= 	(!empty($full_name[0])) ? $full_name[0] : "";
				$last_name 					= 	(!empty($full_name[1])) ? $full_name[1] : "";
				$full_name					=  (!empty($user->name)) ? $user->name : "";	
				$email 						= 	isset($user->email) ? $user->email  : '';
				$socialId 					= 	isset($user->id) ?  $user->id : '';
				break;
			case 'google':
				
				$full_name					=   $user->name;
				$email						=   $user->email;
				$socialId 					= 	$user->id; 
				$profilePic 				= 	$user->avatar_original; 
		}
		
		
		if(!empty($email) && !empty($socialId)){
			$userDetailEmail		=	User::where('email',$email)->where("is_deleted",0)->first();
			$userDetailSocial	=	User::where("$socialField",$socialId)->where("is_deleted",0)->first();
			if(!empty($userDetailEmail)){
				
				User::where('id',$userDetailEmail->id)->update(array("$socialField"=>$socialId));
				$this->loginFromFacebook($userDetailEmail);
				
			}elseif(!empty($userDetailSocial)){
				
				
				
				User::where('id',$userDetailSocial->id)->update(array("email"=>$email));
				$this->loginFromFacebook($userDetailSocial);
			}else{
				
				
				
				$obj 							=  new User;
				$validateString					=  md5(time() . $email);
				$obj->validate_string			=  $validateString;		
				$obj->signup_validate_string_time =  date('Y-m-d H:i:s');
				$obj->disable_verification_code	=  md5(time().time().$email);
				$obj->username 					=  $email;
				$obj->email 					=  $email;
				$obj->slug	 					=  '';
				$obj->password	 				=  '';
				$obj->user_role_id				=  FRONT_USER_ROLE_ID;
				$obj->is_verified				=  1; 	
				$obj->is_active					=  1;
				$obj->allow_fee_user_base		=  1;
				$obj->verify_new_device_and_ip	=  1;
				$obj->twofa_withdrawal			=  1;
				$obj->taker_fee 				=  Config::get('Site.default_taker_trading_fee');
				$obj->maker_fee 				=  Config::get('Site.default_maker_trading_fee');
				$obj->change_password_time		=  "";
				$obj->full_name					=   $full_name;
				Session::put('user_signup_verification_details',$obj);
				$obj->user_lavel_24_withdrawal_limit	=  "LEVEL_1";
				$obj->daily_ithdrawal_imit				=  config::get("Site.user_lavel_one_24_withdrawal_limit");
				$obj->$socialField					=  $socialId;
				//echo '<pre>'; print_r($obj);die;
				$obj->save();

				$this->saveUserCcmWallet($obj->id,$obj->email); 
				
				$userId					=  $obj->id;
				$this->loginFromFacebook($obj);
			
			
			}
		}elseif(!empty($socialId)){
			$userDetailSocial	=	User::where("$socialField",$socialId)->where("is_deleted",0)->first();
			if(!empty($userDetailSocial)){
				$this->loginFromFacebook($userDetailSocial);
			}else{
				$socialInfo = array('fullname'=>$full_name,'social_id'=>$socialId,'type'=>$provider);
				Session::put('social_information_user',$socialInfo);
				$previous_url	=	WEBSITE_URL."register";
				echo ("<script>location.href='$previous_url'</script>");
			}
		}
    }
    
    
    /**
     * login from facebook.
     *
     * @return Response
     */
    public function loginFromFacebook($user)
    {	
		
		$AuthUserDetial = $user;
		$browser 	 			= 	$this->getBrowser();
		$get_client_location	=	$this->get_client_location();
		$get_client_ip			=	$this->get_client_ip();
		$last_login_detail		=	DB::table("authorize_devices")->where("user_id",$AuthUserDetial->id)->where("browser_device",$browser)->where("ip_address",$get_client_ip)->first();
		$is_ask_for_browser_verify	=	0;
		if(empty($last_login_detail)){
			$is_ask_for_browser_verify	=	1;
		}
		if($AuthUserDetial->verify_new_device_and_ip == 0){
			$is_ask_for_browser_verify	=	0;
		}
		if($is_ask_for_browser_verify == 0){
			$browser 	  = $this->getBrowser();
			$loginHistory = DB::table("login_history")->where("user_id",$AuthUserDetial->id)->orderBy("created_at",'DESC')->first();
			if($AuthUserDetial->twofactor_security == 1 && $AuthUserDetial->twofa_login == 1){
				Session::put('two_step_verification',1);
				$AuthUserDetial1	=	json_decode(json_encode($AuthUserDetial),true);
				Session::put('auth_user_data',$AuthUserDetial1);
				$previous_url	=	WEBSITE_URL."two_step_verification";
			}else{
				Session::put('login_email_verification',1);
				$AuthUserDetial1	=	json_decode(json_encode($AuthUserDetial),true);
				Session::put('auth_user_data',$AuthUserDetial1);
				Session::flash('success',  trans("messages.page.login.alert.code.sent"));  
				$this->sendLoginEmailVerificationEmail($AuthUserDetial->id);
				///Auth::logout();
				$previous_url	=	WEBSITE_URL."email_verification";	
			}
			
			echo ("<script>location.href='$previous_url'</script>");
		}else{
			//Send verify ip link
			$settingsEmail 						= 	Config::get('Site.email');
			$emailActions						= 	EmailAction::where('action','=','security_verification')->get()->toArray();
			$emailTemplates						= 	EmailTemplate::where('action','=','security_verification')->get(array('name','subject','action','body'))->toArray();
			$cons 								= 	explode(',',$emailActions[0]['options']);
			$constants 							= 	array(); 
			foreach($cons as $key => $val){	
				$constants[] 					= 	'{'.$val.'}';
			}
			$subject 							= 	$emailTemplates[0]['subject'];
			$varification_code					=	rand(100000,999999);
			User::where("email",$AuthUserDetial->email)->update(array("security_verification_sent_time"=>date("Y-m-d H:i:s"),"security_verification_code"=>$varification_code));
			$verification_link      			= 	URL::to('verify-device/'.$AuthUserDetial->username.'/'.base64_encode($varification_code));
			$userDetail						    =	User::where("email",$AuthUserDetial->email)->first();
			$username							=   $userDetail->username;
			$browser 	  			=   $this->getBrowser();
			$get_client_ip			=	$this->get_client_ip();
			$get_client_location	=	$this->get_client_location();
			
			$rep_Array 				= 	array($verification_link,$get_client_location,$get_client_ip,$browser);
			$messageBody			= 	str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
			$mail					= 	$this->sendMail($AuthUserDetial->email,$AuthUserDetial->email,$subject,$messageBody,$settingsEmail);

			$obj 					= 	new AuthorizeDevice;
			$obj->user_id 			= 	$userDetail->id;
			$obj->login_time 		= 	date("Y-m-d H:i:s");
			$obj->user_agent 		= 	$_SERVER['HTTP_USER_AGENT'];
			$obj->browser_device 	= 	$browser;
			$obj->location 			= 	$get_client_location;
			$obj->ip_address 		= 	$get_client_ip;
			$obj->last_activity 	= 	date("Y-m-d H:i:s");
			$obj->validate_string 	= 	base64_encode($varification_code);
			$obj->save();

			$data = array(
				'user_id'						=>	$AuthUserDetial->id,
				'email'							=>	$AuthUserDetial->email,
				'username'						=>	$userDetail->username,
				'security_verification_code'	=>	$userDetail->security_verification_code,
				'browser'						=>	$browser,
				'get_client_ip'					=>	$get_client_ip,
				'get_client_location'			=>	$get_client_location
			);
			
			Session::put('is_device_verified',$data);
			//Auth::logout();
			Session::flash('success',trans('messages.page.login.alert.verification.link.sent'));
			$previous_url	=	WEBSITE_URL."device-verification";	
			echo ("<script>location.href='$previous_url'</script>");
		}
		
		
	}
    
    
    
    
    
    
 
}
