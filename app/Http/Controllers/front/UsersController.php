<?php

/**
 * User Controller
 */
namespace App\Http\Controllers\front;
use App\Http\Controllers\BaseController;
use Redirect;
use View;
use Input;
use App\Model\Blog;
use App\Model\Team;
use App\Model\Story;
use Validator;
use Hash;
use Session;
use App\Model\User;

class UsersController extends BaseController {
	
/** 
* Function to redirect website on main page
*
* @param null
* 
* @return
*/
	public function index(){
		//return Redirect::to("adminpnlx");
	$blogs = Blog::where(['is_active' => 1, 'is_deleted' => 0])->orderBy('id', 'desc')->limit(4)->get();
	$teams = Team::where(['is_active' => 1, 'is_deleted' => 0])->orderBy('id', 'desc')->limit(4)->get();
	$stories = Story::where(['is_active' => 1, 'is_deleted' => 0])->orderBy('id', 'desc')->limit(4)->get();
	$aboutus = '';
	//echo "<pre>";print_r($stories); 
	$msg = 'test';
	  return View::make('admin.login.front_reset_password_msg',compact('msg'));
	}
	
	/** 
	* Function use for reset passowrd
	* @param null
	* 
	* @return void
	*/	
	public function resetPassword($validate_string ='' ){
		if($validate_string!="" && $validate_string!=null){
			$userDetail	=	User::where('forgot_password_validate_string',$validate_string)->first();
			if(!empty($userDetail)){
				return View::make('admin.login.front_reset_password',compact('validate_string'));
			}else{
				Session::put("show_msg","Somthing went wrong. Please again after some time.");
				return Redirect::to('/reset-password-msg');
			}
		}else{
			Session::put("show_msg","Somthing went wrong. Please again after some time.");
			return Redirect::to('/reset-password-msg');
		}
	}//end resetPassword()

	/** 
	* Function use for save password
	*
	* @param null
	* 
	* @return void
	*/
	public function saveResetPassword($validate_string){
		$thisData				=	Input::all(); 
		Input::replace($this->arrayStripTags($thisData));
		$newPassword		=	Input::get('new_password');
		//$validate_string	=	Input::get('validate_string');
	
		$messages = array(
			'new_password.required' 				=> trans('The New Password field is required.'),
			'new_password_confirmation.required' 	=> trans('The confirm password field is required.'),
			'new_password.confirmed' 				=> trans('The confirm password must be match to new password.'),
			'new_password.min' 						=> trans('The password must be at least 8 characters.'),
			'new_password_confirmation.min' 		=> trans('The confirm password must be at least 8 characters.'),
			"new_password.custom_password"			=>	"Password must have combination of numeric, alphabet and special characters.",
		);
		
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if (preg_match('#[0-9]#', $value) && preg_match('#[a-zA-Z]#', $value) && preg_match('#[\W]#', $value)) {
				return true;
			} else {
				return false;
			}
		});
		$validator = Validator::make(
			Input::all(),
			array(
				'new_password'			=> 'required|min:8|custom_password',
				'new_password_confirmation' => 'required|same:new_password', 

			),$messages
		);
		if ($validator->fails()){	
			return Redirect::back()
				->withErrors($validator)->withInput();
		}else{
			User::where('forgot_password_validate_string',$validate_string)
				->update(array(
						'password'							=>	Hash::make($newPassword),
						'forgot_password_validate_string'	=>	''
				));
			Session::put("show_msg","Thank you for resetting your password. Please login to access your account.");
			return Redirect::to('/reset-password-msg');	
		}
	}//end saveResetPassword()

	public function resetPasswordMsg(){
		$msg		=	(!empty(Session::get("show_msg"))) ? Session::get("show_msg") : "Somthing went wrong. Please again after some time.";
		return View::make('admin.login.front_reset_password_msg',compact('msg'));
	}
}// end UsersController class
