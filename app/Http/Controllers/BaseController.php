<?php
namespace App\Http\Controllers;
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use App\Model\User;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator,Str,App,DateTime;

/**
* Base Controller
*
* Add your methods in the class below
*
* This is the base controller called everytime on every request
*/
class BaseController extends Controller {

	protected $user;

	public function __construct() {

		/* $this->middleware(function ($request, $next){

		});
		 */
	}// end function __construct()

/**
* Setup the layout used by the controller.
*
* @return layout
*/
	protected function setupLayout(){

		if(Request::segment(1) != 'admin'){

		}
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}//end setupLayout()

/**
* Function to make slug according model from any certain field
*
* @param title     as value of field
* @param modelName as section model name
* @param limit 	as limit of characters
*
* @return string
*/
	public function getSlug($title, $fieldName,$modelName,$limit = 30){
		$slug 		= 	 substr(Str::slug($title),0 ,$limit);
		$Model		=	 "\App\Model\\$modelName";
		$slugCount 	=    count($Model::where($fieldName, 'regexp', "/^{$slug}(-[0-9]*)?$/i")->get());
		return ($slugCount > 0) ? $slug."-".$slugCount : $slug;
	}//end getSlug()
/**
* Function to make slug without model name from any certain field
*
* @param title     as value of field
* @param tableName as table name
* @param limit 	as limit of characters
*
* @return string
*/
	public function getSlugWithoutModel($title, $fieldName='' ,$tableName,$limit = 30){
		$slug 		=	substr(Str::slug($title),0 ,$limit);
		$slug 		=	Str::slug($title);
		$DB 		= 	DB::table($tableName);
		$slugCount 	= 	count( $DB->whereRaw("$fieldName REGEXP '^{$slug}(-[0-9]*)?$'")->get() );
		return ($slugCount > 0) ? $slug."-".$slugCount: $slug;
	}//end getSlugWithoutModel()

/**
* Function to search result in database
*
* @param data  as form data array
*
* @return query string
*/
	public function search($data){
		unset($data['display']);
		unset($data['_token']);
		$ret	=	'';
		if(!empty($data )){
			foreach($data as $fieldName => $fieldValue){
				$ret	.=	"where('$fieldName', 'LIKE',  '%' . $fieldValue . '%')";
			}
			return $ret;
		}
	}//end search()
/**
* Function to send email form website
*
* @param string $to            as to address
* @param string $fullName      as full name of receiver
* @param string $subject       as subject
* @param string $messageBody   as message body
*
* @return void
*/
public function sendMail($to,$fullName,$subject,$messageBody, $from = '',$files = false,$path='',$attachmentName='') {
	$from	=	Config::get("Site.from_email");
	/* $url 			= 	'https://api.sendgrid.com/v3/mail/send';
	$access_token	=	"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";

	$view = View::make('emails.template',compact("messageBody"));
	$messageBody = $view->render();

	$json_string 		=	array(
		'personalizations' => array(

			array("to"	=>	array(
				array("email"=>$to)
			))
			),
			"from"	=>	array(
				"email"=>(!empty($from) ? $from : Config::get("Site.email"))
			),
			"content"	=>	array(
				array("type"=>"text/html",
				"value"=>$messageBody)
			),
			"subject"=> $subject
	);
	$s 		= 	curl_init($url);
	curl_setopt($s, CURLOPT_POST, 1);
	curl_setopt($s, CURLOPT_POSTFIELDS, json_encode($json_string));
	curl_setopt($s,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization: Bearer '.$access_token));
	curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
	# Get the response
	$result = curl_exec($s);


	$data				=	array();
	$data['to']			=	$to;
	$data['from']		=	(!empty($from) ? $from : Config::get("Site.email"));
	$data['fullName']	=	$fullName;
	$data['subject']	=	$subject;
	$data['parents_email']	=	$parents_email; */
	/*
	if(!empty($data['parents_email'])){
		Mail::send('emails.template', array('messageBody'=> $messageBody), function($message) use ($data){
			$message->to($data['to'], $data['fullName'])->from($data['from'],Config::get("Site.title"))->subject($data['subject'])->bcc($data['parents_email']);
		});
	}else{
		Mail::send('emails.template', array('messageBody'=> $messageBody), function($message) use ($data){
			$message->to($data['to'], $data['fullName'])->from($data['from'],Config::get("Site.title"))->subject($data['subject']);
		});
	}
	 */
	$data				=	array();
	$data['to']			=	$to;
	$data['from']		=	(!empty($from) ? $from : Config::get("Site.email"));
    $data['fullName']	=	$fullName;
    $data['subject']	=	$subject;
	$data['filepath']	=	$path;
	$data['attachmentName']	=	$attachmentName;
	 if($files===false){
		Mail::send('emails.template', array('messageBody'=> $messageBody), function($message) use ($data){
			$message->to($data['to'], $data['fullName'])->from($data['from'])->subject($data['subject']);

		});
	}else{
		if($attachmentName!=''){
			Mail::send('emails.template', array('messageBody'=> $messageBody), function($message) use ($data){
				$message->to($data['to'], $data['fullName'])->from($data['from'])->subject($data['subject'])->attach($data['filepath'],array('as'=>$data['attachmentName']));
			});
		}else{
			Mail::send('emails.template', array('messageBody'=> $messageBody), function($message) use ($data){
				$message->to($data['to'], $data['fullName'])->from($data['from'])->subject($data['subject'])->attach($data['filepath']);
			});
		}
	}
	DB::table('email_logs')->insert(
		array(
			'email_to'	 => $data['to'],
			'email_from' => $from,
			'subject'	 => $data['subject'],
			'message'	 =>	$messageBody,
			'created_at' => DB::raw('NOW()')
		)
	);
}

	/**
	 * Function to SendSms
	 *
	 * @param string $to  as to mobile
	 *
	 * @param string $messageBody   as message body
	 *
	 * @return void
	 */
	public function SendSms($to = "",$body = "") {
		//$to	=	"+91".$to;
		/* if($to != "") {
			$sid 		= 	config::get("twilio.twilio_sid");
			$from 		= 	config::get("twilio.twilio_from");
			$token 		=	config::get("twilio.twilio_token_number");
			$uri 		= 	"https://api.twilio.com/2010-04-01/Accounts/" . $sid . "/Messages";
			$auth 		= 	$sid . ':' . $token;
			$fields 	=
			'&To=' . urlencode( $to ) .
			'&From=' . urlencode( $from ) .
		//	'&Body=' . urlencode( $body );
			'&Body=' . $body;
			// start cURL
			$res 		= 	curl_init();
			curl_setopt( $res, CURLOPT_URL, $uri );
			curl_setopt( $res, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt( $res, CURLOPT_RETURNTRANSFER, true ); // don't echo
			curl_setopt( $res, CURLOPT_POST, true ); // number of fields
			curl_setopt( $res, CURLOPT_POSTFIELDS, $fields );
			curl_setopt( $res, CURLOPT_USERPWD, $auth ); // authenticate

			try {
				$result 	= 	curl_exec( $res );
				print_r($result);die;
				curl_close($res);
			} catch (Exception $e) {

			}
		} */
		return true;
	}

	public function getVerificationCode(){
		//$code	=	rand(100000,999999);
		$code	=	"0000";
		return $code;
	}

	public  function arrayStripTags($array){
		$result =array();
		foreach ($array as $key => $value) {
			// Don't allow tags on key either, maybe useful for dynamic forms.
			$key = strip_tags($key,ALLOWED_TAGS_XSS);

			// If the value is an array, we will just recurse back into the
			// function to keep stripping the tags out of the array,
			// otherwise we will set the stripped value.
			if (is_array($value)) {
				$result[$key] = $this->arrayStripTags($value);
			} else {
				// I am using strip_tags(), you may use htmlentities(),
				// also I am doing trim() here, you may remove it, if you wish.
				$result[$key] = trim(strip_tags($value,ALLOWED_TAGS_XSS));
			}
		}

		return $result;

	}

	public function saveCkeditorImages() {
		if(!empty($_GET['CKEditorFuncNum'])){
			$image_url				=	"";
			$msg					=	"";
			// Will be returned empty if no problems
			$callback = ($_GET['CKEditorFuncNum']);        // Tells CKeditor which function you are executing
			$image_details 				= 	getimagesize($_FILES['upload']["tmp_name"]);
			$image_mime_type			=	(isset($image_details["mime"]) && !empty($image_details["mime"])) ? $image_details["mime"] : "";
			if($image_mime_type	==	'image/jpeg' || $image_mime_type == 'image/jpg' || $image_mime_type == 'image/gif' || $image_mime_type == 'image/png'){
				$ext					=	$this->getExtension($_FILES['upload']['name']);
				$fileName				=	"ck_editor_".time().".".$ext;
				$upload_path			=	CK_EDITOR_ROOT_PATH;
				if(move_uploaded_file($_FILES['upload']['tmp_name'],$upload_path.$fileName)){
					$image_url 			= 	CK_EDITOR_URL. $fileName;
				}
			}else{
				$msg =  'error : Please select a valid image. valid extension are jpeg, jpg, gif, png';
			}
			$output = '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$callback.', "'.$image_url .'","'.$msg.'");</script>';
			echo $output;
			exit;
		}
	}

	function getExtension($str) {
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		$ext = strtolower($ext);
		return $ext;
	}

/**
 * Function to _update_all_status
 *
 * param source tableName,id,status,fieldName
 */
	public function _update_all_status($tableName = null,$id = 0,$status= 0,$fieldName = 'is_active'){
		DB::beginTransaction();
		$response			=	DB::statement("CALL UpdateAllTableStatus('$tableName',$id,$status)");
		if(!$response) {
			DB::rollback();
			Session::flash('error', trans("messages.msg.error.something_went_wrong"));
			return Redirect::back();
		}
		DB::commit();
	}

/**
 * Function to _delete_table_entry
 *
 * param source tableName,id,fieldName
 */
	public function _delete_table_entry($tableName = null,$id = 0,$fieldName = null){
		DB::beginTransaction();
		$response			=	DB::statement("CALL DeleteAllTableDataById('$tableName',$id,'$fieldName')");
		if(!$response) {
			DB::rollback();
			Session::flash('error', trans("messages.msg.error.something_went_wrong"));
			return Redirect::back();
		}
		DB::commit();
	}// end _delete_table_entry()

	public function change_error_msg_layout($errors = array()){
		$response				=	array();
		$response["status"]		=	"error";
		if(!empty($errors)){
			$error_msg				=	"";
			foreach($errors as $errormsg){
				$error_msg1			=	(!empty($errormsg[0])) ? $errormsg[0] : "";
				$error_msg			.=	$error_msg1.", ";
			}
			$response["msg"]	=	trim($error_msg,", ");
		}else {
			$response["msg"]	=	"";
		}
		$response["data"]			=	(object)array();
		$response["errors"]			=	$errors;
		return $response;
	}

	public function change_error_msg_layout_with_array($errors = array()){
		$response				=	array();
		$response["status"]		=	"error";
		if(!empty($errors)){
			$error_msg				=	"";
			foreach($errors as $errormsg){
				$error_msg1			=	(!empty($errormsg[0])) ? $errormsg[0] : "";
				$error_msg			.=	$error_msg1.", ";
			}
			$response["msg"]	=	trim($error_msg,", ");
		}else {
			$response["msg"]	=	"";
		}
		$response["data"]			=	array();
		$response["errors"]			=	$errors;
		return $response;
	}

	public function get_csv($data=array(),$posttype){
		ob_start();
		ob_clean();
		$file_name = date("d-m-y").$posttype.".csv";
		@header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		@header('Content-Description: File Transfer');
		@header("Content-type: text/csv");
		@header("Content-Disposition: attachment; filename=".$file_name);
		@header("Expires: 0");
		@header("Pragma: public");
		$file = fopen('php://output', 'w');
			foreach ($data as $row) {
				@fputcsv($file, $row);
			}
		fclose($file);
		exit();
	}// end get_csv()

	public  function arrayRemoveStrings($string){
		$commonWords = array('a','able','about','above','abroad','according','accordingly','across','actually','adj','after','afterwards','again','against','ago','ahead','ain\'t','all','allow','allows','almost','alone','along','alongside','already','also','although','always','am','amid','amidst','among','amongst','an','and','another','any','anybody','anyhow','anyone','anything','anyway','anyways','anywhere','apart','appear','appreciate','appropriate','are','aren\'t','around','as','a\'s','aside','ask','asking','associated','at','available','away','awfully','b','back','backward','backwards','be','became','because','become','becomes','becoming','been','before','beforehand','begin','behind','being','believe','below','beside','besides','best','better','between','beyond','both','brief','but','by','c','came','can','cannot','cant','can\'t','caption','cause','causes','certain','certainly','changes','clearly','c\'mon','co','co.','com','come','comes','concerning','consequently','consider','considering','contain','containing','contains','corresponding','could','couldn\'t','course','c\'s','currently','d','dare','daren\'t','definitely','described','despite','did','didn\'t','different','directly','do','does','doesn\'t','doing','done','don\'t','down','downwards','during','e','each','edu','eg','eight','eighty','either','else','elsewhere','end','ending','enough','entirely','especially','et','etc','even','ever','evermore','every','everybody','everyone','everything','everywhere','ex','exactly','example','except','f','fairly','far','farther','few','fewer','fifth','first','five','followed','following','follows','for','forever','former','formerly','forth','forward','found','four','from','further','furthermore','g','get','gets','getting','given','gives','go','goes','going','gone','got','gotten','greetings','h','had','hadn\'t','half','happens','hardly','has','hasn\'t','have','haven\'t','having','he','he\'d','he\'ll','hello','help','hence','her','here','hereafter','hereby','herein','here\'s','hereupon','hers','herself','he\'s','hi','him','himself','his','hither','hopefully','how','howbeit','however','hundred','i','i\'d','ie','if','ignored','i\'ll','i\'m','immediate','in','inasmuch','inc','inc.','indeed','indicate','indicated','indicates','inner','inside','insofar','instead','into','inward','is','isn\'t','it','it\'d','it\'ll','its','it\'s','itself','i\'ve','j','just','k','keep','keeps','kept','know','known','knows','l','last','lately','later','latter','latterly','least','less','lest','let','let\'s','like','liked','likely','likewise','little','look','looking','looks','low','lower','ltd','m','made','mainly','make','makes','many','may','maybe','mayn\'t','me','mean','meantime','meanwhile','merely','might','mightn\'t','mine','minus','miss','more','moreover','most','mostly','mr','mrs','much','must','mustn\'t','my','myself','n','name','namely','nd','near','nearly','necessary','need','needn\'t','needs','neither','never','neverf','neverless','nevertheless','new','next','nine','ninety','no','nobody','non','none','nonetheless','noone','no-one','nor','normally','not','nothing','notwithstanding','novel','now','nowhere','o','obviously','of','off','often','oh','ok','okay','old','on','once','one','ones','one\'s','only','onto','opposite','or','other','others','otherwise','ought','oughtn\'t','our','ours','ourselves','out','outside','over','overall','own','p','particular','particularly','past','per','perhaps','placed','please','plus','possible','presumably','probably','provided','provides','q','que','quite','qv','r','rather','rd','re','really','reasonably','recent','recently','regarding','regardless','regards','relatively','respectively','right','round','s','said','same','saw','say','saying','says','second','secondly','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sensible','sent','serious','seriously','seven','several','shall','shan\'t','she','she\'d','she\'ll','she\'s','should','shouldn\'t','since','six','so','some','somebody','someday','somehow','someone','something','sometime','sometimes','somewhat','somewhere','soon','sorry','specified','specify','specifying','still','sub','such','sup','sure','t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that','that\'ll','thats','that\'s','that\'ve','the','their','theirs','them','themselves','then','thence','there','thereafter','thereby','there\'d','therefore','therein','there\'ll','there\'re','theres','there\'s','thereupon','there\'ve','these','they','they\'d','they\'ll','they\'re','they\'ve','thing','things','think','third','thirty','this','thorough','thoroughly','those','though','three','through','throughout','thru','thus','till','to','together','too','took','toward','towards','tried','tries','truly','try','trying','t\'s','twice','two','u','un','under','underneath','undoing','unfortunately','unless','unlike','unlikely','until','unto','up','upon','upwards','us','use','used','useful','uses','using','usually','v','value','various','versus','very','via','viz','vs','w','want','wants','was','wasn\'t','way','we','we\'d','welcome','well','we\'ll','went','were','we\'re','weren\'t','we\'ve','what','whatever','what\'ll','what\'s','what\'ve','when','whence','whenever','where','whereafter','whereas','whereby','wherein','where\'s','whereupon','wherever','whether','which','whichever','while','whilst','whither','who','who\'d','whoever','whole','who\'ll','whom','whomever','who\'s','whose','why','will','willing','wish','with','within','without','wonder','won\'t','would','wouldn\'t','x','y','yes','yet','you','you\'d','you\'ll','your','you\'re','yours','yourself','yourselves','you\'ve','z','zero','fuck','bastered','bullshit','Shit','shit','devil','Lorem','lorem','Ipsum','ipsum','dummy','unknown','There','There','if','IF','If','else','Else','all','All','text','Text','Free','free','The','THE','the','Words','Word','word','words','All','all','It');
		$result = preg_replace('/\b('.implode('|',$commonWords).')\b/','',$string);
		return $result;
	}

	public function getSkuID()
    {
		$statement = DB::select("show table status like 'products'");
		$skuid	   = $statement[0]->Auto_increment;
    	return $skuid;
	}


}// end BaseController class
