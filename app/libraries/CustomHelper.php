<?php
class CustomHelper {
	public static function  addhttp($url = "") {
		if($url == ""){
			return "";
		}
		if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
			$url = "http://" . $url;
		}
		return $url;
	}
}
